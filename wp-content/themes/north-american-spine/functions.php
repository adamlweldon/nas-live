<?php
/**
 * North American Spine functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package North_American_Spine
 */

@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );

define('NAS_TEMPLATE_DIR', get_stylesheet_directory_uri());

require_once(dirname(__FILE__) . '/pg_forms.php');
require_once(dirname(__FILE__) . '/bx.multipixel.php');


if ( ! function_exists( 'north_american_spine_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function north_american_spine_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on North American Spine, use a find and replace
	 * to change 'north-american-spine' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'north-american-spine', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'menu-1' => esc_html__( 'Primary', 'north-american-spine' ),
		'menu-alt' => esc_html__( 'Alt Main Menu', 'north-american-spine' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'north_american_spine_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif;
add_action( 'after_setup_theme', 'north_american_spine_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function north_american_spine_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'north_american_spine_content_width', 640 );
}
add_action( 'after_setup_theme', 'north_american_spine_content_width', 0 );
// remove_filter('template_redirect', 'redirect_canonical'); 

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function north_american_spine_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'north-american-spine' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'north-american-spine' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Blog Sidebar', 'north-american-spine' ),
		'id'            => 'blog-sidebar',
		'description'   => esc_html__( 'Add widgets here.', 'north-american-spine' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Blog Search Area', 'north-american-spine' ),
		'id'            => 'blog-searcharea',
		'description'   => esc_html__( 'Add widgets here.', 'north-american-spine' ),
		'before_widget' => '',
		'after_widget'  => '',
		// 'before_title'  => '<h2 class="widget-title">',
		// 'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Info Kit Widget Area', 'north-american-spine' ),
		'id'            => 'nas-info-kit-widget-area',
		'description'   => esc_html__( 'Add widgets here.', 'north-american-spine' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Testimonial Sidebar', 'north-american-spine' ),
		'id'            => 'testimonial-sidebar',
		'description'   => esc_html__( 'Add widgets here.', 'north-american-spine' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

    register_sidebar( array(
        'name'          => esc_html__( 'Testimonial Middle', 'north-american-spine' ),
        'id'            => 'testimonial-middle-sidebar',
        'description'   => esc_html__( 'Add widgets here.', 'north-american-spine' ),
        'before_widget' => '<aside id="%1$s" class="container-fluid connect-patient"><div class="row"><div class="col-md-4 col-md-offset-4">',
        'after_widget'  => '</div></div></aside>',
        'before_title'  => '<h1 class="widget-title">',
        'after_title'   => '</h1>',
    ) );

    register_sidebar( array(
        'name'          => esc_html__( 'Footer', 'north-american-spine' ),
        'id'            => 'nas-footer',
        'description'   => esc_html__( 'Add widgets for the footer here. (Under the Copyright notice).', 'north-american-spine' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ) );
}
add_action( 'widgets_init', 'north_american_spine_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function north_american_spine_styles() {
	
    wp_enqueue_style( 'north-american-spine-style', get_stylesheet_uri() );

}
function north_american_spine_maps_handler() {
	global $post;
	$categories = get_the_category();
	$locationsCat = get_category_by_slug('cities'); 
	$locationsId = $locationsCat->term_id;

	$isCat = false;
	foreach ($categories as $category) {
		if ($category->category_parent == $locationsId) {
			$isCat = true;
		}
	}

	if ($isCat == false) {
		return;
	}

    $id = $post->ID;
    $lat = get_post_meta( $id, 'maps_lat', true ) ? get_post_meta( $id, 'maps_lat', true ) : 'false';
    $long = get_post_meta( $id, 'maps_long', true ) ? get_post_meta( $id, 'maps_long', true ) : 'false';
	wp_enqueue_script( 'maps-handler-script', get_template_directory_uri() . '/js/prod/maps-handler.min.js', array('jquery') );
	wp_enqueue_script('google-maps-api', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDG4yfnHtWrh0lwItAgEpKpvJRIfQVkSxw&callback=initMap', array(), false, true );
	 
   
	wp_localize_script( 'maps-handler-script', 'ajax_object',
            array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 'lat' => $lat, 'long' => $long, 'post' => $id, 'date' => date("Y-m-d H:i:s"), 'address' => strip_tags(get_field('overview_block_address')) ) );
}


function north_american_spine_youtube_api() {
    $videoUrl = get_field('video_url');
    if ($videoUrl != null) {
        wp_enqueue_script( 'youtube-api-script', get_template_directory_uri() . '/js/prod/ytiframe.min.js', array('jquery'), null, true );
        wp_localize_script( 'youtube-api-script', 'nas_yt_api', array( 'video' => get_field('video_url' ) ) );
    }
}


add_action( 'wp_enqueue_scripts', 'north_american_spine_styles' );



function category_and_tag_archives( $wp_query ) {
	$my_post_array = array('post','page');
 
 	if ( $wp_query->get( 'category_name' ) || $wp_query->get( 'cat' ) )
	$wp_query->set( 'post_type', $my_post_array );
 
 // 	if ( $wp_query->get( 'tag' ) )
	// $wp_query->set( 'post_type', $my_post_array );
}


/*===================================================================================
* Add global options
* =================================================================================*/
function theme_settings_page(){ 
	?>
	<div class="wrap">
		<h1>Contact Info</h1>
		<p>This information is used around the website, so changing these here will update them across the website.</p>
		<form method="post" action="options.php">
			<?php
			settings_fields("section");
			do_settings_sections("theme-options");
			submit_button();
			?>
		</form>
	</div>
	
<?php }
// Phone
function display_support_phone_element(){ ?>
	
	<input type="tel" name="support_phone" placeholder="Enter phone number" value="<?php echo get_option('support_phone'); ?>" size="35">
<?php }
// Email
function display_support_email_element(){ ?>
	
	<input type="email" name="support_email" placeholder="Enter email address" value="<?php echo get_option('support_email'); ?>" size="35">
<?php }
function display_custom_info_fields(){
	
	add_settings_section("section", "Company Information", null, "theme-options");
	add_settings_field("support_phone", "Phone Number", "display_support_phone_element", "theme-options", "section");
	add_settings_field("support_email", "Email address", "display_support_email_element", "theme-options", "section");
	register_setting("section", "support_phone");
	register_setting("section", "support_email");
}
add_action("admin_init", "display_custom_info_fields");
function add_custom_info_menu_item(){
	
	add_options_page("Contact Info", "Contact Info", "manage_options", "contact-info", "theme_settings_page");
}
add_action("admin_menu", "add_custom_info_menu_item");

function north_american_spine_scripts() {
	wp_enqueue_script( 'nas-jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js', false, 3.1, true );
	wp_enqueue_script( 'nas-slick', 'https://cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js', false, 3.1, true );
	wp_enqueue_script( 'nas-sm', get_template_directory_uri() . '/js/prod/ScrollMagic.min.js', false, 1.0, true );
    wp_enqueue_script( 'nas-scripts', get_template_directory_uri() . '/js/prod/scripts.min.js', false, 1.0, true );
}
add_action( 'wp_enqueue_scripts', 'north_american_spine_scripts' );
add_action( 'wp_enqueue_scripts', 'north_american_spine_maps_handler');
add_action( 'wp_enqueue_scripts', 'north_american_spine_youtube_api');

/**
 * Implement the Custom Header feature.
 */
// require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
// require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
// require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
// require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
// require get_template_directory() . '/inc/jetpack.php';


function north_american_spine_blog_header() {
	// $blog_title = get_the_title( get_option('page_for_posts', true));
	// $post_type = get_post_type();

	// if (condition) {
		# code...
	// 
	// 
	$postType = get_post_type();
	if ($postType == 'post') {
	  	$idObj = get_category_by_slug('blog'); 
  		$id = $idObj->term_id;
		if (in_category('blog') || post_is_in_descendant_category($id) || is_archive()) {
			get_template_part( 'template-parts/header', 'blog' );
		}
		// code...
	} else {
		get_template_part( 'template-parts/header', get_post_type() );	
	}

}

/**
 * Tests if any of a post's assigned categories are descendants of target categories
 *
 * @param int|array $cats The target categories. Integer ID or array of integer IDs
 * @param int|object $_post The post. Omit to test the current post in the Loop or main query
 * @return bool True if at least 1 of the post's categories is a descendant of any of the target categories
 * @see get_term_by() You can get a category by name or slug, then pass ID to this function
 * @uses get_term_children() Passes $cats
 * @uses in_category() Passes $_post (can be empty)
 * @version 2.7
 * @link http://codex.wordpress.org/Function_Reference/in_category#Testing_if_a_post_is_in_a_descendant_category
 */
if ( ! function_exists( 'post_is_in_descendant_category' ) ) {
    function post_is_in_descendant_category( $cats, $_post = null ) {
        foreach ( (array) $cats as $cat ) {
            // get_term_children() accepts integer ID only
            $descendants = get_term_children( (int) $cat, 'category' );
            if ( $descendants && in_category( $descendants, $_post ) )
                return true;
        }
        return false;
    }
}


add_action( 'admin_notices', 'nas_theme_dependencies' );

function nas_theme_dependencies() {
  if( ! function_exists('printer') )
    echo '<div class="error"><p>' . __( 'Warning: The theme needs NAS Support Functions plugin to function', 'north-american-spine' ) . '</p></div>';

  if( ! function_exists('zog_ajax_loader') )
    echo '<div class="error"><p>' . __( 'Warning: The theme needs ZOG Ajax Loader plugin to function', 'north-american-spine' ) . '</p></div>';

  if( ! function_exists('nas_related_post_widget_require') )
    echo '<div class="error"><p>' . __( 'Warning: The theme needs NAS Related Post Widget plugin to function', 'north-american-spine' ) . '</p></div>';
}

/**
 * Filter the except length to 20 words.
 *
 * @param int $length Excerpt length.
 */
function nas_custom_excerpt_length() {
    return 20;
}
add_filter( 'excerpt_length', 'nas_custom_excerpt_length', 999 );



/**
 * Filter the excerpt "read more" string.
 *
 * @param string $more "Read more" excerpt string.
 */
function nas_custom_more() {
    return '...';
}
add_filter( 'excerpt_more', 'nas_custom_more' );



/**
 * Adds additional post types used for different templates.
 * 
 */
function create_post_type() {
  register_post_type( 'nas-testimonials',
    array(
      'labels' => array(
        'name' => __( 'Testimonials' ),
        'singular_name' => __( 'Testimonials' )
      ),
      'menu_icon'           => NAS_TEMPLATE_DIR . "//assets//img//newspaper.svg",
		'public'      => true,
		'has_archive' => true,
        'hierarchical'=> true,
		'rewrite'     => array('slug' => 'testimonials', 'with_front' => false),
		'supports'    => array( 'title', 'editor', 'thumbnail', 'revisions', 'page-attributes' ),
		'taxonomies'  => array( 'category', 'post_tag' ),
    )
  );
  register_post_type( 'nas-news',
    array(
      'labels' => array(
        'name' => __( 'News' ),
        'singular_name' => __( 'News' )
      ),
      'menu_icon'           => NAS_TEMPLATE_DIR . "//assets//img//newspaper.svg",
		'public'      => true,
		'has_archive' => true,
		'rewrite'     => array('slug' => 'news', 'with_front' => false),
		'supports'    => array( 'title', 'editor', 'thumbnail', 'revisions' ),
    )
  );
  register_post_type( 'nas-locations',
    array(
      'labels' => array(
        'name' => __( 'Locations' ),
        'singular_name' => __( 'Locations' )
      ),
      'menu_icon'           => NAS_TEMPLATE_DIR . "//assets//img//locations_icon.svg",
		'public'      => true,
		'rewrite'     => array('slug' => 'locations', 'with_front' => false),
		'supports'    => array( 'title', 'editor', 'thumbnail', 'revisions' ),
		'taxonomies'  => array( 'category' ),
    )
  );
  register_post_type( 'nas-conditions',
    array(
      'labels' => array(
        'name' => __( 'Conditions' ),
        'singular_name' => __( 'Conditions' )
      ),
      'menu_icon'           => NAS_TEMPLATE_DIR . "//assets//img//conditions.svg",
		'public'      => true,
		'rewrite'     => array('slug' => 'conditions', 'with_front' => false),
		'supports'    => array( 'title', 'editor', 'thumbnail', 'revisions' ),
		'taxonomies'  => array( 'category' ),
    )
  );
  register_post_type( 'nas-procedures',
    array(
      'labels' => array(
        'name' => __( 'Procedures' ),
        'singular_name' => __( 'Procedures' )
      ),
      'menu_icon'           => NAS_TEMPLATE_DIR . "//assets//img//pamphlet_icon.png",
		'public'      => true,
		'has_archive' => false,
		'rewrite'     => array('slug' => 'our-procedures', 'with_front' => false),
		'supports'    => array( 'title', 'editor', 'thumbnail', 'revisions' ),
		'taxonomies'  => array( 'category' ),
    )
  );
  register_post_type( 'nas-treatments',
    array(
      'labels' => array(
        'name' => __( 'Treatments' ),
        'singular_name' => __( 'Treatments' )
      ),
      'menu_icon'           => NAS_TEMPLATE_DIR . "//assets//img//spine-logo.svg",
		'public'      => true,
		'has_archive' => false,
		'rewrite'     => array('slug' => 'treatment-guide', 'with_front' => false),
		'supports'    => array( 'title', 'editor', 'thumbnail', 'revisions' ),
		'taxonomies'  => array( 'category' ),
    )
  );
  register_post_type( 'nas-doctors',
    array(
      'labels' => array(
        'name' => __( 'Doctors' ),
        'singular_name' => __( 'Doctors' )
      ),
      'menu_icon'           => NAS_TEMPLATE_DIR . "//assets//img//testimonial.svg",
		'public'      => true,
		'has_archive' => false,
		'rewrite'     => array('slug' => 'resource-center/spine-pain-doctors', 'with_front' => false),
		'supports'    => array( 'title', 'editor', 'thumbnail', 'revisions' ),
    )
  );
  register_post_type( 'nas-pcms',
    array(
      'labels' => array(
        'name' => __( 'PCM' ),
        'singular_name' => __( 'Patient Care Managers' )
      ),
      'menu_icon'           => NAS_TEMPLATE_DIR . "//assets//img//testimonial.svg",
		'public'      => true,
		'has_archive' => false,
		'rewrite'     => array('slug' => 'patient-care-managers', 'with_front' => false),
		'supports'    => array( 'title', 'editor', 'thumbnail', 'revisions' ),
    )
  );
}

function add_taxonomies_to_pages() {

 	// register_taxonomy_for_object_type( 'post_tag', 'page' );
 	register_taxonomy_for_object_type( 'category', 'page' );
}
if ( ! is_admin() ) {
	add_action( 'pre_get_posts', 'category_and_tag_archives' );
}


function add_menu_icons_styles(){
?>

<style>
#adminmenu .wp-menu-image img {
  width: auto;
  max-height: 15px;
}
</style>

<?php
}
add_action( 'admin_head', 'add_menu_icons_styles' );


add_action( 'init', 'create_post_type' );
add_action( 'init', 'add_taxonomies_to_pages' );


function change_page_menu_classes($menu)
{
    global $post;
    // printer(get_post_type)
    if (get_post_type($post) == 'nas-testimonials')
    {
    	// printer($menu);
        $menu = str_replace( 'current_page_parent', '', $menu ); // remove all current_page_parent classes
        $menu = str_replace( 'menu-item-47', 'menu-item-47 current_page_parent', $menu ); // add the current_page_parent class to the page you want
    }
    return $menu;
}
add_filter( 'nav_menu_css_class', 'change_page_menu_classes', 10,2 );

// Let's stop WordPress re-ordering my categories/taxonomies when I select them    
function stop_reordering_my_categories($args) {
    $args['checked_ontop'] = false;
    return $args;
}

// Let's initiate it by hooking into the Terms Checklist arguments with our function above
add_filter('wp_terms_checklist_args','stop_reordering_my_categories');

// function get_lat_long( $post_id ) {

// 	// If this is just a revision, don't do anything
// 	if ( wp_is_post_revision( $post_id ) )
// 		return;

// 	var_dump($post_id);
// 	die;
// }
// add_action( 'save_post', 'get_lat_long' );



// Same handler function...
add_action( 'wp_ajax_maps_handler', 'maps_handler' );
add_action( 'wp_ajax_nopriv_maps_handler', 'maps_handler' );
function maps_handler() {
	    ob_clean();

	    $postMetaDate = get_post_meta( $id, 'maps_lat', true);

	    if ($postMetaDate == false) {
	    	// echo json_encode('it\'s false.');
	    $id = intval($_POST['post_id']);


	    // // $mapLong = array(
	    // // 	$id,
	    // // 	'maps_long',
	    // // 	$_POST['long'],

	    // // );
	    // // $mapsUpdated = array(
	    // // 	$id,
	    // // 	'maps_update',
	    // // 	$_POST['date'],
	    // // );

	    	json_encode(update_post_meta($id, 'maps_lat', $_POST['lat']));
	    	json_encode(update_post_meta($id, 'maps_long', $_POST['long']));
	    // // update_post_meta($mapLong);
	    }
	    // $postMetaDate = get_post_meta( $id, 'maps_lat', true);
	// global $wpdb;
	// $whatever = intval( $_POST['whatever'] );
	// $whatever += 10;
        
	wp_die();
}

function new_subcategory_hierarchy() { 
    $category = get_queried_object();


    $parent_id = $category->category_parent;

    $templates = array();

	// var_dump($parent_id);
	// die;

    if ( $parent_id == 0 ) {
        // Use default values from get_category_template()
        $templates[] = "category-{$category->slug}.php";
        $templates[] = "category-{$category->term_id}.php";
        $templates[] = 'category.php';     
    } else {
        // Create replacement $templates array
        $parent = get_category( $parent_id );
		// var_dump($parent);
		// die;

        // Current first
        $templates[] = "category-{$category->slug}.php";
        $templates[] = "category-{$category->term_id}.php";

        // Parent second
        $templates[] = "category-{$parent->slug}.php";
        $templates[] = "category-{$parent->term_id}.php";
        $templates[] = 'category.php'; 
    }
    return locate_template( $templates );
}

//change text to leave a reply on comment form
function isa_comment_reform ($arg) {
	$arg['title_reply'] = __('Leave a Comment:');
	return $arg;
}
add_filter('comment_form_defaults','isa_comment_reform');
add_filter('widget_text','do_shortcode');
add_filter( 'category_template', 'new_subcategory_hierarchy' );

/**
 * Unregisters certain styles. So we can either include them in SASS or use the function below to include them in the footer
 * 
 *
 * @return void
 */
function dequeue_other_styles() {
	wp_dequeue_style('PGForm_globals');
	wp_deregister_style('PGForm_globals');

	wp_dequeue_style('DP_style');
	wp_deregister_style('DP_style');
}
add_action('wp_enqueue_scripts','dequeue_other_styles');

/**
 * Footer styles
 * Use this for any styles that can be displayed 'after the fold'. For page speed insights.
 *
 * @return void
 */
function footer_styles() {
};
add_action( 'get_footer', 'footer_styles' );


/**
 * Adds ASYNC to the named handles
 *
 * @param string The actual script tag
 * @param string The handle of this particular tag. Should be unique. 
 * @return void
 */
function add_async_attribute($tag, $handle) {

	$asyncScripts = [
		'twitter_widget_api',
		'zog_support_script',
		'zog_ajax_loader_script',
		'jquery-cookie',
		'jquery-migrate'
	];

    if ( !in_array($handle, $asyncScripts) )
        return $tag;
    return str_replace( ' src', ' async="async" src', $tag );
}
add_filter('script_loader_tag', 'add_async_attribute', 10, 2);
