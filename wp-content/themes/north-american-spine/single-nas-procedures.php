<?php
/**
 * Template Name: Each Procedure Template Page
 *
 */
?>

<?php get_header(); ?>

<?php 
	// Start the loop.
	while ( have_posts() ) : the_post();
?>
	<div class="bluearea"></div>
	<div class="container alt-conditions each-procedure" id="conditions">
		<section class="each-block-all" id="pinned-lock-1">
			<div class="left-50">
				<div class="each-block-lock first-lock alt-lock">
					<?php 
						$thumb_id = get_post_thumbnail_id();
						$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
						$thumb_url = $thumb_url_array[0];
						$image_alt = get_post_meta( $thumb_id, '_wp_attachment_image_alt', true);	
					?>
					<?php //printer($thumb_id); ?>
					<?php if ($thumb_id != ''): ?>
						<div class="condition-content-left">
							<h1><?php the_title(); ?></h1>

								<?php
								if( have_rows('alternate_names') ):
									while( have_rows('alternate_names') ): the_row();  ?>
										<h4><?php echo get_sub_field('other_name_used'); ?></h4>
									<?php endwhile; ?>
								<?php endif; ?>
								<p><?php the_content(); ?></p>
						</div>
						<div class="right-img">
						
							<img src="<?php echo $thumb_url; ?>" alt="<?php echo $image_alt; ?>">
						</div>
					<?php else: ?>
						<h1><?php the_title(); ?></h1>
						<?php
						if( have_rows('alternate_names') ):
							while( have_rows('alternate_names') ): the_row();  ?>
								<h4><?php echo get_sub_field('other_name_used'); ?></h4>
							<?php endwhile; ?>
						<?php endif; ?>
						<p><?php the_content(); ?></p>
					<?php endif ?>
					
				</div>
			</div>	
			
			<div class="right-50">
				<div class="each-block-info targetedscrollpoint" id="symptoms-scroll-to">

					<h2>Benefits of <?php the_title(); ?></h2>

					<div class="toggle-area">
						<p><?php echo get_field('benefits_of'); ?></p>
						<a href="/patient-care/free-mri-review/" class="btn-orange invert"><div></div>Free MRI/CT Review</a>
					</div>

					<h2 class="spaces targetedscrollpoint" id="causes-scroll-to">Details of the <?php the_title(); ?></h2>
					
					<div class="toggle-area causesof">

						<?php echo get_field('details_of'); ?>
						
						<?php if (get_field('procedure_youtube_embed') == ''): ?>
							<?php if (get_field('video_embed_code')): ?>
								<div class="proc-video">
									<?php echo get_field('video_embed_code'); ?>
								</div>
							<?php endif ?>

						<?php else: ?>
								<?php if (get_field('video_embed_code')): ?>
									<div class="proc-video">
										<iframe width="100%" height="100%" src="<?php echo get_field('video_embed_code'); ?>?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
									</div>
								<?php endif ?>
						<?php endif ?>
						
					</div>

					<h2 class="spaces targetedscrollpoint" id="prep-scroll-to">Preparing for the <?php the_title(); ?></h2>
					
					<div class="toggle-area">
						<?php echo get_field('preparing_for'); ?>
						<a href="/locations" class="btn-orange invert"><div></div>Explore Our Facilities</a>
					</div>

					<h2 class="spaces targetedscrollpoint" id="life-scroll-to"><?php the_title(); ?> Recovery</h2>
					
					<div class="toggle-area">
						<?php echo get_field('life__after'); ?>
					</div>

					<div class="request-alt-block targetedscrollpoint" id="request-scroll-to">
						<h2>Request a Free Info Kit</h2>
						
						<div class="toggle-area">
							<a href="/resource-center/request-free-info-kit/"><img src="../../wp-content/themes/north-american-spine/assets/img/brochureback.png" alt="#"><img src="../../wp-content/themes/north-american-spine/assets/img/brochurefront.png" alt="North American Spine"></a>
							<a href="/resource-center/request-free-info-kit/" class="btn-orange invert"><div></div>Get Information Kit</a>
						</div>
					</div>

				</div>
				
			</div>

		</section>

		<section class="each-block-all info testimony-info-block">

			<?php //if (get_field('testimonial_block_title') != ''): ?>

			

			<div class="left-50">
				<div class="each-block-lock targetedscrollpoint" id="testimony-scroll-to">
					<h2><?php the_title(); ?> Testimonial</h2>

					<?php 

						$post_object = get_field('featured_testimonial');
						$post = $post_object;
						setup_postdata($post); 

					?>

					<div class="patient-data">

						<?php 

							$thumb_id = get_post_thumbnail_id();
							$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
							$thumb_url = $thumb_url_array[0];
							$image_alt = get_post_meta( $thumb_id, '_wp_attachment_image_alt', true);
						?>
						<?php if ($thumb_url_array[1] != 48): ?>
							<img src="<?php echo $thumb_url; ?>" alt="<?php echo $image_alt; ?>">
						<?php endif ?>


						<div class="patient-info">
							<h4><?php echo get_field('testimonial_block_patient_name', $post->ID); ?></h4>
							
							<?php 
								$cond = get_field('testimonial_associated_condition', $post->ID);
								$proc = get_field('testimonial_page_link', $post->ID);
							?>
							<p>Condition: <a href="<?php the_permalink( $cond->ID ); ?>"><?php echo $cond->post_title; ?></a></p>
							<p>Procedure: <a href="<?php the_permalink( $proc->ID ); ?>"><?php echo $proc->post_title; ?></a></p>
							<?php if (get_field('testimonial_block_hometown', $post->ID) != ''): ?>
								<p>Hometown: <span><?php echo get_field('testimonial_block_hometown', $post->ID) ?></span></p>
							<?php endif ?>
						</div>
						
					</div>
					<h6 class="quote">“<?php echo get_field('testimonial_block_quote', $post->ID); ?>”</h6>
					<h4 class="lifestyle"><?php echo get_field('testimonial_block_subheader', $post->ID); ?></h4>
					<p><?php echo get_field('testimonial_block_content', $post->ID); ?></p>
					<div class="center-btns">

						<a href="<?php the_permalink( $post->ID ); ?>" class="btn-orange first-space"><div></div>Read Full Story</a>
						<a href="/testimonials" class="btn-orange hidden-md"><div></div>See All Testimonials</a>
					</div>
					<?php wp_reset_postdata(); ?>
				</div>

			</div>	

			<div class="right-50">
				<div class="each-block-info">
					<h2>Connect With A Patient</h2>
					
					<div class="toggle-area">
						<p>Some of our former patients have been kind enough to offer to speak with back and neck pain sufferers who are looking into treatment options. We encourage you to take them up on it! After we determine a treatment plan, we’ll connect you with a past patient who had a similar condition and procedure.</p>
						<a href="/connect-with-a-patient" class="btn-orange invert"><div></div>Connect with a Patient</a>
					</div>
				</div>
				
			</div>
		</section>

		<div class="nav-dots">
			<ul>
				<li data-target="symptoms-scroll-to"><div class="fade-slide">Potential benefits of <?php the_title(); ?></div></li>
				<li data-target="causes-scroll-to"><div class="fade-slide">Details of <?php the_title(); ?></div></li>
				<li data-target="prep-scroll-to"><div class="fade-slide">Preparing for <?php the_title(); ?></div></li>
				<li data-target="life-scroll-to"><div class="fade-slide"><?php the_title(); ?> Recovery</div></li>
			</ul>
		</div>

	</div>
<?php 		
	// End of the loop.
	endwhile;
?>

<?php get_footer(); ?>

