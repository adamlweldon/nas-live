/**
 * header
 * @type {{init}}
 */
var header = (function () {
    var events = {},
        isDocked = false,
        selector = jQuery('.header'),
        dockYPos = 76;

    var dock = function () {
        var yDistance = jQuery(window).scrollTop();

        if (yDistance >= dockYPos) {
            if (isDocked) {
                return false;
            }

            isDocked = true;
            selector.addClass('fixed');
            triggerEvents();
        }
        else {
            if (!isDocked) {
                return false;
            }

            isDocked = false;
            triggerEvents();
            selector.removeClass('fixed');
        }
    };

    var triggerEvents = function () {
        if (isDocked && events.onDocked) {
            events.onDocked();
        }

        if (!isDocked && events.onUnDocked) {
            events.onUnDocked();
        }
    };

    var attachEvents = function (evts) {
        events = evts;

        return this;
    };

    var init = function () {
        dock();
        triggerEvents();

        jQuery(window).scroll(function () {
            dock();
        });
    };

    return {
        init: init,
        attachEvents: attachEvents
    }
})();

/**
 * contactForm
 * @type {{init}}
 */
var contactForm = (function () {
    var base = jQuery('header .contact-form'),
        head = base.find('.head'),
        content = base.find('.content'),
        foot = base.find('.foot');

    var open = function () {
        head.removeClass('show-phone-icon');
        content.slideDown(200);
    };

    var close = function () {
        content.slideUp(200);
        head.addClass('show-phone-icon');
    };

    var toggle = function () {
        content.slideToggle(200);
    };

    var init = function () {
        head.find('.left').bind('click', toggle);
        foot.bind('click', toggle);

        header
            .attachEvents({
                onDocked: function () {
                    close();
                },
                onUnDocked: function () {
                    open();
                }
            })
            .init()
        ;
    };

    return {
        init: init
    }
})();

jQuery(function () {
    contactForm.init();
});
