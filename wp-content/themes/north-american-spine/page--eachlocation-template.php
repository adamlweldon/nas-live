<?php
/**
 * Template Name: Each Location Template Page
 *
 */
?>

<?php get_header(); ?>

<?php 
	// Start the loop.
	while ( have_posts() ) : the_post();
?>

	<div class="container each-loc" id="conditions">
		<section class="each-block-all" id="pinned-lock-1">
			<div class="left-50">
				<div class="each-block-lock first-lock alt-lock">
					<!-- <div class="condition-content-left"> -->
						<h1><?php the_title(); ?></h1>
						<h4>Scottsdale, AZ</h4>
						<ul class="address">
							<li><address>Scottsdale Liberty Hospital<br>17500 N Permimeter Dr.<br>Scottsdale, AZ 88255</address></li>
							<li class="phone"><a href="tel:#">555.555.5555</a></li>
						</ul>
						<p class="italics">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						<div class="orange-grad">Schedule Consult</div>
					<!-- </div> -->
					<!-- <div class="right-img"> -->
					
					<?php 
						// $supportingimg = get_field('each_condition_what_is_image');
						// $actualimage =  wp_get_attachment_url($supportingimg);
						// $image_alt = get_post_meta( $supportingimg, '_wp_attachment_image_alt', true);	
					?>

						<!-- <img src="<?php echo $actualimage; ?>" alt="<?php echo $image_alt; ?>"> -->
					<!-- </div> -->
				</div>
			</div>	
			
			<div class="right-50">
				<div class="each-block-info targetedscrollpoint" id="symptoms-scroll-to">
					<h2>Plan Your Trip Now</h2>
					
					<div class="toggle-area">
						<h4>A Solution Worth Traveling For!</h4>
						<h4 class="location-icon"><?php the_title(); ?> Overview</h4>
						<p class="with-icon">Scottsdale Liberty Hospital is the first hospital in the country dedicated exclusively to the treatment of chronic pain. Founded on the belief that chronic pain should be viewed as a disease state, not a symptom to be dismissed, Scottsdale Liberty Hospital is one of the country’s premiere destinations for compassionate, technologically advanced care.</p>
					</div>
					<img class="fill-it" src="/wp-content/themes/north-american-spine/inc/images/Liberty.jpg" alt="<?php echo the_title(); ?>">
					<h2 class="spaces targetedscrollpoint" id="causes-scroll-to">Patients & Visitors</h2>
					
					<div class="toggle-area">
						<h4>More information about this location, it's doctors, and its accredations.</h4>
						<h5>Doctors</h5>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						<a href="#" class="btn-orange invert"><div></div>Learn More</a>
						<h5>Accredations</h5>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						<a href="#" class="btn-orange invert"><div></div>Learn More</a>
						<h5>Hotels</h5>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						<a href="#" class="btn-orange invert"><div></div>Learn More</a>
					</div>
				</div>

			</div>

		</section>

		<section class="each-block-all info testimony-info-block">

			<?php if (get_field('testimonial_block_title') != ''): ?>
			<div class="left-50">
				<div class="each-block-lock targetedscrollpoint" id="testimony-scroll-to">
					<h2><?php echo get_field('testimonial_block_title'); ?></h2>
					<div class="patient-data">

						<?php 
							$testimg = get_field('testimonial_block_image');
							$actualtestimage =  wp_get_attachment_url($testimg);
							$image_test_alt = get_post_meta($testimg, '_wp_attachment_image_alt', true);	
						?>
						
						<?php if ($actualtestimage != ''): ?>
							<img src="<?php echo $actualtestimage; ?>" alt="<?php echo $image_test_alt; ?>">
						<?php endif ?>

						<div class="patient-info">
							<h4><?php echo get_field('testimonial_block_patient_name'); ?></h4>
							
							<?php 
								$getesttitle = url_to_postid(get_field('testimonial_page_link'));
								$getesttitle =	get_the_title($getesttitle);
							?>

							<p>Procedure: <a href="<?php echo get_field('testimonial_page_link'); ?>"><?php echo $getesttitle; ?></a></p>
							<p>Hometown: <span><?php echo get_field('testimonial_block_hometown'); ?></span></p>
						</div>
						
					</div>
					<h6 class="quote">“<?php echo get_field('testimonial_block_quote'); ?>”</h6>
					<h4 class="lifestyle"><?php echo get_field('testimonial_block_subheader'); ?></h4>
					<p><?php echo get_field('testimonial_block_content'); ?></p>
					<div class="center-btns">
						<a href="<?php echo get_field('testimonial_block_link_url'); ?>" class="btn-orange first-space"><div></div><?php echo get_field('testimonial_block_link_text'); ?></a>
						<a href="/testimonials" class="btn-orange hidden-md"><div></div>See All Testimonials</a>
					</div>
					
				</div>
			</div>	
			<?php else: ?>
			<div class="left-50">
				
			</div>
			<?php endif ?>
			
			<!--  -->
			<!-- update links -->
			<!--  -->
			<!--  -->
			<!-- update links -->
			<!--  -->
			<!--  -->
			<!-- update links -->
			<!--  -->
			<!--  -->
			<!-- update links -->
			<!--  -->

			<div class="right-50">
				<div class="each-block-info">
					<h2>Meet With a Back Surgery Specialist</h2>
					
					<div class="toggle-area">
						<!-- <a href="#" class="btn-orange invert"><div></div>Get In Touch</a> -->
					</div>
				</div>
				
			</div>
		</section>

		<div class="nav-dots">
			<ul>
				<li data-target="symptoms-scroll-to"><div class="fade-slide">Plan Your Trip Now</div></li>
				<li data-target="causes-scroll-to"><div class="fade-slide">Patients & Visitors</div></li>
				<li data-target="testimony-scroll-to"><div class="fade-slide">Map</div></li>
			</ul>
		</div>

	</div>
<?php 		
	// End of the loop.
	endwhile;
?>

<?php get_footer(); ?>

