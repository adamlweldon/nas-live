<?php
/**
 * Template Name: Procedures Template Page
 *
 */
?>

<?php get_header(); ?>

<?php 
	// Start the loop.
	while ( have_posts() ) : the_post();
?>
	<div class="bluearea"></div>
	<div class="container" id="conditions">
		<section class="each-block-all" id="pinned-lock-1">
			<div class="left-50">
				<div class="each-block-lock first-lock">
					<h1><?php the_title(); ?></h1>
					<p><?php the_content(); ?></p>
				</div>
			</div>	
			
			<div class="right-50">
				<div class="each-block-info">

					<?php 
						$thumb_id = get_post_thumbnail_id();
						$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
						$thumb_url = $thumb_url_array[0];
						$image_alt = get_post_meta( $thumb_id, '_wp_attachment_image_alt', true);

						if (isset($thumb_url)) {
					?>
							<img src="<?php echo $thumb_url; ?>" alt="<?php echo $image_alt; ?>">
					<?php
						}
					?>

				</div>
				
			</div>

		</section>

		<?php if( have_rows('each_focus') ): ?>
			<?php while( have_rows('each_focus') ): the_row(); ?>
				<h2 class="proc-cond-toggle visible-xs visible-sm"><?php echo get_sub_field('focal_title'); ?></h2>
				<section id="back-pain-<?php echo get_row_index(); ?>" class="each-block-all toggle-area-alt">
					<div class="left-50">
						<div class="each-block-lock lock-it">
							<h1 class="hidden-xs hidden-sm"><?php echo get_sub_field('focal_title'); ?></h1>
							<p><?php echo get_sub_field('focal_content'); ?></p>
							<?php if (get_sub_field('focal_category')[0] != ''): ?>
								<div class="mob-center-content">
									<a href="<?php echo get_category_link(get_sub_field('focal_category')[0]); ?>" class="btn-orange" tabindex="0"><div></div>Learn More</a>
								</div>
							<?php endif ?>
							
						</div>
					</div>	
					
					<div class="right-50">
						<div class="each-block-info">
							<?php //if (get_sub_field('list_conditions_section_title') != ''): ?>
								<h2 class="no-line no-toggle"><?php echo get_sub_field('right_side_focal_title'); ?></h2>
							<?php //endif ?>
							<!-- <div class="toggle-area"> -->
								
								<ul class="no-line">

									<?php 
										$getcat_id = get_sub_field('focal_category')[0];
										

										global $post;

										$runme = get_posts(array( 
										    'post_type' => 'nas-procedures',
										    'showposts' => -1,
										    'tax_query' => array(
										        array(
										            'taxonomy' => 'category',
										            'terms' => $getcat_id,
										            'field' => 'term_id',
										        ),

										    ),
										    'orderby' => 'title',
    										'order'   => 'ASC'

										));

										foreach ($runme as $post) {
									
											setup_postdata($post);

											echo '<li><a href="'.esc_url( get_permalink()).'">' . get_the_title() . '</a></li>';

											wp_reset_postdata();
										}

										wp_reset_postdata();

									?>
									
								</ul>
							<!-- </div> -->
						</div>
						
					</div>
				</section>

			<?php endwhile ?>
		<?php endif ?>

		<h2 class="proc-cond-toggle visible-xs visible-sm">Request a Free Info Kit</h2>
		<section class="each-block-all info global-info toggle-area-alt" id="request-info-block">
			<div class="left-50">
				
			</div>	
			<div class="right-50">
				<div class="request-alt-block targetedscrollpoint updatefix" id="request-scroll-to">
					<h2 class="hidden-xs hidden-sm">Request a Free Info Kit</h2>
					
					<div class="toggle-area">
						<a href="/resource-center/request-free-info-kit/"><img src="../../wp-content/themes/north-american-spine/assets/img/brochureback.png" alt="#"><img src="../../wp-content/themes/north-american-spine/assets/img/brochurefront.png" alt="Request Free Info Kit"></a>
						<a href="/resource-center/request-free-info-kit/" class="btn-orange invert"><div></div>Get Information Kit</a>
					</div>
				</div>
			</div>
		</section>

		<div class="nav-dots">
			<ul>
				<li data-target="pinned-lock-1"><div class="fade-slide">List of procedures</div></li>
				<?php if( have_rows('each_focus') ): ?>
					<?php while( have_rows('each_focus') ): the_row(); ?>
						<li data-target="back-pain-<?php echo get_row_index(); ?>"><div class="fade-slide"><?php echo get_sub_field('focal_title'); ?></div></li>
					<?php endwhile ?>
				<?php endif ?>
				<li data-target="request-info-block"><div class="fade-slide">Request Info Kit</div></li>

			</ul>
		</div>

	</div>
<?php 		
	// End of the loop.
	endwhile;
?>


<?php get_footer(); ?>