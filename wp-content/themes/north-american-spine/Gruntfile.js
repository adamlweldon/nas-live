module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		sass: {
			dev: {
				files: {
					'style.css' : 'sass/style.scss',
				},
				options: {
					loadPath: 'sass/',
					style: 'compressed'
				}
			},
			prod: {
				files: {
					'style.css' : 'sass/style.scss',
				},
                options: {
                    sourceMap: true,
                    outputStyle: 'compressed'
                }
			}
		},
        postcss: {
            options: {
                map: true,
                processors: [
                    require('autoprefixer')
                ]
            },
            dist: {
                src: 'style.css'
            }
		},
		uglify: {
			options: {
				sourceMap: true,
			},
			my_target: {
				files: [ { 
					expand: true,
					cwd: 'js',
					src: '*.js',
					dest: 'js/prod',
					ext: '.min.js',
				 } ]
			}
		},
		watch: {
			dev: {
				files: ['sass/*.scss', 'sass/_partials/*.scss'],
				tasks: ['sass:dev', 'postcss']
			},
			prod: {
				files: ['sass/*.scss', 'sass/_partials/*.scss'],
				tasks: ['sass:prod', 'postcss', 'uglify']
			}
		}
	});

	grunt.loadNpmTasks('grunt-postcss');
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.registerTask('dev',['watch:dev']);
	grunt.registerTask('prod', ['watch:prod']);

};