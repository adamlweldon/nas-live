<?php
/**
 * Template Name: Research Template
 *
 */
?>

<?php get_header(); ?>

<?php 
	// Start the loop.
	while ( have_posts() ) : the_post();
?>
	<div class="bluearea"></div>
	<div class="container generic research" id="conditions">
		<section class="each-block-all" id="pinned-lock-1">
			<div class="left-50">
				<div class="each-block-lock first-lock alt-lock">
					<div class="generic-content-left">
					<h1><?php the_title(); ?></h1>
					<p><?php the_content(); ?></p>
					</div>
				</div>
			</div>	
			
			<div class="right-50">
				<div class="each-block-info">
					<h2 class="no-toggle"><?php echo get_field('research_right_title'); ?></h2>
					<h4><?php echo get_field('research_right_sub_title'); ?></h4>
					<?php if( have_rows('research_articles') ): ?>
						<?php while( have_rows('research_articles') ): the_row(); ?>
							<article>
								<h3><?php echo get_sub_field('research_article_title'); ?></h3>
								
								<!-- if subheader exists -->
								<?php if (get_sub_field('research_article_source') != ''): ?>
									<h4 class="subheader"><?php echo get_sub_field('research_article_source'); ?></h4>
								<?php endif ?>
							
								<!-- if author exists -->
								<?php if (get_sub_field('research_article_authors') != ''): ?>
									<p class="authors">Authors: <?php echo get_sub_field('research_article_authors'); ?></p>
								<?php endif ?>

								<!-- if type exists -->
								<?php if (get_sub_field('research_article_type') != ''): ?>
									<p class="type"><?php echo get_sub_field('research_article_type'); ?></p>
								<?php endif ?>

								<?php if (get_sub_field('research_article_content') != ''): ?>
									<p><?php echo get_sub_field('research_article_content'); ?></p>
								<?php endif ?>

								<!-- if button exists -->
								<?php if (get_sub_field('research_article_file') != ''): ?>	
									<a href="<?php echo get_sub_field('research_article_file'); ?>" target="_blank">Read More</a>
								<?php endif ?>
							</article>
						<?php endwhile ?>
					<?php endif ?>	

				</div>
				
			</div>

		</section>

		<?php if (!is_page('spine-pain-doctors') && !(is_page('research'))): ?>
			
			<div class="nav-dots">
				<ul>
					<?php if( have_rows('each_content_block') ): ?>
						<?php while( have_rows('each_content_block') ): the_row(); ?>
							<li data-target="generic-sectionscroll-<?php echo get_row_index(); ?>"><div class="fade-slide"><?php echo get_sub_field('each_content_block_title'); ?></div></li>
						<?php endwhile ?>
					<?php endif ?>	

				</ul>
			</div>
		<?php endif ?>

	</div>
<?php 		
	// End of the loop.
	endwhile;
?>

<?php get_footer(); ?>

