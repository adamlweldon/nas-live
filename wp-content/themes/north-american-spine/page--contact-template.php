<?php
/**
 * Template Name: Contact Template
 *
 */
?>

<?php get_header(); ?>

<?php 
	// Start the loop.
	while ( have_posts() ) : the_post();
?>
	<div class="bluearea"></div>
	<div class="container generic contact" id="conditions">
		<section class="each-block-all" id="pinned-lock-1">
			<div class="left-50">
				<div class="each-block-lock first-lock alt-lock">
					<div class="generic-content-left">
					<h1><?php the_title(); ?></h1>
					<p><?php the_content(); ?></p>
					</div>
				</div>
			</div>	
			
			<div class="right-50">
				<div class="each-block-info">

					<h2 class="spaces targetedscrollpoint">Connect With Us</h2>
					
					<div class="toggle-area contact-options">

						<p>When you contact us at <?php echo get_option('support_phone'); ?> or through the form on this page, you will be given a dedicated patient coordinator to answer all of your questions and help you through the steps to pain relief.</p>

						<h4>To reach a live person, 24 hours a day call:</h4>

						<?php
							if(get_option('support_phone')){ ?>
								<a href="tel:<?php echo get_option('support_phone'); ?>"><i class="phone"></i><?php echo get_option('support_phone'); ?></a>

						<?php } ?>

						<h4>Write to us at:</h4>
						<div class="writetous">
							<i class="letters"></i>
							<div class="address">North American Spine<br>
							10740 N. Central Expy, Ste. 275<br>
							Dallas, TX 75231</div>
						</div>
						<h4>Find Locations</h4>
						<a href="/locations" class="btn-orange invert"><div></div>View Locations</a>
					</div>

				</div>
				
			</div>

		</section>

	</div>
<?php 		
	// End of the loop.
	endwhile;
?>

<?php get_footer(); ?>

