<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package North_American_Spine
 */

get_header(); ?>

	<!-- <div class="container"> -->
	<div id="primary" class="content-area">
		<main id="blog" class="site-main" role="main">

		<?php
		if ( have_posts() ) :   ?>

			<div class="container">
					<?php north_american_spine_blog_header(); ?>
			</div>


			
			<?php 
			// north_american_spine_blog_header();
			?>

			<div class="load-section">
			<?php
			$counter = 1;
			/* Start the Loop */
			while ( have_posts() ) : the_post();
				if ($counter % 2 != 0 ) : ?>
				<div class="container">
					<div class="archive-row">

				<?php endif;

				
				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_format() );
				
				if ($counter % 2 == 0 || $wp_query->post_count == 1) : ?>
					</div>
				</div>
				<?php endif;

				if ($counter == 4 && get_post_type() == 'nas-testimonials' ) : ?>
				<?php dynamic_sidebar( 'testimonial-middle-sidebar' ); ?>
				<?php endif; 
				$counter++;
			endwhile;
			?>

			</div> 
			<div class="loader"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>
			<?php
			$categories = isset($wp_query->get_queried_object()->term_id) ? $wp_query->get_queried_object()->term_id : false; 
			zog_ajax_load_more($categories);
			
		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->
	<!-- </div> -->

<?php
// get_sidebar();
get_footer();
