<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package North_American_Spine
 */

get_header(); ?>
	<div class="container">
	<div id="primary" class="content-area">
		<main id="blog" class="site-main" role="main">

		<?php
		if ( have_posts() ) :

			if ( is_home() && ! is_front_page() ) : ?>
				<!-- <header>
					<h1 class="page-title screen-reader-text"><?php // single_post_title(); ?></h1>
				</header> -->

			<?php
				north_american_spine_blog_header();
			endif;

			?>
				
			<div class="load-section">

			<?php

			$counter = 1;
			/* Start the Loop */
			while ( have_posts() ) : the_post();
				if ($counter % 2 != 0 ) : ?>

					<div class="row">
				<?php endif;
				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_format() );
				// printer(get_post_format());

				
				// printer($counter % 2);
				if ($counter % 2 == 0 || $wp_query->post_count == 1) : ?>
					</div>
				<?php endif;

				if ($counter % 3 == 0) : ?>
					<div class="whole-width"><h1Test></div>
				<?php endif;

				$counter++;
			endwhile;

			?> 
			
			</div> 
			<div class="loader"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>
			<?php

			zog_ajax_load_more();
			// the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->
	</div><!-- .container -->

<?php
// get_sidebar();
get_footer();
