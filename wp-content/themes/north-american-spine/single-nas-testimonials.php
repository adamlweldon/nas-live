<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package North_American_Spine
 */

get_header(); ?>
    
    <div class="container nas-testimonials">
        <!-- <div class="row"> -->
            <?php north_american_spine_blog_header(); ?>
        <!-- </div> -->


    <div class="row">
        <div id="primary" class="content-area col-md-9 ">
            <main id="testimonial" class="site-main" role="main">
            <?php ( get_categories( array('taxonomy' => 'link_category') ) ); ?>
            <?php
            while ( have_posts() ) : the_post();
                // var_dump(get_post_format());
                get_template_part( 'template-parts/content', get_post_format() );

                // the_post_navigation();

                // If comments are open or we have at least one comment, load up the comment template.
                if ( comments_open() || get_comments_number() ) :
                    comments_template();
                endif;

            endwhile; // End of the loop.
            ?>

            </main><!-- #main -->
        </div><!-- #primary -->

    
        <aside id="secondary" class="widget-area col-md-3 related-posts" role="complementary">
            <?php dynamic_sidebar( 'testimonial-sidebar' ); ?>
        </aside><!-- #secondary -->
    </div>
</div>
<?php
get_footer();
