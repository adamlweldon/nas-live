<?php
/**
 * Template Name: Thank You Completion Template
 *
 */
?>

<?php get_header(); ?>

<?php
// Start the loop.
while ( have_posts() ) : the_post();
    ?>
<!--    <div class="bluearea"></div>-->
    <div class="container generic thank-you" id="conditions">
        <section class="each-block-all" id="pinned-lock-1">
            <!--<div class="left-50">
                <div class="each-block-lock first-lock alt-lock">
                    <div class="generic-content-left">
                        <h1></h1>

                        <div id="webinar" class="thank-you-messages">
                            <?php /*echo get_field('webinar_thank_you'); */?>
                        </div>
                        <div id="weblead" class="thank-you-messages">
                            <?php /*echo get_field('web_lead_thank_you'); */?>
                        </div>
                        <div id="kitrequest" class="thank-you-messages">
                            <?php /*echo get_field('kit_request_thank_you'); */?>
                        </div>
                    </div>
                </div>
            </div>
-->
<!--            <div class="right-50">-->
                <div class="each-block-info" style="margin: 20px;">
                    <h2><?php the_title(); ?></h2>
                    <div class="toggle-area">
                        <h4>
                            Step 2 of 2 complete
                            <progress value="100" max="100" style="display:block; width: 100%;"></progress>
                        </h4>
                        <p>Your Patient Care Manager will reach out to you within one business day to begin the process. In the meantime, we encourage you to explore our website, read patient testimonials, and watch our exclusive webinar below.</p>
                        <br />
                        <p style="text-align: center">
                            <iframe src="https://www.youtube.com/embed/W4apW8wsu94?rel=0" frameborder="0" allowfullscreen></iframe>
                        </p>
                    </div>

                </div>

<!--            </div>-->

        </section>
    </div>
    <?php
    // End of the loop.
endwhile;
?>

<?php get_footer(); ?>

