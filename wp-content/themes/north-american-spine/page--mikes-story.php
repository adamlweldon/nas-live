<?php  /* Template Name: mikes story */?>

<?php get_header("lp");?>
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo get_stylesheet_directory_uri() ?>/landing/mikes-story/style.css" />

    <div id="content" class="nas-landing-page site-container">

        <main class="hero slick-initialized slick-slider">
            <div aria-live="polite" class="slick-list draggable">
                <div class="slick-track" role="listbox">
                    <section class="each-slide slick-slide slick-current slick-active" data-slick-index="0" aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide00">
                        <div class="container">
                            <div class="left-content">
                                <h1>Care for you and <br class="desktop-only">your spine pain.</h1>
                                <h2>“It's changed our lives.”</h2>
                                <a class="btn-orange" data-toggle="modal" data-target="#consult"><div></div>Hear Mike &amp; Jill's Story<i class="fa fa-play-circle-o" aria-hidden="true"></i></a>
                                <p class="actual-patient"><em>Actual Patient</em></p>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </main>

        <section class="cities-section show-on-mobile">
            <ul class="cities">
                <li>Dallas</li>
                <li>Houston</li>
                <li>Detroit</li>
                <li>Nashville</li>
                <li>Minneapolis</li>
                <li>Phoenix</li>
            </ul>
        </section>

        <section class="blue-sub">
            <h2>Minimally Invasive Procedures for Back and Neck Pain</h2>
            <h3>North American Spine Benefits</h3>
            <ul>
                <li>Free evaluation of your MRI &amp; insurance</li>
                <li>Superior concierge-style service</li>
                <li>Affordable financial options</li>
                <li>Most procedures are outpatient</li>
                <li>Lower risk of infection, faster recovery</li>
                <li>Reduced need for repeat procedures</li>
            </ul>
        </section>

        <section class="contact-bar visible-xs visible-sm">
            <p>Call 24/7 or submit the form below. <a href="tel:<?php echo get_option('support_phone'); ?>"><i class="phone"></i></a><a href="tel:<?php echo get_option('support_phone'); ?>"><?php echo get_option('support_phone'); ?></a></p>
        </section>

        <section class="visible-xs visible-sm">
            <div class="ask-expert-static toggled">
                <div class="form-area">
                    <a name="topofformmobile"></a>
                    <?php pgform_lp_main() ?>
                </div>
            </div>
        </section>

        <!-- dnd-procedure -->
        <section class="dnd-procedure">
            <div class="container">
                <div class="left">
                    <h3 class="title">
                        Exclusive provider of the Laser DND Procedure
                    </h3>
                </div>

                <div class="center">
                    <img alt="DND Procedure" src="/wp-content/themes/north-american-spine/landing/mikes-story/img/dnd-procedure.jpg"/>
                </div>

                <div class="right">
                    <h3 class="title">
                        Treats multiple levels<br/> in one procedure
                    </h3>
                </div>

                <div class="show-on-mobile">
                    <h3 class="title">
                        Exclusive Provider of the Laser DND Procedure
                    </h3>

                    <h3 class="title">
                        Treats multiple levels in one procedure
                    </h3>
                </div>
            </div>
        </section>

        <!-- pain-relief -->
        <section class="pain-relief">
            <div class="container">
                <div class="table">
                    <div class="left">
                        <div class="head">
                            <h3 class="title">Have you already tried these steps to pain relief?</h3>
                        </div>

                        <div class="body">
                            <div class="col-1">
                                <div class="step">
                                    <span>1.</span> Visited Your Primary Care Physician
                                </div>

                                <div class="step">
                                    <span>2.</span> Received an MRI/CT Scan
                                </div>

                                <div class="step">
                                    <span>3.</span> Received a Diagnosis
                                </div>
                            </div>

                            <div class="col-2">
                                <div class="step">
                                    <span>4.</span> Treated Your Pain with Conservative Methods:

                                    <ul>
                                        <li>Pain medication</li>
                                        <li>Massage/Chiropractor</li>
                                        <li>Acupuncture</li>
                                        <li>Physical therapy</li>
                                        <li>Spinal injections</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="right">
                        <div class="head">
                            <h3 class="title">
                                Still in pain? You could be a candidate for one of our minimally invasive procedures.
                            </h3>
                        </div>

                        <div class="body">
                            <p>
                                <b>If you have an MRI/CT scan:</b> Contact us at <strong><?php echo get_option('support_phone'); ?></strong> or <a href="/resource-center/webinar/" class="invert button mobile" data-target="#mrict-modal">submit the form</a> on this
                                page and
                                we'll review it free of charge, providing you with a treatment plan that is customized
                                to
                                your unique condition.
                            </p>

                            <p>
                                <b>If you don't have an MRI/CT scan:</b> Your first step will be to make an appointment
                                with your
                                Primary Care Physician.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- info-bar -->
        <section class="info-bar">
            <p>
                We're available 24/7. Call us anytime.
            </p>

            <p class="call">
                <a href="tel:<?php echo get_option('support_phone'); ?>"><img alt="Call us anytime" src="/wp-content/themes/north-american-spine/landing/mikes-story/img/info-bar-icon.png"/></a>
                <a href="tel:<?php echo get_option('support_phone'); ?>"><?php echo get_option('support_phone'); ?></a>
            </p>
        </section>

        <!-- pushes -->
        <section class="pushes">
            <div class="container">
                <div class="push">
                    <i class="dna icon"></i>

                    <div class="body">
                        <h3 class="title">
                            Conditions Treated
                        </h3>

                        <ul>
                            <li>Sciatica</li>
                            <li>Degenerative Disc Disease</li>
                            <li>Bulging or Herniated Disc</li>
                            <li>Pinched Nerve</li>
                            <li>Spinal Stenosis</li>
                            <li>And many more</li>
                        </ul>
                    </div>
                </div>

                <div class="push invert">
                    <i class="heartbeat icon"></i>

                    <div class="body">
                        <h3 class="title">
                            Common Symptoms
                        </h3>

                        <ul>
                            <li>Back or neck pain with or without radiating leg/arm pain</li>
                            <li>Back pain that involves the hips, buttocks or legs</li>
                            <li>Weakness and/or numbness in the limbs</li>
                        </ul>
                    </div>
                </div>

                <div class="push">
                    <i class="care icon"></i>

                    <div class="body">
                        <h3 class="title">
                            Unique Benefits
                        </h3>

                        <ul>
                            <li>Smaller incisions and minimal scar tissue</li>
                            <li>Advanced technology including HD camera and laser</li>
                            <li>Majority outpatient procedures/ minimal hospital stay</li>
                            <li>Quicker recovery time</li>
                        </ul>
                    </div>
                </div>

                <div class="push invert">
                    <i class="file icon"></i>

                    <div class="body">
                        <h3 class="title">
                            By The Numbers
                        </h3>

                        <ul>
                            <li>More than 8,000 patient success stories</li>
                            <li>More than 95% of patients would recommend to a friend</li>
                            <li>$0 – cost for an MRI Review</li>
                            <li>Incisions as small as 3 millimeters</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="review">
                <a href="#topofform" class="button desktop">
                    Get Your Free MRI/CT Review
                </a>
                <a href="/resource-center/webinar/" class="btn-orange invert button mobile" data-target="#mrict-modal">
                    Get Your Free MRI/CT Review
                </a>
            </div>
        </section>

        <section class="info">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 gray">
                        <div class="half video">
                            <div class="video-container">
                                <div id="yt-home" style="background: url('/wp-content/themes/north-american-spine/landing/shared/img/webinar-new.jpg');"></div>
                                <div id="yt-player"></div>
                                <div class="hover-video">
                                    <p>Preview Our Free On-Demand Webinar</p>
                                    <ul>
                                        <li>
                                            <i class="fa fa-pause pause-vid" aria-hidden="true"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-play play-vid" aria-hidden="true"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-repeat repeat-vid" aria-hidden="true"></i>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                            <a href="/resource-center/webinar/" class="btn-orange invert" data-target="#webinar-modal"><div></div>Watch Full Video</a>
                        </div>
                        <div class="half request">
                            <?php dynamic_sidebar( 'nas-info-kit-widget-area' ); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- info-bar -->
        <section class="info-bar info-bar-two">
            <div class="container">
                <p>
                    Learn more about our unparalleled patient care, quick easy process, and more.<br class="hidden-md hidden-lg" />
                    <a href="/wp-content/themes/north-american-spine/landing/shared/NAS_FAQs.pdf" target="_blank">Read Our FAQs >></a>
                </p>
            </div>
        </section>

        <section class="contact-area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-3"></div>
                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-6">
                        <div class="expertimg"></div>
                        <h2>We love your questions!</h2>
                        <p>If you would like to talk to a spine expert, please call us: <a href="tel:<?php echo get_option('support_phone'); ?>" class="white"><?php echo get_option('support_phone'); ?></a>.</p>
                        <p>We're available 24/7.</p>
                    </div>
                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-3"></div>
                </div>
            </div>
            <div class="close-contact">
                <div class="rotate-orange <?php if (is_front_page()) {echo "toggled";} ?>">
                    <div></div>
                    <div></div>
                </div>
            </div>
        </section>
        <section class="toggle-contact <?php if (is_front_page()) {echo "toggled";} ?>">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <?php pgform_lp_main() ?>
                    </div>
                </div>
            </div>
        </section>

        <!-- footer -->
        <footer id="colophon" class="site-footer" role="contentinfo">
            <div class="sitemap">
                <ul class="policies">
                    <li><a href="/user-agreement/" target="_blank">User Agreement</a></li>
                    <li><a href="/privacy-policy/" target="_blank">Privacy Policy</a></li>
                </ul>
            </div>
            <div class="site-info">
                <div class="container">
                    <p>&copy; <?php echo date('Y'); ?> North American Spine. All rights reserved.<br/>Dallas · Detroit · Houston · Minneapolis · Nashville · Phoenix<br/>Please note: Treatment plans are customized to meet your specific pathology. Patient experiences may vary.</p>
                </div>
            </div><!-- .site-info -->
        </footer><!-- #colophon -->

    </div>

</div><!-- #page -->

<!-- Main Modal Popup -->
<div id="consult" tabindex="-1" role="dialog" aria-labelledby="consult modal" class="">
    <div class="modal-form home-modal">
        <div class="close-it"></div>
        <iframe src="https://www.youtube.com/embed/6jDTiLTnc1M?rel=0&amp;controls=0&amp;showinfo=0" allowfullscreen="" data-attr="https://www.youtube.com/embed/GFQU5MwDznE?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0"></iframe>
    </div>
</div>

<!-- Webinar Modal Popup -->
<div id="webinar-modal" tabindex="-1" role="dialog" aria-labelledby="consult modal" class="">
    <div class="modal-form home-modal">
        <div class="close-it"></div>
        <h3>Webinar</h3>
        <?php pgform_lp_webinar() ?>
    </div>
</div>

<!-- Get Info Kit Modal Popup -->
<div id="freekit" tabindex="-1" role="dialog" aria-labelledby="consult modal" class="">
    <div class="modal-form home-modal">
        <div class="close-it"></div>
        <h3>Request a Free Info Kit</h3>
        <?php pgform_infokit_generic() ?>
    </div>
</div>

<!-- Get Your Free MRI/CT Review Modal Popup -->
<div id="mrict-modal" tabindex="-1" role="dialog" aria-labelledby="consult modal" class="">
    <div class="modal-form home-modal">
        <div class="close-it"></div>
        <h3>Get Your Free MRI/CT Review</h3>
        <?php pgform_lp_main() ?>
    </div>
</div>

<?php wp_footer(); ?>

<script>
    $('a[data-target="#freekit"]').on('click', function(ev){
        // console.log('open it');

        $('#freekit').addClass('visible-time');
        ev.preventDefault();
        return false;

    });

    $('a[data-target="#webinar-modal"]').on('click', function(ev){

        $('#webinar-modal').addClass('visible-time');
        ev.preventDefault();
        return false;

    });

    $('a[data-target="#consult"]').on('click', function(ev){

        $('#consult').addClass('visible-time');
        ev.preventDefault();
        return false;

    });
</script>

<script type='text/javascript'>
    /* <![CDATA[ */
    var nas_yt_api = {"video":"https:\/\/www.youtube.com\/embed\/stu5hGc6T3A"};
    /* ]]> */
</script>
<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/js/ytiframe.js'></script>

</body>
</html>