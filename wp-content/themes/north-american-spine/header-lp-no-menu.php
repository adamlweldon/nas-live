<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>

	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo get_stylesheet_directory_uri() ?>/landing/header-lp-no-menu/style.css" />
</head>

<body <?php body_class(); ?>>

<!-- roi tracker -->
<script type="text/javascript">
	if(typeof ROITracker==='undefined'){window.ROIStorage={};ROIStorage.q=[];window.ga=function(){ROIStorage.q.push(arguments)};var ga=window.ga;window.ga.q=window.ga.q||[];ROIStorage.roiq=[];ROIStorage.analyticsJsNotLoaded=true;window.ga.q.push([function(){var e;ROIStorage.realGa=window.ga;ROIStorage.analyticsJsNotLoaded=false;window.ga=function(){if(typeof arguments[0]==='function'){ROIStorage.realGa(arguments)}else{ROIStorage.q.push(arguments)}};ROIStorage.roiq.push=function(){ROIStorage.realGa.apply(window,arguments)};for(e=0;e<ROIStorage.roiq.length;e+=1){ROIStorage.realGa.call(window,ROIStorage.roiq[e])}}])}ROIStorage.gaq=ROIStorage.gaq||[];window._gaq={push:function(){var e;for(e=0;e<arguments.length;e++){ROIStorage.gaq.push(arguments[e])}}};var _gaq=window._gaq;
	(function () {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://c772931.ssl.cf2.rackcdn.com/gate.js' : 'http://c772931.r62.cf2.rackcdn.com/gate.js');
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(ga, s);
	})();
</script>

<!--Call Tracking Metrics script - https://calltrackingmetrics.com/-->
<script async src="//18862.tctm.co/t.js"></script>

<!-- Optimizely Script -->
<script src="//cdn.optimizely.com/js/73151603.js"></script>

<div id="page" class="site landing-page" data-page="landing">

	<a name="topofform"></a>

	<div class="top_bar">
		<p>We’re available 24/7. Call us anytime. <a href="tel:<?php echo get_option('support_phone'); ?>"><i class="phone"></i><span class="hidden-xs"><?php echo get_option('support_phone'); ?></span></a></p>
	</div>

	<header class="site-header" role="banner">

		<div class="site-branding">
			<div class="container">
				<a href="/" class="logo"></a>
			</div>
			<!-- <div class="fa fa-search search-alt hidden-xs hidden-sm" id="search"></div> -->
			<div class="visible-sm visible-xs phone-spot">
				<a href="tel:<?php echo get_option('support_phone'); ?>"><i class="phone"></i></a>
			</div>
		</div><!-- .site-branding -->

		<nav class="main-navigation" role="navigation">

			<div class="container">

			<!-- <div class=""> -->
			<div class="menu-alt-main-menu-container">
				<h3>On-Demand Video Access</h3>
			</div>
			<div class="ask-expert visible-sm visible-xs">
				<div class="toggle-abs-section"></div>
				<div class="contactus-icon"></div>
				<h5>Know Your Options</h5>
				<p>Get a free review of your MRI.</p>
				<div class="toggle"></div>
				<div class="form-area">
					<?php pgform_lp_main(); ?>
					<div class="toggle-close">
						<div></div>
						<div></div>
					</div>
				</div>
			</div>
			<div class="ask-expert hidden-xs hidden-sm toggled">
				<div class="toggle-abs-section"></div>
				<div class="contactus-icon"></div>
				<h5>Know Your Options</h5>
				<p>Get a free review of your MRI.</p>
				<div class="toggle"></div>
				<div class="in-phone">
					<i class="phone"></i>
					<div>
						<p>We're available 24/7. Call Us Anytime</p>
						<a href="tel:<?php echo get_option('support_phone'); ?>"><?php echo get_option('support_phone'); ?></a>
					</div>
				</div>
				<div class="form-area">
					<?php pgform_lp_main() ?>

					<div class="toggle-close">
						<div></div>
						<div></div>
					</div>
				</div>
			</div>
			<!-- </div> -->

			</div>

		</nav><!-- #site-navigation -->

	</header><!-- #masthead -->
