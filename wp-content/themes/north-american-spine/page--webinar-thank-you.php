<?php  /* Template Name: webinar thank you */?>

<?php get_header("lp-no-menu");?>

<link rel="stylesheet" type="text/css" media="screen" href="<?php echo get_stylesheet_directory_uri() ?>/landing/webinar-thank-you/style.css" />


<div id="content" class="nas-landing-page site-container">

    <main class="hero slick-initialized slick-slider">
        <div aria-live="polite" class="slick-list draggable">
            <div class="slick-track" role="listbox">
                <section class="each-slide slick-slide slick-current slick-active" data-slick-index="0" aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide00">
                    <div class="container">
                        <div class="left-content">
                            <h1>"I was blown away<br class="desktop-only"> by the results"</h1>
                            <div class="btn-orange trigger-consult" data-toggle="modal" data-target="#consult"><div></div>Hear Marsha's Story<i class="fa fa-play-circle-o" aria-hidden="true"></i></div>
                            <p class="actual-patient"><em>Actual Patient</em></p>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </main>

    <section class="cities-section show-on-mobile">
        <ul class="cities">
            <li>Dallas</li>
            <li>Houston</li>
            <li>Detroit</li>
            <li>Nashville</li>
            <li>Minneapolis</li>
            <li>Phoenix</li>
        </ul>
    </section>

    <section class="blue-sub">
        <h2>Looking for back or neck pain relief? We can help.<br/></h2>
        <h3><em>Provide your information and we'll be in touch.</em></h3>
        <p>North American Spine is the leader in minimally invasive spine care. With facilities across the US, we treat all kinds of back or neck pain, and we do it in the most hassle-free, individualized way possible. Also, the majority of our procedures can be done without an overnight hospital stay, and recovery is much quicker than recovery after large, open procedures. most patients are back to work or play within a couple days.</p>
    </section>

    <section class="contact-bar visible-xs visible-sm">
        <p>Call 24/7 or submit the form below. <a href="tel:<?php echo get_option('support_phone'); ?>"><i class="phone"></i></a><a href="tel:<?php echo get_option('support_phone'); ?>"><?php echo get_option('support_phone'); ?></a></p>
    </section>

    <section class="visible-xs visible-sm">
        <div class="ask-expert-static toggled">
            <div class="form-area">
                <a name="topofformmobile"></a>
                <?php pgform_lp_main() ?>
            </div>
        </div>
    </section>

    <!-- dnd-procedure -->
    <section class="dnd-procedure">
        <div class="container">
            <div class="left">
                <h3 class="title">
                    Exclusive Provider of the Laser DND Procedure
                </h3>
            </div>

            <div class="center">
                <img alt="DND Procedure" src="/wp-content/themes/north-american-spine/landing/shared/img/dnd-procedure.jpg"/>
            </div>

            <div class="right">
                <h3 class="title">
                    Treats multiple levels<br/> in one procedure
                </h3>
            </div>

            <div class="show-on-mobile">
                <h3 class="title">
                    Exclusive Provider of the Laser DND Procedure
                </h3>

                <h3 class="title">
                    Treats multiple levels in one procedure
                </h3>
            </div>
        </div>
    </section>

    <!-- info-bar -->
    <section class="info-bar">
        <p>
            We're available 24/7. Call us anytime.
        </p>

        <p class="call">
            <a href="tel:<?php echo get_option('support_phone'); ?>"><img alt="Call us anytime" src="/wp-content/themes/north-american-spine/landing/shared/img/info-bar-icon.png"/></a>
            <a href="tel:<?php echo get_option('support_phone'); ?>"><?php echo get_option('support_phone'); ?></a>
        </p>
    </section>

    <!-- pushes -->
    <section class="pushes">
        <div class="container">
            <div class="push">
                <i class="dna icon"></i>

                <div class="body">
                    <h3 class="title">
                        The Team
                    </h3>
                    <p>Highly experienced physicians who specialize in minimally invasive techniques</p>
                </div>
            </div>

            <div class="push">
                <i class="heartbeat icon"></i>

                <div class="body">
                    <h3 class="title">
                        The Process
                    </h3>
                    <p>Patient Care Managers who make every step clear and comfortable</p>
                </div>
            </div>

            <div class="push">
                <i class="care icon"></i>

                <div class="body">
                    <h3 class="title">
                        The Results
                    </h3>
                    <p>Thousands of patients who finally found lasting relief from back or neck pain</p>
                </div>
            </div>

        </div>

        <div class="review">
            <a href="#topofform" class="button desktop">
                Get Your Free MRI/CT Review
            </a>
            <a href="/resource-center/webinar/" class="btn-orange invert button mobile" data-target="#mrict-modal">
                Get Your Free MRI/CT Review
            </a>
        </div>
    </section>

    <div class="contact-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-3"></div>
                <div class="col-xs-12">
                    <div class="expertimg"></div>
                    <h2>Let's chat about how we can help!</h2>
                    <p>Fill out the <a href="#">form on this page</a>, or call us at: <a href="tel:<?php echo get_option('support_phone'); ?>"><?php echo get_option('support_phone'); ?></a>. <br/>Our specialists are available 24/7.</p>

                    <div class="white-toggle hidden-xs hidden-sm"><em>Or take advantage of one of our free offers!</em></div>
                    <div class="visible-xs visible-sm mobile-form-options">
                        <ul>
                            <li><em>Or take advantage of one of our free offers below!</em></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-3"></div>
            </div>
        </div>
        <div class="close-contact">
            <div class="rotate-orange toggled">
                <div></div>
                <div></div>
            </div>
        </div>
    </div>
    <div class="toggle-contact toggled" id="scrollhere">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php pgform_main_split() ?>
                </div>
            </div>
        </div>
    </div>

    <section class="info">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 gray">
                    <div class="half video">
                        <div class="video-container">
                            <div id="yt-home" style="background: url('/wp-content/themes/north-american-spine/landing/shared/img/webinar-new.jpg');"></div>
                            <div id="yt-player"></div>
                            <div class="hover-video">
                                <p>Preview Our Free On-Demand Webinar</p>
                                <ul>
                                    <li>
                                        <i class="fa fa-pause pause-vid" aria-hidden="true"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-play play-vid" aria-hidden="true"></i>
                                    </li>
                                    <li>
                                        <i class="fa fa-repeat repeat-vid" aria-hidden="true"></i>
                                    </li>
                                </ul>
                            </div>

                        </div>
                        <a href="/resource-center/webinar/" class="btn-orange invert" data-target="#webinar-modal"><div></div>Watch Full Video</a>
                    </div>
                    <div class="half request">
                        <?php dynamic_sidebar( 'nas-info-kit-widget-area' ); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div><!-- #content -->
<div class="contact-area">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-3"></div>
            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-6">
                <div class="expertimg"></div>
                <h2>We love your questions!</h2>
                <p>If you would like to talk to a spine expert, please call us: <a href="tel:<?php echo get_option('support_phone'); ?>" class="white"><?php echo get_option('support_phone'); ?></a>.</p>
                <p>We're available 24/7</p>
            </div>
            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-3"></div>
        </div>
    </div>
    <div class="close-contact">
        <div class="rotate-orange <?php if (is_front_page()) {echo "toggled";} ?>">
            <div></div>
            <div></div>
        </div>
    </div>
</div>
<div class="toggle-contact <?php if (is_front_page()) {echo "toggled";} ?>">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php pgform_lp_main() ?>
            </div>
        </div>
    </div>
</div>

<footer id="colophon" class="site-footer" role="contentinfo">
    <div class="sitemap">
        <ul class="policies">
            <li><a href="/user-agreement/" target="_blank">User Agreement</a></li>
            <li><a href="/privacy-policy/" target="_blank">Privacy Policy</a></li>
        </ul>
    </div>
    <div class="site-info">
        <div class="container">
            <p>&copy; <?php echo date('Y'); ?> North American Spine. All rights reserved.<br/>Dallas · Detroit · Houston · Minneapolis · Nashville · Phoenix<br/>Please note: Treatment plans are customized to meet your specific pathology. Patient experiences may vary.</p>
        </div>
    </div><!-- .site-info -->
</footer><!-- #colophon -->
</div><!-- #page -->

<!-- Main Modal Popup -->
<div id="consult" tabindex="-1" role="dialog" aria-labelledby="consult modal" class="">
    <div class="modal-form home-modal">
        <div class="close-it"></div>
        <iframe src="https://www.youtube.com/embed/GFQU5MwDznE?rel=0&amp;controls=0&amp;showinfo=0" allowfullscreen="" data-attr="https://www.youtube.com/embed/GFQU5MwDznE?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0"></iframe>
    </div>
</div>

<!-- Webinar Modal Popup -->
<div id="webinar-modal" tabindex="-1" role="dialog" aria-labelledby="consult modal" class="">
    <div class="modal-form home-modal">
        <div class="close-it"></div>
        <h3>Webinar</h3>
        <?php pgform_lp_webinar() ?>
    </div>
</div>

<!-- Get Info Kit Modal Popup -->
<div id="freekit" tabindex="-1" role="dialog" aria-labelledby="consult modal" class="">
    <div class="modal-form home-modal">
        <div class="close-it"></div>
        <h3>Request a Free Info Kit</h3>
        <?php pgform_infokit_generic(); ?>
    </div>
</div>

<!-- Get Your Free MRI/CT Review Modal Popup -->
<div id="mrict-modal" tabindex="-1" role="dialog" aria-labelledby="consult modal" class="">
    <div class="modal-form home-modal">
        <div class="close-it"></div>
        <h3>Get Your Free MRI/CT Review</h3>
        <?php pgform_lp_main() ?>
    </div>
</div>

<?php wp_footer(); ?>

<script src="/wp-content/themes/north-american-spine/landing/shared/js/app.js"></script>

<script>
    $('a[data-target="#freekit"]').on('click', function(ev){
        // console.log('open it');

        $('#freekit').addClass('visible-time');
        ev.preventDefault();
        return false;

    });

    $('a[data-target="#webinar-modal"]').on('click', function(ev){

        $('#webinar-modal').addClass('visible-time');
        ev.preventDefault();
        return false;

    });

    $('a[data-target="#consult"]').on('click', function(ev){

        $('#consult').addClass('visible-time');
        ev.preventDefault();
        return false;

    });
</script>


<script type='text/javascript'>
    /* <![CDATA[ */
    var nas_yt_api = {"video":"https:\/\/www.youtube.com\/embed\/stu5hGc6T3A"};
    /* ]]> */
</script>
<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/js/ytiframe.js'></script>

</body>
</html>