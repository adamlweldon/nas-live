<?php
/**
 * Template Name: Generic Content Template (no blocks)
 *
 */
?>

<?php get_header(); ?>

<?php 
	// Start the loop.
	while ( have_posts() ) : the_post();
?>

	<div class="generic-no-block">
		<div class="container">
			
			<h1><?php the_title(); ?></h1>

			<div class="do-columns">
				<?php if (get_field('generic_content_template_subheader') != ''): ?>
					<h4 class="keep"><?php echo get_field('generic_content_template_subheader'); ?></h4>
				<?php endif ?>

				<p><?php the_content(); ?></p>
			
				<?php if (get_field('generic_content_template_link_text') != ''): ?>
					<a href="<?php echo get_field('generic_content_template_link_url'); ?>" class="btn-orange invert"><div></div><?php echo get_field('generic_content_template_link_text'); ?></a>
				<?php endif ?>
				

			</div>

		</div>
				
	</div>
<?php 		
	// End of the loop.
	endwhile;
?>

<?php get_footer(); ?>

