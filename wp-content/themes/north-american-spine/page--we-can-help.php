<?php  /* Template Name: we can help */?>

<?php wp_head();

require_once(dirname(__FILE__)."/pg_forms.php");

// Start the loop.
while ( have_posts() ) : the_post();
    ?>
    <title>We Can Help - North American Spine</title>
    <link rel="icon" href="http://new.nas.dev/wp-content/uploads/2017/03/cropped-favicon-1-32x32.png" sizes="32x32" />
    <link rel="icon" href="http://new.nas.dev/wp-content/uploads/2017/03/cropped-favicon-1-192x192.png" sizes="192x192" />
    <link rel="apple-touch-icon-precomposed" href="http://new.nas.dev/wp-content/uploads/2017/03/cropped-favicon-1-180x180.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel='stylesheet' id='fontawesome-css'  href='https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css?ver=4.7.5' type='text/css' media='all' />
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo get_stylesheet_directory_uri() ?>/landing/we-can-help/style.css" />


    <!--Call Tracking Metrics script - https://calltrackingmetrics.com/-->
    <script async src="//18862.tctm.co/t.js"></script>

    <!-- Optimizely Script -->
    <script src="//cdn.optimizely.com/js/73151603.js"></script>

    <!-- header goes here -->
    <div class="nas-landing-page site-container">
        <a name="topofform"></a>


    </div><!-- end fixed-nav-bar -->

    <header>
        <div class="mobile">
            <i class="fa fa-phone" aria-hidden="true"></i> <a href="tel:+18884851456">Call Now</a>
        </div>
        <div class="tablet">
            <i class="fa fa-phone" aria-hidden="true"></i> Call <a href="tel:+18884851456">888.485.1456</a> 24/7 to speak with a spine expert.
        </div>
        <div class="desktop">
            <i class="fa fa-phone" aria-hidden="true"></i> Call <span>888.485.1456</span> 24/7 to speak with a spine expert.
        </div>
    </header>

    <!-- header goes here -->
    <div class="accurascope-lp site-container">

        <div class="hero">
            <div class="hero-txt">
                <div class="nas-logo"><img src="/wp-content/themes/north-american-spine/landing/we-can-help/img/nas-logo.png"></div>
                <h1>Looking for back or neck pain relief? We can help.<br class="desktop" /> Provide your information below and we'll be in touch.</h1>
                <a name="topofform"></a>
            </div>
            <div class="form">
                <h2>See if you're a candidate. Contact us for a FREE review of your MRI/CT Scan.</h2>
                <div class="desktop">
                    <?php pgform_wecanhelp() ?>
                </div>
                <div class="tablet">
                    <?php pgform_wecanhelp() ?>
                </div>
                <div class="mobile">
                    <a data-toggle="modal" data-target="#consult"><button>Get Your Free MRI/CT Review <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></button></a>
                </div>
            </div>
        </div><!-- end .hero -->

        <div class="leader">
            <div class="col-xs-12 col-md-6">
                <p>North American Spine is the leader in minimally invasive spine care. With facilities across the U.S., we treat all kinds of back or neck pain, and we do it in the most hassle-free, individualized way possible. Also, the majority of our procedures can be done without an overnight hospital stay, and recovery is much quicker than recovery after large, open procedures. Many patients are back to work or play within several days. </p>
            </div>
            <div class="col-xs-12 col-md-6">
                <img src="/wp-content/themes/north-american-spine/landing/we-can-help/img/leader-img.png">
            </div>
        </div>

        <div class="lets-chat">
            <div class="col-xs-12 col-sm-5 desktop">
                <img src="/wp-content/themes/north-american-spine/landing/we-can-help/img/cara.png">
                <div class="patient-care-manager">Cara H.<br/>Patient Care Manager</div>
            </div>
            <div class="col-xs-12 col-sm-5 tablet">
                <img src="/wp-content/themes/north-american-spine/landing/we-can-help/img/cara.png">
                <div class="patient-care-manager">Cara H.<br/>Patient Care Manager</div>
            </div>
            <div class="col-xs-12 col-sm-7">
                <p>Let's chat about how we can help. Fill out the <a href="#topofform">form on this page</a> or call <span class="desktop">888.485.1456</span><a class="tablet" href="tel:+18884851456">888.485.1456</a><a class="mobile" href="tel:+18884851456">888.485.1456</a>. Our specialists are standing by.</p>
            </div>
            <div class="col-xs-12 col-sm-5 mobile">
                <img src="/wp-content/themes/north-american-spine/landing/we-can-help/img/cara.png">
                <div class="patient-care-manager">Cara H.<br/>Patient Care Manager</div>
            </div>
        </div>

        <div class="bullets">
            <div class="col-xs-12 col-sm-6">
                <div class="bullet-list symptoms">
                    <h3>Common Symptoms</h3>
                    <ul>
                        <li>Back or neck pain with or without radiating leg/arm pain</li>
                        <li>Back pain that involves the hips, buttocks or legs</li>
                        <li>Weakness and/or numbness in the limbs</li>
                    </ul>
                </div>
                <div class="bullet-list treated">
                    <h3>Conditions Treated</h3>
                    <ul>
                        <li>Sciatica</li>
                        <li>Degenerative Disc Disease</li>
                        <li>Bulging or Herniated Disc</li>
                        <li>Pinched Nerve</li>
                        <li>Spinal Stenosis</li>
                        <li>And many more</li>
                    </ul>
                </div>
                <div class="bullet-list benefits">
                    <h3>Our Unique Benefits</h3>
                    <ul>
                        <li>Smaller incisions and minimal scar tissue</li>
                        <li>Diagnosis through HD camera</li>
                        <li>Majority outpatient procedures/ minimal hospital stay</li>
                        <li>Quicker recovery time</li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <img src="/wp-content/themes/north-american-spine/landing/we-can-help/img/cutout.png">
            </div>
        </div>

        <div class="contact-us">
            <p>Contact us today at <span class="desktop">888.485.1456</span><a class="mobile" href="tel:+18884851456">888.485.1456</a><a class="tablet" href="tel:+18884851456">888.485.1456</a>,<br class="desktop" /> or take advantage of one of our free offers:</p>
        </div>

        <div class="modules">
            <ul>
                <li><a data-toggle="modal" data-target="#freekit"><img class="desktop" src="/wp-content/themes/north-american-spine/landing/we-can-help/img/free-info-kit-module.jpg"><span class="button"><button type="button" class="btn btn-info btn-lg">Free Info Kit</button></span></a></li>
                <li><a data-toggle="modal" data-target="#webinar-modal"><img class="desktop" src="/wp-content/themes/north-american-spine/landing/we-can-help/img/free-on-demand-webinar-module.jpg"><span class="button"><button type="button" class="btn btn-info btn-lg">Free On-Demand Webinar</button></span></a></li>
            </ul>

        <footer>
            <div class="col-sm-12">
                <div class="stillhavequestions">
                    <p>If you still have questions, please call us: <span class="desktop"><nobr>888.485.1456</nobr></span><a class="tablet" href="tel:+18884851456"><nobr>888.485.1456</nobr></a><a class="mobile" href="tel:+18884851456"><nobr>888.485.1456</nobr></a>. Or fill out the <a href="#topofform">form on this page</a>.<br/>Our specialists are available 24/7. Please note: results may vary.</span></p>
                    <p class="please-note">Dallas | Detroit | Houston | Minneapolis | Nashville | Phoenix<br/>&copy; <?php echo date('Y'); ?> <a href="/" target="_blank">North American Spine</a>. <br class="mobile"/>All rights reserved.</p>
                </div>
        </footer>

    </div><!-- end .site-container -->

</div>

    <!-- Main Modal Popup -->
    <div id="consult" tabindex="-1" role="dialog" aria-labelledby="consult modal" class="">
        <div class="modal-form home-modal">
            <div class="close-it"></div>
            <h3>Know your options. Get a free MRI/CT review.</h3>
            <?php pgform_lp_main() ?>
        </div>
    </div>

    <!-- Webinar Modal Popup -->
    <div id="webinar-modal" tabindex="-1" role="dialog" aria-labelledby="consult modal" class="">
        <div class="modal-form home-modal">
            <div class="close-it"></div>
            <h3>Webinar</h3>
            <?php pgform_lp_webinar() ?>
        </div>
    </div>

    <!-- Get Info Kit Modal Popup -->
    <div id="freekit" tabindex="-1" role="dialog" aria-labelledby="consult modal" class="">
        <div class="modal-form home-modal">
            <div class="close-it"></div>
            <h3>Request a Free Info Kit</h3>
            <?php pgform_infokit_generic() ?>
        </div>
    </div>

    <script src="<?php echo get_template_directory_uri(); ?>/js/scripts.js"></script>

    <script>
        jQuery('a[data-target="#freekit"]').on('click', function(ev){
            // console.log('open it');

            jQuery('#freekit').addClass('visible-time');
            ev.preventDefault();
            return false;

        });

        jQuery('a[data-target="#webinar-modal"]').on('click', function(ev){

            jQuery('#webinar-modal').addClass('visible-time');
            ev.preventDefault();
            return false;

        });

        jQuery('a[data-target="#consult"]').on('click', function(ev){

            jQuery('#consult').addClass('visible-time');
            ev.preventDefault();
            return false;

        });
    </script>


    <script type="text/javascript">
        jQuery(function(){
            jQuery('img[data-url]').click(function () {
                jQuery('.current-show iframe').attr('src', jQuery(this).data('url') + '?rel=0');
            });
        });
    </script>

    <script>
        //fixed nav bar
        var mn = jQuery(".fixed-nav-bar");

        jQuery(window).scroll(function () {
            if( jQuery(this).scrollTop() > 106 ) {
                mn.addClass("main-nav-scrolled");
            } else {
                mn.removeClass("main-nav-scrolled");
            }
        });
        //@ sourceURL=pen.js
    </script>

    <?php
// End of the loop.
endwhile;

wp_footer();
?>

