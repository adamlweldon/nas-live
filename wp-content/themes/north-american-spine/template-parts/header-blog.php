<?php
    $blog_title = get_the_title( get_option('page_for_posts', true));
?>

    <section id="blog-header" class="">

        <div class="col-md-12">
    
        <header>
            <h1><?php echo $blog_title; ?></h1>


            <div class="search-area col-md-3">
            
                <?php dynamic_sidebar( 'blog-searcharea' ); ?>

            </div>
        </header>


        
        </div>

    </section>