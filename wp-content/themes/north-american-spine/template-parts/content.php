<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package North_American_Spine
 */

?>

<article id="post-<?php the_ID(); ?>" <?php if ( is_single() ) { post_class(array('single-post')); } else { post_class(array('col-md-6')); } ?>>
	<header class="entry-header">

		<?php
			if ( is_single() ) :
				
				?>
				<div class="image-container">
			<?php
				echo get_the_post_thumbnail($post, 'original');
				?>
				</div>
				<?php
				the_title( '<h1 class="entry-title">', '</h1>' );
			else :

				?>
					<div class="image-container">
					<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">
					<?php
						if(get_the_post_thumbnail($post, 'medium_large') == ''){
					?>
							<img src="/wp-content/themes/north-american-spine/inc/images/logo.png" alt="<?php the_title(); ?>" style="max-width: 100%; min-height: auto;">
					<?php
						}
						else{
							echo get_the_post_thumbnail($post, 'medium_large');
						}
						
					?>
					</div>

					<?php
					the_title( '<h1 class="entry-title">', '</<h1>' );
					?>
					</a>
					<?php



					// echo get_post_type();
					// var_dump(has_post_thumbnail());
				// }
			
			endif;

		if (get_post_type() == 'nas-testimonials') : ?>
			<div class="entry-meta lb-border-bottom clear-both">
				<?php nas_posted_entry(true, false); ?>
				<?php if (get_field('contains_video') == 1) : ?>
                	<img src="<?php echo NAS_TEMPLATE_DIR . '/assets/img/video-icon.svg'; ?>" alt="Video Icon" class="has-video">
                <?php endif; ?>
			</div>
		<?php
		endif;

		// if ( 'post' === get_post_type() ) : ?>
		<!-- <div class="entry-meta lb-border-bottom"> -->
			<?php // nas_posted_entry(true, true); ?>
		<!-- </div> .entry-meta  -->
		<?php
		// endif; ?>
	</header><!-- .entry-header -->

	<?php
	if (is_single()) : ?>
		<div class="entry-content">
			<?php
				
				the_content( sprintf(
					/* translators: %s: Name of current post. */
					wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'north-american-spine' ), array( 'span' => array( 'class' => array() ) ) ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				) );
				$next_post = get_adjacent_post(false, '', false);

				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'north-american-spine' ),
					'after'  => '</div>',
				) );
				
			?>
		</div><!-- .entry-content -->
		<footer class="entry-footer">
			<?php nas_entry_footer(); ?>
		</footer><!-- .entry-footer -->

		
	<?php else : ?>
		<div class="entry-content">
			<?php the_excerpt(); ?>
		</div>
		<footer class="entry-footer">
			<a class="read-more" href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">Read More</a>
		</footer>
	<?php endif; ?>
</article><!-- #post-## -->
