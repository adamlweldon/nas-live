<section id="blog-header" class="">
    <div class="col-md-12">
    <header>
 
        <?php       
        $post_type = get_post_type();
        $post_type_data = get_post_type_object( $post_type );
        $post_type_slug = $post_type_data->rewrite['slug'];
        ?>
        <h1><a href="/<?php echo $post_type_slug; ?>"><?php echo get_post_type_object(get_post_type())->label; ?></a></h1>

    </header>

    </div>

</section>