<?php

function pgform_main()
{
    return new pgform\forms\NAS_Main([
        "attributes" => [
            "id" => "sidebarform",
            "action" => "/thank-you-tell-us-more",
            "class" => "center-align"
        ],
        "submit-text" => "Get Free MRI/CT Review"
    ]);
}

function pgform_main_split () {
    new \pgform\forms\NAS_Main_Split([
        "attributes" => [
            "action" => "/thank-you-tell-us-more"
        ],
        "submit-text" => "Get Your Answer"
    ]);
}

function pgform_lp_main()
{
    return new \pgform\forms\NAS_LP_Main([
        "attributes" => [
            "action" => "/thank-you-tell-us-more"
        ],
        "submit-text" => "GET YOUR FREE MRI/CT REVIEW"
    ]);
}

function pgform_lp_main_bluesubmit()
{
    return new \pgform\forms\NAS_LP_Main([
        "attributes" => [
            "action" => "/thank-you-tell-us-more",
            "class" => "blue-submit"
        ],
        "submit-text" => "GET YOUR FREE MRI/CT REVIEW"
    ]);
}

function pgform_lp_main_bluesubmit_blackcheck()
{
    return new \pgform\forms\NAS_LP_Main([
        "attributes" => [
            "action" => "/thank-you-tell-us-more",
            "class" => "blue-submit black-check"
        ],
        "submit-text" => "GET YOUR FREE MRI/CT REVIEW"
    ]);
}

function pgform_wecanhelp()
{
    return new \pgform\forms\NAS_LP_WeCanHelp([
        "attributes" => [
            "action" => "/thank-you-tell-us-more"
        ],
        "submit-text" => "Get Your Free MRI/CT Review"
    ]);
}

function pgform_webinar()
{
    return new \pgform\forms\NAS_Webinar([
        "attributes" => [
            "id" => "webinarform",
            "class" => "center-align",
            "action" => "/webinar-thank-you"
        ],
    ]);
}

function pgform_lp_webinar()
{
    return new \pgform\forms\NAS_LP_Webinar([
        "attributes" => [
            "id" => "webinarform",
            "class" => "center-align",
            "action" => "/webinar-thank-you"
        ],
    ]);
}

function pgform_thankyou()
{
    return new \pgform\forms\NAS_ThankYouTellUsMore([
        "attributes" => [
            "action" => "/thank-you-for-contacting-us?thanks2"
        ]
    ]);
}

function pgform_infokit_generic()
{
    return new \pgform\forms\NAS_LP_FreeInfoKit([
            "unique-name" => "generic",
            "submit-text" => "REQUEST KIT",
            "attributes" => [
                "class" => "center-align",
                "action" => "/thank-you-tell-us-more"
            ],
            "fulfillment" => "8"
        ]);
}
add_shortcode("pgform_infokit_generic", "pgform_infokit_generic");


function pgform_infokit_reusable()
{
    $page_id = get_the_ID();

    $mappings = [
        // Generic Fulfillments
        'generic' => [8, 'generic-procedures'],

        // Page-specific fulfillments
        "18610" => [3, 'bulging-disc'],
        "18693" => [3, 'herniated-disc'],
        "18299" => [4, 'sciatica'],
        "18699" => [5, 'spinal-stenosis'],
        "18683" => [6, 'ddd'],

    ];

    if (!array_key_exists($page_id, $mappings)) {
        $page_id = "generic";
    }

    return new \pgform\forms\NAS_LP_FreeInfoKit([
        "attributes" => [
            "class" => "center-align",
            "action" => "/thank-you-tell-us-more"
        ],
        "submit-text" => "REQUEST KIT",
        "fulfillment" => $mappings[$page_id][0],
        "unique-name" => $mappings[$page_id][1]
    ]);

}

function pgform_connectformerpatient()
{
    return new pgform\forms\NAS_ConnectFormerPatient([
        "attributes" => [
            "id" => "sidebarform",
            "action" => "/thank-you-tell-us-more"
        ],
        "submit-text" => "Submit"
    ]);
}
add_shortcode("pgform_connectformerpatient", "pgform_connectformerpatient");

function pgform_insurance_coverage()
{
    return (new pgform\forms\NAS_InsuranceCoverage([
            "attributes" => [
                "id" => "sidebarform",
                "action" => "/thank-you-tell-us-more"
            ],
            "auto-echo" => false,
            "submit-text" => "Get Your Answer"]
    ))->render();
}
add_shortcode("pgform_insurance_coverage", "pgform_insurance_coverage");

function pgform_lp_main_shortcode()
{
    return (new \pgform\forms\NAS_LP_Main([
        "attributes" => [
            "action" => "/thank-you-tell-us-more"
        ],
        "auto-echo" => false,
        "submit-text" => "GET YOUR FREE MRI/CT REVIEW"
    ]))->render();
}
add_shortcode("pgform_lp_main_shortcode", "pgform_lp_main_shortcode");

function pgform_infokit_generic_shortcode()
{
    return (new \pgform\forms\NAS_LP_FreeInfoKit([
        "unique-name" => "generic",
        "submit-text" => "REQUEST KIT",
        "attributes" => [
            "class" => "center-align",
            "action" => "/thank-you-tell-us-more"
        ],
        "auto-echo" => false,
        "fulfillment" => "8"
    ]))->render();
}
add_shortcode("pgform_infokit_generic_shortcode", "pgform_infokit_generic_shortcode");