<?php
/**
 * Template Name: Doctor Template
 *
 */
?>

<?php get_header(); ?>

<?php 
	// Start the loop.
	while ( have_posts() ) : the_post();
?>
	<div class="bluearea"></div>
	<div class="container generic bios" id="conditions">
		<section class="each-block-all" id="pinned-lock-1">
			<div class="left-50">
				<div class="each-block-lock first-lock alt-lock">
					<div class="generic-content-left">
						<h1>NAS Doctor Bios</h1>
						<h4><?php echo the_title(); ?><?php if (get_field('doctor_title') != ''): ?>, <?php echo get_field('doctor_title'); ?><?php endif ?></h4>
						
						<?php if (get_field('doctor__pcm_specialty_title') != ''): ?>
							<p><?php echo get_field('doctor__pcm_specialty_title'); ?></p>
						<?php endif ?>

						<?php 
						$thumb_id = get_post_thumbnail_id();
						$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
						$thumb_url = $thumb_url_array[0];
						$image_alt = get_post_meta( $thumb_id, '_wp_attachment_image_alt', true);

							if (isset($thumb_url)) {
						?>
								<img src="<?php echo $thumb_url; ?>" alt="<?php echo $image_alt; ?>">
						<?php
							}
						?>

						<?php if( have_rows('doctor_summary_subheaders') ): ?>
							
							<?php while( have_rows('doctor_summary_subheaders') ): the_row(); ?>
								<p><?php echo get_sub_field('featured_subheader'); ?></p>
							<?php endwhile; ?>
						<?php endif; ?>
						<p class="location-icon">Practices at:</p>

						<?php if( have_rows('practice_locations') ): ?>
							
							<?php while( have_rows('practice_locations') ): the_row(); ?>

								<a href="<?php echo get_permalink(get_sub_field('practice_location')); ?>"><?php echo get_the_title(get_sub_field('practice_location')) ?></a>
							<?php endwhile; ?>
						<?php endif; ?>
						
					
					</div>
				</div>
			</div>	
			
			<div class="right-50">
				<div class="each-block-info">


					<h2 class="spaces targetedscrollpoint" id="generic-sectionscroll-<?php echo get_row_index(); ?>">Dr. <?php echo the_title(); ?></h2>

					<?php 
						//$thumb_id = get_post_thumbnail_id();
						// $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
						// $thumb_url = $thumb_url_array[0];
						// $image_alt = get_post_meta( $thumb_id, '_wp_attachment_image_alt', true);

						//if (isset($thumb_url)) {
					?>
							<!-- <img src="<?php echo $thumb_url; ?>" alt="<?php echo $image_alt; ?>"> -->
					<?php
						//}
					?>
					
					<div class="toggle-area">

						<?php if( have_rows('basic_info_blocks') ): ?>
							
							<?php while( have_rows('basic_info_blocks') ): the_row(); ?>
								<h4><?php echo get_sub_field('subheader'); ?></h4>
								<p><?php echo get_sub_field('content'); ?></p>
							<?php endwhile; ?>
						<?php endif; ?>
						
					</div>

				</div>
				
			</div>

		</section>

		<section class="each-block-all info testimony-info-block">

			<div class="left-50">
				<div class="each-block-lock targetedscrollpoint" id="testimony-scroll-to">
					<h2><?php the_title(); ?> Testimonial</h2>

					<?php 

						$post_object = get_field('featured_testimonial');
						$post = $post_object;
						setup_postdata($post); 

					?>

					<div class="patient-data">

						<?php 

							$thumb_id = get_post_thumbnail_id();
							$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
							$thumb_url = $thumb_url_array[0];
							$image_alt = get_post_meta( $thumb_id, '_wp_attachment_image_alt', true);
						?>
						
						<?php if ($thumb_url_array[1] != 48): ?>
							<img src="<?php echo $thumb_url; ?>" alt="<?php echo $image_alt; ?>">
						<?php endif ?>


						<div class="patient-info">
							<h4><?php echo get_field('testimonial_block_patient_name', $post->ID); ?></h4>
							
							<?php 
								$cond = get_field('testimonial_associated_condition', $post->ID);
								$proc = get_field('testimonial_page_link', $post->ID);
							?>
							<p>Condition: <a href="<?php the_permalink( $cond->ID ); ?>"><?php echo $cond->post_title; ?></a></p>
							<p>Procedure: <a href="<?php the_permalink( $proc->ID ); ?>"><?php echo $proc->post_title; ?></a></p>
							<?php if (get_field('testimonial_block_hometown', $post->ID) != ''): ?>
								<p>Hometown: <span><?php echo get_field('testimonial_block_hometown', $post->ID) ?></span></p>
							<?php endif ?>
						</div>
						
					</div>
					<h6 class="quote">“<?php echo get_field('testimonial_block_quote', $post->ID); ?>”</h6>
					<h4 class="lifestyle"><?php echo get_field('testimonial_block_subheader', $post->ID); ?></h4>
					<p><?php echo get_field('testimonial_block_content', $post->ID); ?></p>
					<div class="center-btns">

						<a href="<?php the_permalink( $post->ID ); ?>" class="btn-orange first-space"><div></div>Read Full Story</a>
						<a href="/testimonials" class="btn-orange hidden-md"><div></div>See All Testimonials</a>
					</div>
					<?php wp_reset_postdata(); ?>
				</div>

			</div>	

			<div class="right-50">
				<div class="each-block-info">
					<h2>Connect With A Patient</h2>
					
					<div class="toggle-area">
						<p>Some of our former patients have been kind enough to offer to speak with back and neck pain sufferers who are looking into treatment options. We encourage you to take them up on it! After we determine a treatment plan, we’ll connect you with a past patient who had a similar condition and procedure.</p>
						<a href="/connect-with-a-patient" class="btn-orange invert"><div></div>Connect with a Patient</a>
					</div>
				</div>
				
			</div>
		</section>

	</div>
<?php 		
	// End of the loop.
	endwhile;
?>

<?php get_footer(); ?>

