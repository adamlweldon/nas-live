<?php
/**
 * Template Name: Each Condition Template Page
 *
 */
?>

<?php get_header(); ?>

<?php 
	// Start the loop.
	while ( have_posts() ) : the_post();
?>
	<div class="bluearea"></div>
	<div class="container alt-conditions" id="conditions">
		<section class="each-block-all" id="pinned-lock-1">
			<div class="left-50">
				<div class="each-block-lock first-lock alt-lock">
				<?php 
					$supportingimg = get_field('each_condition_what_is_image');
					$actualimage =  wp_get_attachment_url($supportingimg);
					$image_alt = get_post_meta( $supportingimg, '_wp_attachment_image_alt', true);	
				?>

				<?php if ($actualimage==TRUE): ?>
					<div class="condition-content-left">
						<h1>What is <?php the_title(); ?>?</h1>
						<h4><?php echo get_field('each_condition_subheader'); ?></h4>
						<p><?php echo get_field('each_condition_what_is_content'); ?></p>
					</div>
					<div class="right-img">

						<img src="<?php echo $actualimage; ?>" alt="<?php echo $image_alt; ?>">
					</div>
				<?php else: ?>
					<!-- <div class="condition-content-left"> -->
						<h1>What is <?php the_title(); ?>?</h1>
						<h4><?php echo get_field('each_condition_subheader'); ?></h4>
						<p><?php echo get_field('each_condition_what_is_content'); ?></p>
					<!-- </div> -->
					
				<?php endif ?>

					
				</div>
			</div>	
			
			<div class="right-50">
				<div class="each-block-info targetedscrollpoint" id="symptoms-scroll-to">
					<h2>Symptoms of <?php the_title(); ?></h2>
					
					<div class="toggle-area">
						<h4><?php echo get_field('each_condition_symptoms_subheader'); ?></h4>
						
						
						<?php if( have_rows('each_condition_symptoms_list') ): ?>
							<ul>Common symptoms of <?php the_title(); ?> include:
							<?php while( have_rows('each_condition_symptoms_list') ): the_row(); ?>
								<li><?php echo get_sub_field('each_condition_symptom'); ?></li>
								
							<?php endwhile ?>
							</ul>
						<?php endif ?>
						
					
						
						<?php if( have_rows('each_condition_types_of_list') ): ?>
							<ul class="typesof">Types of <?php the_title(); ?> include:
							<?php while( have_rows('each_condition_types_of_list') ): the_row(); ?>
								
									<li><?php echo get_sub_field('each_condition_type_of'); ?></li>
								
							<?php endwhile ?>
							</ul>
						<?php endif ?>

						<?php if (get_field('each_condition_symptom_content')): ?>
							<p><?php echo get_field('each_condition_symptom_content'); ?></p>
						<?php endif ?>
						
						<a href="<?php echo get_field('treatment_guide_link'); ?>" class="btn-orange invert"><div></div>Explore Treatment Options</a>

					</div>

					<h2 class="spaces targetedscrollpoint" id="causes-scroll-to">Causes of <?php the_title(); ?></h2>
					
					<div class="toggle-area">
						<h4><?php echo get_field('each_condition_causes_subheader'); ?></h4>
						<ul class="causesof">Additional common <?php the_title(); ?> causes include:
							<?php if( have_rows('each_condition_causes_list') ): ?>
								<?php while( have_rows('each_condition_causes_list') ): the_row(); ?>
									<li><?php echo get_sub_field('each_condition_cause'); ?></li>
								<?php endwhile ?>
							<?php endif ?>
						</ul>
						<p><?php echo get_field('each_condition_causes_content'); ?></p>
						<a href="/our-procedures" class="btn-orange invert"><div></div>Learn About Our Procedures</a>
					</div>

					<div class="request-alt-block targetedscrollpoint" id="request-scroll-to">
						<h2>Request a Free Info Kit</h2>
						
						<div class="toggle-area">
							<a href="/resource-center/request-free-info-kit/"><img src="../../wp-content/themes/north-american-spine/assets/img/brochureback.png" alt="#"><img src="../../wp-content/themes/north-american-spine/assets/img/brochurefront.png" alt="Request Free Info Kit"></a>
							<a href="/resource-center/request-free-info-kit/" class="btn-orange invert"><div></div>Get Information Kit</a>
						</div>
					</div>

				</div>
				
			</div>

		</section>

		<section class="each-block-all info testimony-info-block">

			<div class="left-50">
				<div class="each-block-lock targetedscrollpoint" id="testimony-scroll-to">
					<h2><?php the_title(); ?> Testimonial</h2>

					<?php 

						$post_object = get_field('featured_testimonial');
						$post = $post_object;
						setup_postdata($post); 
						
					?>

					<div class="patient-data">

						<?php 

							$thumb_id = get_post_thumbnail_id();
							$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
							$thumb_url = $thumb_url_array[0];
							$image_alt = get_post_meta( $thumb_id, '_wp_attachment_image_alt', true);
						?>
						<?php if ($thumb_url_array[1] != 48): ?>
							<img src="<?php echo $thumb_url; ?>" alt="<?php echo $image_alt; ?>">
						<?php endif ?>

						<div class="patient-info">
							<h4><?php echo get_field('testimonial_block_patient_name', $post->ID); ?></h4>
							
							<?php 
								$cond = get_field('testimonial_associated_condition', $post->ID);
								$proc = get_field('testimonial_page_link', $post->ID);
							?>
							<p>Condition: <a href="<?php the_permalink( $cond->ID ); ?>"><?php echo $cond->post_title; ?></a></p>
							<p>Procedure: <a href="<?php the_permalink( $proc->ID ); ?>"><?php echo $proc->post_title; ?></a></p>
							<?php if (get_field('testimonial_block_hometown', $post->ID) != ''): ?>
								<p>Hometown: <span><?php echo get_field('testimonial_block_hometown', $post->ID) ?></span></p>
							<?php endif ?>
							
						</div>
						
					</div>
					<h6 class="quote">“<?php echo get_field('testimonial_block_quote', $post->ID); ?>”</h6>
					<h4 class="lifestyle"><?php echo get_field('testimonial_block_subheader', $post->ID); ?></h4>
					<p><?php echo get_field('testimonial_block_content', $post->ID); ?></p>
					<div class="center-btns">

						<a href="<?php the_permalink( $post->ID ); ?>" class="btn-orange first-space"><div></div>Read Full Story</a>
						<a href="/testimonials" class="btn-orange hidden-md"><div></div>See All Testimonials</a>
					</div>
					<?php wp_reset_postdata(); ?>
				</div>
			</div>	
			
			<!--  -->
			<!-- update links -->
			<!--  -->
			<!--  -->
			<!-- update links -->
			<!--  -->
			<!--  -->
			<!-- update links -->
			<!--  -->
			<!--  -->
			<!-- update links -->
			<!--  -->

			<div class="right-50">
				<div class="each-block-info">
					<h2>Connect With A Patient</h2>
					
					<div class="toggle-area">
						<p>Some of our former patients have been kind enough to offer to speak with back and neck pain sufferers who are looking into treatment options. We encourage you to take them up on it! After we determine a treatment plan, we’ll connect you with a past patient who had a similar condition and procedure.</p>
						<a href="/connect-with-a-patient" class="btn-orange invert"><div></div>Connect with a Patient</a>
					</div>
				</div>
				
			</div>
		</section>

		<div class="nav-dots">
			<ul>
				<li data-target="symptoms-scroll-to"><div class="fade-slide">Symptoms <?php echo the_title(); ?></div></li>
				<li data-target="causes-scroll-to"><div class="fade-slide">Causes of <?php echo the_title(); ?></div></li>
				<li data-target="request-scroll-to"><div class="fade-slide">Request Info</div></li>
				<li data-target="testimony-scroll-to"><div class="fade-slide">Patient Testimonial</div></li>
			</ul>
		</div>

	</div>
<?php 		
	// End of the loop.
	endwhile;
?>

<?php get_footer(); ?>

