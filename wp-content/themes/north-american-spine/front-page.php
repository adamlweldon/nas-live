<?php 
/**
	Home page
*/

	get_header();
?>

<main class="hero">

	<?php if( have_rows('each_slide_homepage') ): ?>

		<?php while( have_rows('each_slide_homepage') ): the_row(); 

			$slidetitle = get_sub_field('slide_title');
			$slidecite = get_sub_field('slide_cite');
			$slidelinkcontent = get_sub_field('slide_link_content');
			$slidelink = get_sub_field('slide_link');
			$slideimage = get_sub_field('slide_image');
		?>

			<section class="each-slide" style="background: url('<?php echo $slideimage; ?>') center center no-repeat;">
				<div class="container">
					<div class="center-content">
						<h1><?php echo $slidetitle; ?></h1>
						<?php if ($slidecite): ?>
							<p><?php echo $slidecite; ?></p>
						<?php endif ?>
						<?php if (get_sub_field('open_modal') != NULL): ?>
							<div class="btn-orange trigger-consult" class="open_modal" data-toggle="modal" data-target="#consult"><div></div><?php  echo $slidelinkcontent; ?><i class="fa fa-play-circle-o" aria-hidden="true"></i></div>
						<?php endif ?>
						<?php if (get_sub_field('slide_link')): ?>
							<a href="<?php echo $slidelink; ?>" class="btn-orange"><div></div><?php echo $slidelinkcontent; ?></a>
						<?php endif ?>
					</div>
				</div>
			</section>

		<?php wp_reset_postdata(); endwhile; ?>

	<?php endif; ?>

</main>
<section class="blue-sub hidden-xs hidden-sm">
	<?php echo get_field('blue_featured_content'); ?>
</section>
<section class="mob-only visible-xs visible-sm">
	<div class="container">
		<div class="row">
			<div class="col-xs-4 col-sm-4 border">
				<a href="/patient-care/free-mri-review/">
					<i class="clipboard"></i>
					<h3>Free MRI/CT Review</h3>
				</a>
			</div>
			<div class="col-xs-4 col-sm-4 border">
				<a href="/resource-center/webinar/">
					<i class="computer"></i>
					<h3>Watch Our Webinar</h3>
				</a>
			</div>
			<div class="col-xs-4 col-sm-4 border">
				<a href="#" data-toggle="modal" data-target="#freekit">
					<i class="packet"></i>
					<h3>Request Info Kit</h3>
				</a>
			</div>

		</div>
	</div>
</section>
<!-- <div class="ask-expert visible-sm visible-xs toggled">
	<div class="toggle-abs-section"></div>
	<div class="expert-icon"></div>
	<h5>Ask an expert</h5>
	<p>Have questions? We can help.</p>
	<div class="toggle"></div>
	<div class="form-area">
		<?php //pgform_main(); ?>
		
		<div class="toggle-close">
			<div></div>
			<div></div>
		</div>
	</div>
</div> -->
<section class="slider">
	<div class="condition-procedure">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-1 col-md-2 col-lg-2"></div>
				<div class="col-xs-12 col-sm-10 col-md-8 col-lg-8 slick-it">

				<?php if( have_rows('each_spinal_slide') ): ?>

					<?php while( have_rows('each_spinal_slide') ): the_row(); 

						$fadeimage = get_sub_field('center_image');
						$fadeleftt = get_sub_field('left_title');
						$faderightt = get_sub_field('right_title');
					?>

						<div class="row fade-area">
							<div class="target-corners-left"></div>
							<div class="target-corners-right"></div>
							<div class="col-sm-3 col-md-3 col-lg-3 hidden-xs">
								<h3><?php echo $fadeleftt; ?></h3>
								<ul>
									<?php if( have_rows('left_list_item') ): ?>

										<?php while( have_rows('left_list_item') ): the_row();  
											// $subtitle = get_sub_field('title');
											// $suburl = get_sub_field('link_url');
										?>	
											<li>
												<a href="<?php echo get_permalink(get_sub_field('condition_link')); ?>"><?php echo get_the_title(get_sub_field('condition_link')); ?></a>
											</li>

											<li class="move-down"><a href="/conditions">All Conditions</a></li>

										<?php endwhile; ?>

									<?php endif; ?>
								
								</ul>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
								
								<img src="<?php echo $fadeimage; ?>" alt="North American Spine">
							</div>
							<div class="col-xs-6 visible-xs">
								<h3><?php echo $fadeleftt; ?></h3>
								<ul>
									<?php if( have_rows('left_list_item') ): ?>

										<?php while( have_rows('left_list_item') ): the_row();  
											// $subtitle = get_sub_field('title');
											// $suburl = get_sub_field('link_url');
										?>	
											<li>
												<a href="<?php echo get_the_permalink(get_sub_field('condition_link')); ?>"><?php echo get_the_title(get_sub_field('condition_link')); ?></a>
											</li>

											<li class="move-down"><a href="/conditions">All Conditions</a></li>
											

										<?php endwhile; ?>

									<?php endif; ?>
								
								</ul>
							</div>
							<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
								<h3><?php echo $faderightt; ?></h3>
								<ul>
									<?php if( have_rows('right_list_item') ): ?>

										<?php while( have_rows('right_list_item') ): the_row();  
											
										?>	
											<li>
												<a href="<?php echo get_permalink(get_sub_field('procedure_link')); ?>"><?php echo get_the_title(get_sub_field('procedure_link')); ?></a>
											</li>

											<li class="move-down"><a href="/our-procedures">All Procedures</a></li>

										<?php endwhile; ?>

									<?php endif; ?>
								
								</ul>
							</div>
						</div>

					<?php endwhile; ?>

				<?php endif; ?>	

				</div>
				<div class="col-xs-12 col-sm-1 col-md-2 col-lg-2"></div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">

			<?php if( have_rows('each_box') ): ?>

				<?php while( have_rows('each_box') ): the_row(); 

					$thirdstitle = get_sub_field('box_title');
					$thirdssub = get_sub_field('box_subtitle');
					$thirdscontent = get_sub_field('box_content');
					$thirdslinktext = get_sub_field('box_link_title');
					$thirdslinkurl = get_sub_field('box_link_url');
				?>
						
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 thirds">
						<h3><?php echo $thirdstitle; ?></h3>
						<h4><?php echo $thirdssub; ?></h4>
						<p><?php echo $thirdscontent; ?></p>
						<a href="<?php echo $thirdslinkurl; ?>" class="btn-orange invert"><div></div><?php echo $thirdslinktext; ?></a>
					</div>

				<?php endwhile; ?>

			<?php endif; ?>	
		</div>
	</div>
</section>
<section class="info">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="half video">
					<div class="video-container">
						<div id="yt-home" style="background: url(<?php echo get_field('video_featured_image'); ?>);"></div>
						<div id="yt-player"></div>
						<div class="hover-video">
							<p><?php echo get_field('video_title'); ?></p>
							<ul>
								<li>
									<i class="fa fa-pause pause-vid" aria-hidden="true"></i>
								</li>
								<li>
									<i class="fa fa-play play-vid" aria-hidden="true"></i>
								</li>
								<li>
									<i class="fa fa-repeat repeat-vid" aria-hidden="true"></i>
								</li>
							</ul>
						</div>
						
					</div>
					<a href="/resource-center/webinar/" class="btn-orange invert" data-target="#webinar-modal"><div></div>Watch Full Video</a>
				</div>
				<div class="half request">
					<?php dynamic_sidebar( 'nas-info-kit-widget-area' ); ?>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-override">
				<div class="half loc" style="background: url(<?php echo get_field('locations_image'); ?>) center center no-repeat;">
					<h3><?php echo get_field('locations_title'); ?></h3>
					<h4><?php echo get_field('locations_sub_header'); ?></h4>
					<p><?php echo get_field('locations_content'); ?></p>
					<a href="<?php echo get_field('location_link_url'); ?>" class="btn-orange invert"><div></div><?php echo get_field('locations_link_title'); ?></a>
				</div>
				<div class="half approach" style="background: url(<?php echo get_field('approach_background'); ?>) center center no-repeat;">
					<h3><?php echo get_field('approach_title'); ?></h3>
					<h4><?php echo get_field('approach_sub_header'); ?></h4>
					<p><?php echo get_field('approach_content'); ?></p>
					<a href="<?php echo get_field('approach_link_url'); ?>" class="btn-orange invert"><div></div><?php echo get_field('approach_link_title'); ?></a>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- modal -->
<div id="consult" tabindex="-1" role="dialog" aria-labelledby="consult modal">
	<div class="modal-form home-modal">
		<div class="close-it"></div>
		<?php if( have_rows('each_slide_homepage') ): ?>
			<?php while( have_rows('each_slide_homepage') ): the_row(); ?>
			
			<?php if (get_sub_field('modal_video_content')): ?>
				<iframe src="<?php echo get_sub_field('modal_video_content'); ?>?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen data-attr="<?php echo get_sub_field('modal_video_content'); ?>?rel=0&amp;controls=0&amp;showinfo=0"></iframe>
			<?php endif ?>
			
			<?php wp_reset_postdata(); endwhile; ?>
		<?php endif; ?>
	</div>
</div>

<?php 
	get_footer();
?>