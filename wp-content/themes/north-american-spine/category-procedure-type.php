<?php
/**
* A Simple Category Template
*/
	get_header();
?>

	<div class="bluearea"></div>
	<div class="container generic" id="conditions">
		<section class="each-block-all" id="pinned-lock-1">
			<div class="left-50">
				<div class="each-block-lock first-lock alt-lock">
					<div class="generic-content-left">
						<h1><?php echo get_field('left_category_title') ?></h1>
						<p><?php echo category_description(); ?></p>
					</div>
				</div>
			</div>	
			
			<div class="right-50">
				<div class="each-block-info">
					
					<h2 class="spaces"><?php echo get_field('right_category_title') ?></h2>
					
					<div class="toggle-area">

						<?php 
						    $category = get_queried_object();
							$current_cat_id = $category->term_id;

							global $post;

							$runme = get_posts(array( 
							    'post_type' => 'nas-procedures',
							    'showposts' => -1,
							    'tax_query' => array(
							        array(
							            'taxonomy' => 'category',
							            'terms' => $current_cat_id,
							            'field' => 'term_id',
							        )
							    ),
						    	'orderby' => 'title',
								'order'   => 'ASC'

							));
						?>
							
							<ul>
				
						<?php
							foreach ($runme as $post) {
						
								setup_postdata($post);

								$trimmedcontent = wp_trim_words(get_the_content(), 25, '...');
								echo '<li><strong>'. get_the_title() .'</strong>' . $trimmedcontent . ' <a href="'.esc_url( get_permalink()).'">Read more.</a></li>';

								wp_reset_postdata();
							}

						?>
							</ul>
					</div>	
					
				</div>
				
			</div>

		</section>

	</div>

<?php get_footer(); ?>