<?php
/**
 * Template Name: Forms Template
 *
 */
?>

<?php get_header(); ?>

<?php 
	// Start the loop.
	while ( have_posts() ) : the_post();
?>
	<div class="bluearea"></div>
	<div class="container generic mri" id="conditions">
		<section class="each-block-all" id="pinned-lock-1">
			<div class="left-50">
				<div class="each-block-lock first-lock alt-lock">
					<div class="generic-content-left">
					<h1><?php the_title(); ?></h1>
					<p><?php the_content(); ?></p>
					</div>
				</div>
			</div>	
			
			<div class="right-50">
				<div class="each-block-info">

					<h2 class="spaces targetedscrollpoint"><?php echo get_field('form_header'); ?></h2>
							
					<div class="toggle-area">
						<h4><?php echo get_field('form_subheader') ?></h4>

						<!-- call the namespace \ class -->
						<?php echo do_shortcode(get_field('form_shortcode')); ?>

					</div>

				</div>
				
			</div>

		</section>

	</div>
<?php 		
	// End of the loop.
	endwhile;
?>

<?php get_footer(); ?>

