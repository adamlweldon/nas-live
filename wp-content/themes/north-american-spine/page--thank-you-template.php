<?php
/**
 * Template Name: Thank You Template (no blocks)
 *
 */
?>

<?php get_header(); ?>

<?php 
	// Start the loop.
	while ( have_posts() ) : the_post();
?>
	<div class="container generic thank-you" id="conditions">
		<section class="each-block-all" id="pinned-lock-1">
			<!--<div class="left-50">
				<div class="each-block-lock first-lock alt-lock">
					<div class="generic-content-left">
						<h1><?php /*the_title(); */?></h1>

						<div id="webinar" class="thank-you-messages">
							<?php /*echo get_field('webinar_thank_you'); */?>
						</div>
						<div id="weblead" class="thank-you-messages">
							<?php /*echo get_field('web_lead_thank_you'); */?>
						</div>
						<div id="kitrequest" class="thank-you-messages">
							<?php /*echo get_field('kit_request_thank_you'); */?>
						</div>
					</div>
				</div>
			</div>	-->

			<!--<div class="right-50">-->
				<div class="each-block-info" style="margin: 20px;">
					<h2 style="font-size: 2.4rem;">Thank You for Taking the First Step!</h2>
					<div class="toggle-area">
						<h4>
                            Step 2 of 2
                            <progress value="50" max="100" style="display:block; width: 100%;"></progress>
                        </h4>
						<p>
                            Please answer the questions below to help us better understand your condition.
                            This information is required for a thorough case review by our medical team.
                            Your Patient Care Manager will be in touch within one business day to begin the process.
                        </p>
						<?php pgform_thankyou() ?>
					</div>

				</div>
				
			<!--</div>-->

		</section>
	</div>
<?php 		
	// End of the loop.
	endwhile;
?>

<?php get_footer(); ?>

