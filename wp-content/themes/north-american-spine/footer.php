<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package North_American_Spine
 */

?>
		
	</div><!-- #content -->
	<div class="contact-area">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-2 col-md-2 col-lg-3"></div>
				<div class="col-xs-12 col-sm-8 col-md-8 col-lg-6">
					<div class="expertimg"></div>
					<h2>We love your questions!</h2>
					<p>Our friendly Patient Care Managers are standing by and ready to help.</p>

					<div class="orange-toggle hidden-xs hidden-sm">Get Started Now</div>
					<div class="visible-xs visible-sm mobile-form-options">
						<ul>
							<?php if (is_front_page()) : ?>
							<li class="orange-toggle btn-orange">
								<div></div>Get Started Now
							</li>
							<?php else : ?>
							<li class="orange-toggle btn-orange">
								<div></div>Get Started Now
							</li>
							<?php endif; ?>
							<!-- <li>
								<a href="tel:<?php echo get_option('support_phone'); ?>" class="btn-orange"><div></div>Or call us anytime!</a>
							</li> -->
						</ul>
					</div>
				</div>
				<div class="col-xs-12 col-sm-2 col-md-2 col-lg-3"></div>
			</div>
		</div>
		<div class="close-contact">
			<div class="rotate-orange toggled">
				<div></div>
				<div></div>
			</div>
		</div>
	</div>
	<div class="toggle-contact toggled" id="scrollhere">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<?php pgform_main_split() ?>
				</div>
			</div>
		</div>
	</div>

	<!-- modals -->
	<div id="freekit" tabindex="-1" role="dialog" aria-labelledby="consult modal">
		<div class="modal-form home-modal">
			<div class="close-it"></div>
			<h3>Request a Free Info Kit</h3>
			<?php pgform_infokit_reusable() ?>
		</div>
	</div>

	<div id="webinar-modal" tabindex="-1" role="dialog" aria-labelledby="consult modal">
		<div class="modal-form home-modal">
			<div class="close-it"></div>
			<h3>Webinar</h3>
			<?php pgform_webinar(); ?>
		</div>
	</div>

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="backtotop">
			<div class="gototop">
				<p>Back to top</p>
			</div>
		</div>
		<div class="sitemap">
			<div class="container">
				<div class="row foot-top">
					<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 foot-left">
						<?php wp_nav_menu( array( 'theme_location' => 'footer' ) ); ?>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 social-foot">
						<p>For tips, more info, fun, and community, visit us on social media.</p>
						<ul class="socialfooter">
							<li>
								<a href="https://www.facebook.com/northamericanspine" target="_blank">
									<i class="fa fa-facebook"></i>
								</a>
							</li>
							<li>
								<a href="https://twitter.com/NASpine" target="_blank">
									<i class="fa fa-twitter"></i>
								</a>
							</li>
							<li>
								<a href="https://www.youtube.com/user/NorthAmericanSpine" target="_blank">
									<i class="fa fa-youtube"></i>
								</a>
							</li>
							<li>
								<a href="https://www.instagram.com/northamericanspine/" target="_blank">
									<i class="fa fa-instagram"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
				
				<div class="row foot-bottom">
					<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
						<img src="<?php echo NAS_TEMPLATE_DIR; ?>/assets/img/ab-seal-horizontal.svg" alt="Better Business Bureau Accredited Business" class="bbb-logo visible-sm visible-xs">
						<?php wp_nav_menu( array( 'theme_location' => 'footer_bottom' ) ); ?>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
						<img src="<?php echo NAS_TEMPLATE_DIR; ?>/assets/img/ab-seal-horizontal.svg" alt="Better Business Bureau Accredited Business" class="bbb-logo hidden-sm hidden-xs">
					</div>

				</div>
			</div>
		</div>
		<div class="site-info">
			<div class="container">
				<p>Copyright © <?php echo date('Y'); ?> North American Spine · <?php echo get_option('support_phone'); ?></p>

				<?php dynamic_sidebar( 'nas-footer' ); ?>
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
