<?php  /* Template Name: webinar-lp */?>


<!--test-->
<?php wp_head();

// Start the loop.
while ( have_posts() ) : the_post();
    ?>

    <!--Call Tracking Metrics script - https://calltrackingmetrics.com/-->
    <script async src="//18862.tctm.co/t.js"></script>

    <!-- Optimizely Script -->
    <script src="//cdn.optimizely.com/js/73151603.js"></script>
    
<script>

    jQuery(function(){
        //hide the state select box
        jQuery(".imaging-center").hide();
        jQuery(".freeMRI-dob").hide();

        //listen for a vhange event on the insurance field
        jQuery("#mktoRadio_4390_4").change(function(){

            var optionSelected = jQuery("#mktoRadio_4390_4").val();
            if(optionSelected == 'request from Image Center')
            {
                //slide down the state select field if BCBS is selected
                jQuery(".imaging-center").slideDown('100');
                jQuery(".freeMRI-dob").slideDown('100');
                jQuery("input#imagingCenter").prop('required',true);
                jQuery("select#dobMonth").prop('required',true);
                jQuery("select#dobDay").prop('required',true);
                jQuery("input#dobYear").prop('required',true);
                //alert(optionSelected);
            }
            else
            {
                //slide up if not bcbs is selected and set state value to empty sting
                jQuery(".imaging-center").slideUp('100');
                jQuery(".freeMRI-dob").slideDown('100');
            }
        });

    });

</script>
<title>Free Video - North American Spine</title>
<link rel="icon" href="http://new.nas.dev/wp-content/uploads/2017/03/cropped-favicon-1-32x32.png" sizes="32x32" />
<link rel="icon" href="http://new.nas.dev/wp-content/uploads/2017/03/cropped-favicon-1-192x192.png" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="http://new.nas.dev/wp-content/uploads/2017/03/cropped-favicon-1-180x180.png" >
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link rel='stylesheet' id='fontawesome-css'  href='https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css?ver=4.7.5' type='text/css' media='all' />
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo get_stylesheet_directory_uri() ?>/landing/webinar/style.css" />


<!-- header goes here -->
<div class="nas-landing-page site-container">
    <a name="topofform"></a>

    <!-- start fixed-nav-bar -->
    <div class="fixed-nav-bar">

        <div class="fixed-nav-bar-content">
            <div class="logo desktop">
                <img src="/wp-content/themes/north-american-spine/landing/shared/img/nas_logo_final.png">
            </div>
            <div class="logo tablet">
                <img src="/wp-content/themes/north-american-spine/landing/shared/img/nas-logo-mobile.png">
            </div>
            <div class="call-us desktop"><div class="call-us-content"><a href="tel:<?php echo get_option('support_phone'); ?>"><?php echo get_option('support_phone'); ?></a><br/><span>Talk to a Spine Expert</span></div></div>
            <div class="call-us tablet"><a href="tel:<?php echo get_option('support_phone'); ?>"><img class="callnowimg" src="/wp-content/themes/north-american-spine/landing/webinar/img/call-desktop.png"></a></div>
        </div>

    </div><!-- end fixed-nav-bar -->

</div><!-- end fixed-nav-bar -->

<!-- header goes here -->
<div class="accurascope-lp site-container webinar-lp-page">
	<a name="topofform"></a>
    
    <header>
    	<div class="mobile">
        	<div class="logo">
                <a href="/"><img src="/wp-content/themes/north-american-spine/landing/webinar/img/nas-logo-mobile.png"></a>
            </div>
            <div class="call-us"><a href="tel:<?php echo get_option('support_phone'); ?>"><img class="callnowimg" src="/wp-content/themes/north-american-spine/landing/webinar/img/call-desktop.png"></a></div>
        </div>
        <div class="tablet">
        	<div class="logo">
            	<a href="/"><img src="/wp-content/themes/north-american-spine/landing/webinar/img/nas-logo-mobile.png"></a>
        	</div>
            <div class="call-us"><a href="tel:<?php echo get_option('support_phone'); ?>"><img class="callnowimg" src="/wp-content/themes/north-american-spine/landing/webinar/img/call-desktop.png"></a></div>
        </div>
        <div class="desktop">
        	<div class="logo">
                <a href="/"><img src="/wp-content/themes/north-american-spine/landing/webinar/img/nas_logo_final.png"></a>
        	</div>
            <div class="call-us">
            	<div class="call-us-content"><span class="desktop num"><?php echo get_option('support_phone'); ?></span><br/><span class="talk">Talk to a Spine Expert</span></div>
            </div>
        </div>
    </header>
    
    <div class="tagline">
    <p>The Leader in Minimally Invasive Spine Care</p>
    </div>
    
    <div class="hero">
    	<div class="small-testimonial"><img class="callnowimg" src="/wp-content/themes/north-american-spine/landing/webinar/img/small-testimonial-bg.png"></div>
  		<div class="hero-txt"><p>Watch our free video for everything you need to know about the minimally invasive spine surgery that could change your life. Hear from real patients who found relief, expert surgeons, and caring Patient Care Managers.</p></div>
    </div><!-- end .hero -->
    
    <div class="body-section">
    	
        <div class="modules section-one">
            <ul>
            	<li>
                	<h2>Register Now For Video Access</h2>
                    <?php pgform_lp_webinar() ?>
                </li>
                <li>
                	<div class="bullets desktop">
                        <h2>The Benefits of North American Spine:</h2>
                        <ul class="precision">
                            <li><span><strong>Precision.</strong> Tiny incisions and high-definition imaging systems allow for pin-point accuracy, virtually no scarring, and almost zero risk of infection.</span></li>
                            <li><span><strong>Agility.</strong> Advanced instrumentation allows surgeons to diagnose and treat multiple problems in one surgery, if necessary.</span></li>
                            <li><span><strong>Rapid Recovery.</strong> Greatly reduced recovery time compared to traditional, "open" procedures.</span></li>
                        </ul>
                    </div>
                </li>
        	</ul>
        </div>
        
        <div class="bullets mobile tablet">
        	<h2>The Benefits of North American Spine:</h2>
            <ul class="precision">
				<li><span><strong>Precision.</strong> Tiny incisions and high-definition imaging systems allow for pin-point accuracy, virtually no scarring, and almost zero risk of infection.</span></li>
                <li><span><strong>Agility.</strong> Advanced instrumentation allows surgeons to diagnose and treat multiple problems in one surgery, if necessary.</span></li>
                <li><span><strong>Rapid Recovery.</strong> Greatly reduced recovery time compared to traditional, "open" procedures.</span></li>
            </ul>
        </div>

        <div class="testimonials-section">
        	<h1>The exceptional care you can expect:</h1>
        	<div class="section section1">
                <img src='/wp-content/themes/north-american-spine/landing/webinar/img/physicians.png' />
                <h2>The Team</h2>
                <h3>Highly experienced physicians who specialize in minimally invasive techniques</h3>
            </div><!-- end section1 -->
            <div class="section section2">
                <img src='/wp-content/themes/north-american-spine/landing/webinar/img/coordinators.png' />
                <h2>The Process</h2>
                <h3>Patient Care Managers who make every step clear and comfortable</h3>
            </div><!-- end section2 -->
            <div class="section section3">
                <img src='/wp-content/themes/north-american-spine/landing/webinar/img/patients.png' />
                <h2>The Results</h2>
                <h3>Thousands of patients who finally found lasting relief from back or neck pain</h3>
            </div><!-- end section3 -->
        </div><!-- end .bodycopy -->
        
        <div class="video-blurb">
        	<h3>Find Out If You're A Candidate Now</h3>
            <p>North American Spine offers FREE MRI and Insurance reviews.<br />Get started by <a class="trigger-consult" data-toggle="modal" data-target="#consult">filling out this form</a> or call us 24/7 at <a href="tel:<?php echo get_option('support_phone'); ?>"><?php echo get_option('support_phone'); ?></a>.</p>
        </div>
        
        <div class="factssection">
        	<div class="section section1">
            	<img src="/wp-content/themes/north-american-spine/landing/webinar/img/section1-icon.png" class="icon">
                <h3>Conditions Treated</h3>
                <ul>
                	<li>Sciatica</li>
                    <li>Degenerative Disc Disease</li>
                    <li>Bulging or Herniated Disc</li>
                    <li>Pinched Nerve</li>
                    <li>Spinal Stenosis</li>
                    <li>And many more</li>
                </ul>
            </div><!-- end section1 -->
            <div class="section section2">
            	<img src="/wp-content/themes/north-american-spine/landing/webinar/img/section2-icon.png" class="icon">
                <h3>Common Symptoms</h3>
                <ul>
                	<li>Back or neck pain with or without radiating leg/arm pain</li>
                    <li>Back pain that involves the hips, buttocks or legs</li>
                    <li>Weakness and/or numbness in the limbs</li>
                </ul>
            </div><!-- end section2 -->
            <div class="section section3">
            	<img src="/wp-content/themes/north-american-spine/landing/webinar/img/section3-icon.png" class="icon">
                <h3>Free Second Opinions</h3>
                <ul>
                	<li>Free medical review of MRI and benefits means you have nothing to lose and everything to gain</li>
                    <li>Many patients have had previous surgeries</li>
                    <li>We provide a thorough case review to match you with the doctor that best suits your needs</li>
                </ul>
            </div><!-- end section3 -->
        </div>
        
    </div><!-- end .bodysection -->
    
    <footer>
    	<div class="col-sm-12">
                <h3>Our spine specialists are available 24/7 to answer your call. Get started on pain relief now by calling <a href="tel:<?php echo get_option('support_phone'); ?>"><?php echo get_option('support_phone'); ?></a> or <a class="trigger-consult" data-toggle="modal" data-target="#consult">filling out this form</a>. To learn more or explore our full website, <a href="/" target="_blank">click here</a>.</h3>
                <h4>Dallas | Detroit | Houston | Minneapolis | Nashville | Phoenix</h4>
                <p>&copy; <?php echo date('Y'); ?> North American Spine. All rights reserved.</p>
        </div>
    </footer>
    
</div><!-- end accurascope -->

    <!-- Main Modal Popup -->
    <div id="consult" tabindex="-1" role="dialog" aria-labelledby="consult modal" class="">
        <div class="modal-form home-modal">
            <div class="close-it"></div>
            <h3>Know your options. Get a free MRI/CT review.</h3>
            <?php pgform_lp_main() ?>
        </div>
    </div>

    <!-- Webinar Modal Popup -->
    <div id="webinar-modal" tabindex="-1" role="dialog" aria-labelledby="consult modal" class="">
        <div class="modal-form home-modal">
            <div class="close-it"></div>
            <h3>Webinar</h3>
            <?php pgform_lp_webinar() ?>
        </div>
    </div>

    <!-- Get Info Kit Modal Popup -->
    <div id="freekit" tabindex="-1" role="dialog" aria-labelledby="consult modal" class="">
        <div class="modal-form home-modal">
            <div class="close-it"></div>
            <h3>Request a Free Info Kit</h3>
            <?php pgform_infokit_generic() ?>
        </div>
    </div>

    <script src="<?php echo get_template_directory_uri(); ?>/js/scripts.js"></script>

    <script>
        jQuery('a[data-target="#freekit"]').on('click', function(ev){
            // console.log('open it');

            jQuery('#freekit').addClass('visible-time');
            ev.preventDefault();
            return false;

        });

        jQuery('a[data-target="#webinar-modal"]').on('click', function(ev){

            jQuery('#webinar-modal').addClass('visible-time');
            ev.preventDefault();
            return false;

        });

        jQuery('a[data-target="#consult"]').on('click', function(ev){

            jQuery('#consult').addClass('visible-time');
            ev.preventDefault();
            return false;

        });
    </script>

    <script type="text/javascript">
        jQuery(function(){
            jQuery('img[data-url]').click(function () {
                jQuery('.current-show iframe').attr('src', jQuery(this).data('url') + '?rel=0');
            });
        });
    </script>

    <script>
        //fixed nav bar
        var mn = jQuery(".fixed-nav-bar");

        jQuery(window).scroll(function () {
            if( jQuery(this).scrollTop() > 106 ) {
                mn.addClass("main-nav-scrolled");
            } else {
                mn.removeClass("main-nav-scrolled");
            }
        });
        //@ sourceURL=pen.js
    </script>

    <?php
// End of the loop.
endwhile;

wp_footer();
?>
