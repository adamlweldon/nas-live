<?php
/**
 * Template Name: FAQs Template
 *
 */
?>

<?php get_header(); ?>

<?php 
	// Start the loop.
	while ( have_posts() ) : the_post();
?>
	<div class="bluearea"></div>
	<div class="container generic faqs" id="conditions">
		<section class="each-block-all" id="pinned-lock-1">
			<div class="left-50">
				<div class="each-block-lock first-lock alt-lock">
					<div class="generic-content-left">
						<h1><?php the_title(); ?></h1>
						<p><?php the_content(); ?></p>
						<h3 class="call-us">Call Us Anytime:</h3>
						<a href="tel:<?php echo get_option('support_phone'); ?>" class="content-phone"><i class="phone"></i><?php echo get_option('support_phone'); ?></a>
					</div>
				</div>
			</div>	
			
			<div class="right-50">
				<div class="each-block-info">

					<?php if( have_rows('each_content_block') ): ?>
						<?php while( have_rows('each_content_block') ): the_row(); ?>
							<h2 class="spaces targetedscrollpoint" id="generic-sectionscroll-<?php echo get_row_index(); ?>"><span class="title-entry">Q:</span><?php echo get_sub_field('each_content_block_title'); ?></h2>
							
							<div class="toggle-area">
								<!-- if subheader exists -->
								<?php if (get_sub_field('each_content_block_subheader') != ''): ?>
									<h4><?php echo get_sub_field('each_content_block_subheader'); ?></h4>
								<?php endif ?>
							
								<!-- if content exists -->
								<?php if (get_sub_field('each_content_block_content') != ''): ?>
									<p><?php echo get_sub_field('each_content_block_content'); ?></p>
								<?php endif ?>

								<!-- if button exists -->
								<?php if (get_sub_field('each_content_block_link_text') != ''): ?>	
									<a href="<?php echo get_sub_field('each_content_block_link_url'); ?>" class="btn-orange invert"><div></div><?php echo get_sub_field('each_content_block_link_text'); ?></a>
								<?php endif ?>
							</div>
							
						<?php endwhile ?>
					<?php endif ?>	

				</div>
				
			</div>

		</section>

	</div>
<?php 		
	// End of the loop.
	endwhile;
?>

<?php get_footer(); ?>

