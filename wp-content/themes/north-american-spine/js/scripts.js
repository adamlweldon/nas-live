/**
 * jQuery Wrapper
 */
(function($) {

    // conditions scroll magic
    function runscrollmagic(){

        var maxHeight = -1;
        $('.each-block-lock').each(function() {
            if ($(this).height() > maxHeight) {
                maxHeight = $(this).height();
            }
        });

        if ($(window).height() >= maxHeight + 195  && $(window).width() >= 992) {
            // init ScrollMagic Controller
            var controller = new ScrollMagic.Controller();
            var getdur = $('#pinned-lock-1 .each-block-info').height() - $('.first-lock').height();
            getdur = getdur < 0 ? 1 : getdur;
            // Scene Handler
            var scene1 = new ScrollMagic.Scene({
                triggerElement: "#pinned-lock-1", // point of execution
                offset: -135, // start scene after scrolling for x px
                duration: getdur, // pin element for the window height - 1
                triggerHook: 0, // don't trigger until #pinned-trigger1 hits the top of the viewport
                reverse: true // allows the effect to trigger when scrolled in the reverse direction
            })
                .setPin(".first-lock") // the element we want to pin
                .addTo(controller);

            var totalsections = $('.each-block-lock:not(#testimony-scroll-to)').length;

            var scene = new Array;

            for (var i = 1; i < totalsections; i++) {
                // console.log('run-me'+ i);
                var useme = 0;
                if ($('#back-pain-'+i+' .each-block-info').height() - $('#back-pain-'+i+' .lock-it').height() < 0) {  useme = 1 }else{  useme = $('#back-pain-'+i+' .each-block-info').height() - $('#back-pain-'+i+' .lock-it').height()};
                scene[i] = new ScrollMagic.Scene({
                    triggerElement: "#back-pain-"+i,
                    offset: -135,
                    triggerHook: 0,
                    duration: useme,
                    reverse: true
                })
                    .setPin("#back-pain-"+i+" .lock-it")
                    .addTo(controller);
            }
        }
    }

    // nav dots active
    function dotsection(){
        var scrollTop = $(this).scrollTop();

        $('#conditions section').each(function() {
            var topDistance = $(this).offset().top;

            if ( (topDistance-136) < scrollTop ) {
                $('.nav-dots li:not([data-target='+targetedfire+'])').removeClass('active');
                var targetedfire = $(this).attr('id');
                $('[data-target='+targetedfire+']').addClass('active');
            }
        });
    }

    function conddotsection(distance1, distance2, distance3, distance4){

        if ( $(window).scrollTop() >= distance1 - 135 ) {
            $('.nav-dots li:not([data-target="symptoms-scroll-to"])').removeClass('active');
            $('[data-target="symptoms-scroll-to"]').addClass('active');
        }
        if ( $(window).scrollTop() >= distance2 - 135 ) {
            $('.nav-dots li:not([data-target="causes-scroll-to"])').removeClass('active');
            $('[data-target="causes-scroll-to"]').addClass('active');
        }
        if ( $(window).scrollTop() >= distance3 - 135 ) {
            $('.nav-dots li:not([data-target="request-scroll-to"])').removeClass('active');
            $('[data-target="request-scroll-to"]').addClass('active');
        }
        if ( $(window).scrollTop() >= distance4 - 135 ) {
            $('.nav-dots li:not([data-target="testimony-scroll-to"])').removeClass('active');
            $('[data-target="testimony-scroll-to"]').addClass('active');
        }
    }

    function genericdots(){
        var scrollTop = $(this).scrollTop();
        $('#conditions.generic .targetedscrollpoint').each(function() {
            var topDistance = $(this).offset().top;

            if ( (topDistance-165) < scrollTop ) {
                $('.nav-dots li:not([data-target='+targetedfire+'])').removeClass('active');
                var targetedfire = $(this).attr('id');
                $('[data-target='+targetedfire+']').addClass('active');
            }
        });
    }

    function getUrlVars(){
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for(var i = 0; i < hashes.length; i++)
        {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }


    //
    // all scroll functions
    //
    $(window).resize(function() {

        if ($(window).width() > 991) {
            if ($('.phone-spot').hasClass('open-mob')) {
                // console.log('it does');
                $('.phone-spot').removeClass('open-mob');
            }
        } else {
            if ( $(window).scrollTop() >= 60 ) {
                $('#page').addClass('fixtime');
                $('.phone-spot').addClass('open-mob');
            }
        }
    })

    $(window).scroll(function(e) {
        // nav bar
        if ($(window).width()>991) {
            if ( $(window).scrollTop() >= 60 ) {
                $('#page').addClass('fixtime');
                $('.ask-expert').removeClass('toggled');
            }
            if ($('#page').hasClass('fixtime') && $(window).scrollTop() <= 59) {
                $('#page').removeClass('fixtime');
                if (window.location.href.indexOf('/') > -1) {
                    $('.ask-expert').addClass('toggled');
                }
                else if (window.location.pathname == '/back-pain-lp/') {
                    $('.ask-expert').addClass('toggled');
                }
                else if (window.location.pathname == '/minimally-invasive-spine-surgery-lp/') {
                    $('.ask-expert').addClass('toggled');
                }
                else if (window.location.pathname == '/laser-spine-treatment-lp/') {
                    $('.ask-expert').addClass('toggled');
                }
                else if (window.location.pathname == '/laser-spine-treatment-national/') {
                    $('.ask-expert').addClass('toggled');
                }
            }

            if(document.querySelector('.alt-conditions') !== null){
                conddotsection(distance1, distance2, distance3, distance4);
            }
            else if(document.querySelector('#conditions:not(.generic) .each-block-lock') !== null){
                dotsection();
            }
            else if(document.querySelector('#conditions.generic:not(.faqs) .each-block-lock') !== null){
                genericdots();
            }

            if(document.querySelector('#blog .single-post') !== null){
                if ($(window).width() > 991) {
                    var distTop = $(window).scrollTop();

                    // $('.request-info-kit').each(function() {
                    var topDistance = $('.request-info-kit').offset().top;

                    if ( (topDistance - 135) < distTop ) {
                        //     // alert('yes');
                        $('.request-info-kit').addClass('fixer');
                        $('.request-info-kit').css({
                            'position' : 'fixed',
                            'top' : '135px',
                            'z-index' : '-1'
                        })
                    }


                    if (topDistance < 321 && $('.request-info-kit').hasClass('fixer')) {
                        $('.request-info-kit').removeClass('fixer');
                        $('.request-info-kit').css({
                            'position' : 'relative',
                            'top' : '0'
                        });
                    }
                    // console.log(topDistance);
                    // if ( (topDistance < 136) ) {
                    //
                    // console.log(distTop + ' scroled from top');
                    // console.log(topDistance + ' total from top');
                    // else{
                    // 	$(this).css({
                    //     	'position' : 'relative',
                    //     	'top' : 'initial',
                    //     	'padding-left' : '0px'
                    //     })
                    // }
                    // });
                }
            }
        } else {
            if ( $(window).scrollTop() >= 60 ) {
                $('#page').addClass('fixtime');
                $('.phone-spot').addClass('open-mob');
            }
            if ($('#page').hasClass('fixtime') && $(window).scrollTop() <= 59) {
                $('#page').removeClass('fixtime');
                $('.phone-spot').removeClass('open-mob');
            }
        }
    });



    //
    // all ready functions
    //
    $(document).ready(function(){

        var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;

        if (iOS) {
            $('body').addClass('ios-mobile');
        }

        $('#triggerform').on('click',function(){
            $('.toggle-contact').addClass('toggled');
            $('html, body').animate({
                scrollTop: $("#scrollhere").offset().top - 135
            }, 1000);
        });

        // loadMore();
        if ( window.location.pathname == '/' ){

            if ($(window).width()>991 && $(window).width()<1200) {
                $('.main-navigation #primary-menu > li').hover(function(){
                    $('.main-navigation .ask-expert').removeClass('toggled');
                });
            }
        }

        $('.home-modal .close-it').on('click', function(){
            var yturl = $('.home-modal iframe').attr('data-attr');
            $('.home-modal iframe').attr('src','');
            $('.home-modal iframe').attr('src', yturl);
        });

        $('h6.toggle').on('click', function(){
            $('.toggle-area').removeClass('toggle-time');
            if ($(this).hasClass('toggled')) {
                $('h6.toggle').removeClass('toggled');
                $(this).next().removeClass('toggle-time');
            }else{
                $('h6.toggle').removeClass('toggled');
                $(this).addClass('toggled');
                $(this).next().addClass('toggle-time');
            }




        });

        if ( window.location.pathname == '/locations/') {

            $('.menu-item.current_page_parent a').on('click', function(e) {
                if ($(window).width()<991) {
                    $('.main-navigation').removeClass('toggled')
                    $('.mobnav-toggle').removeClass('toggled')
                }
              
            })

        }

        if(document.querySelector('.each-block-lock') !== null){
            runscrollmagic();


            $('.each-block-info h2:not(.no-toggle)').on('click', function(){
                if ($(window).width() < 992) {
                    $(this).toggleClass('sub-toggle');
                    $(this).next().slideToggle();
                }
            });
            $('h2.proc-cond-toggle').on('click', function(){
                if ($(window).width() < 992) {
                    $(this).toggleClass('sub-toggle');
                    $(this).next().slideToggle();
                }
            });

            $('.nav-dots li').on('click', function(){
                var targetsection = $(this).attr('data-target');
                $("html, body").animate({ scrollTop: $('#'+targetsection).offset().top - 135 }, 500);
            });




            if ($(window).width() < 992) {
                var top = 0;
                if (!(window.location.href.indexOf("locations") > -1)) {
                    top: 50;
                }
                $('.smoothscroll').on('click', function(){
                    event.preventDefault();
                    $('html, body').animate({
                        scrollTop: $( $.attr(this, 'href') ).offset().top + top
                    }, 500);
                });
            }
            else{
                var top = 0;
                if (!(window.location.href.indexOf("locations") > -1)) {
                    top: 150;
                }
                $('.smoothscroll').on('click', function(){
                    event.preventDefault();
                    $('html, body').animate({
                        scrollTop: $( $.attr(this, 'href') ).offset().top - top
                    }, 500);
                });
            }
        }

        // open dropdown form
        $('.toggle-abs-section').on('click', function(){
            $('.ask-expert').toggleClass('toggled');
        });

        // close dropdown form
        $('.toggle-close').on('click', function(){
            $('.ask-expert').removeClass('toggled');
        });

        // footer scroll to top of page
        $('.gototop').on('click',function(){
            $('html, body').animate({ scrollTop: 0 }, 'slow');
        });

        if (window.location.pathname == '/'){

        }else{

            $('.orange-toggle').on('click', function(){
                $('.toggle-contact').toggleClass('toggled');
                $('.rotate-orange').toggleClass('toggled');
            });

            $('.rotate-orange div').on('click', function(){
                $('.toggle-contact').toggleClass('toggled');
                $('.rotate-orange').toggleClass('toggled');
            });
        }

        $('a[data-target="#freekit"]').on('click', function(ev){
            // console.log('open it');
            $('#freekit').addClass('visible-time');
            ev.preventDefault();
            return false;
        });

        $('a[data-target="#webinar-modal"]').on('click', function(ev){
            $('#webinar-modal').addClass('visible-time');
            ev.preventDefault();
            return false;
        });

        $('a[data-target="#mrict-modal"]').on('click', function(ev){
            $('#mrict-modal').addClass('visible-time');
            ev.preventDefault();
            return false;
        });

        $('.request-alt-block a').on('click', function(ev){
            // console.log('open it');
            $('#freekit').addClass('visible-time');
            ev.preventDefault();
            return false;
        });
        // open consult modal
        $('.trigger-consult').on('click', function(){
            // var lookfor = $(this).attr('data-target');
            // console.log(lookfor);
            $('#consult').addClass('visible-time');
        });
        // close consult modal
        $('.close-it').on('click', function(){
            $('#consult').removeClass('visible-time');
            $('#freekit').removeClass('visible-time');
            $('#webinar-modal').removeClass('visible-time');
            $('#mrict-modal').removeClass('visible-time');
        });

        $('.scroll-section a').on('click', function(e){
            e.preventDefault();
            var smoothto = $(this).attr('href');
            if (!(window.location.href.indexOf("locations") > -1)) {
                if ($(window).width()>991) {
                    $('html, body').animate({
                        scrollTop: $(smoothto).offset().top - 165
                    }, 500);
                }
                else{
                    $('html, body').animate({
                        scrollTop: $(smoothto).offset().top - 50
                    }, 500);
                }
            } else {
                if ($(window).width()>991) {
                    $('html, body').animate({
                        scrollTop: $(smoothto).offset().top
                    }, 500);
                }
                else{
                    $('html, body').animate({
                        scrollTop: $(smoothto).offset().top
                    }, 500);
                }
            }
        });


        // youtube homepage
        $('.play-vid, .repeat-vid').on('click', function() {
            var hover = $('.hover-video');
            hover.addClass('fadeout');

        });

        $('.play-vid').on('click', function(e) {
            var hover = $('.hover-video');
            if (!hover.hasClass('playing')) {
                hover.addClass('playing');
            }
            $('#yt-home').addClass('playing-now');
            player.playVideo()

        })
        $('.repeat-vid').on('click', function(e) {
            var hover = $('.hover-video');
            player.pauseVideo();
            player.seekTo(0);
            if (hover.hasClass('playing')) {
                hover.removeClass('playing');
            }
        })


        // pause youtube video
        $('.pause-vid').on('click', function(){
            player.pauseVideo();
            $('#yt-home').removeClass('playing-now');
            var hover = $('.hover-video');
            if (hover.hasClass('playing')) {
                hover.removeClass('playing');
            }
            hover.removeClass('fadeout');
        });

        // homepage slider
        if ($('.hero').length && $('.hero .each-slide').length > 1) {
            $('.hero').slick({
                arrows : false,
                autoplay : true,
                autoplaySpeed : 7500,
                fade : true,
                speed : 1000
            });
        }


        // global slider with fade
        if ($('.slick-it').length) {
            $('.slick-it').slick({
                arrows : false,
                autoplay : true,
                dots: true,
                autoplaySpeed : 7500,
                fade : true,
                speed : 1000
            });
        }

        $('input[type="checkbox"]').on('change', function(){

            if ($('input[type="checkbox"]').is(':checked')) {
                $(this).closest('label').addClass('checked-animate');

            }
            else{
                $(this).closest('label').removeClass('checked-animate');
            }
        });


        // grow ask expert form if bcbs or other selected
        $('.ask-expert').on('change', '.insurance-provider', function(){
            if ($(window).width()>991) {
                // console.log('change');
                var checkit = $('.ask-expert form .insurance-provider option:selected').text();
                var currentheight = $('.ask-expert.toggled .form-area').height();
                // console.log(checkit);
                if (checkit == 'BCBS' || checkit == 'Other Insurance' || checkit == 'BCBSInsurance Provider' || checkit == 'Other InsuranceInsurance Provider'  || checkit == 'BCBSInsurance (Primary)' || checkit == 'Other InsuranceInsurance (Primary)' || checkit == 'Insurance (Primary)BCBS' || checkit == 'Insurance (Primary)Other Insurance') {
                    // console.log('yes');
                    var newheight = currentheight + 60
                    $('.ask-expert.toggled .form-area').css({
                        'height' : newheight
                    });
                }
            }
            else{
                var checkit = $('.ask-expert form .insurance-provider option:selected').text();
                var currentheight = $('.ask-expert .form-area').height();
                // console.log(checkit);
                if (checkit == 'BCBS' || checkit == 'Other Insurance' || checkit == 'BCBSInsurance Provider' || checkit == 'Other InsuranceInsurance Provider'  || checkit == 'BCBSInsurance (Primary)' || checkit == 'Other InsuranceInsurance (Primary)' || checkit == 'Insurance (Primary)BCBS' || checkit == 'Insurance (Primary)Other Insurance') {
                    // console.log('yes');
                    var newheight = currentheight + 60
                    $('.ask-expert.toggled .form-area').css({
                        'height' : newheight
                    });
                }
            }

        });

        // grow footer form if bcbs or other selected
        $('.toggle-contact').on('change', '.insurance-provider', function(){
            // console.log('change');
            var checkit = $('.toggle-contact form .insurance-provider option:selected').text();
            var currentheight = $('.toggle-contact.toggled').height();
            // console.log(checkit);
            if (checkit == 'BCBS' || checkit == 'Other Insurance' || checkit == 'BCBSInsurance Provider' || checkit == 'Other InsuranceInsurance Provider') {
                // console.log('yes');
                var newheight = currentheight + 60
                $('.toggle-contact.toggled').css({
                    'height' : newheight
                });
            }
        });

        $('.close-stuff').on('click', function(){
            $('#search').removeClass('toggled');
        });

        $('#search').click(function(){
            window.setTimeout(function() {
                $('#search .search-field').focus();
                console.log('focus');
            }, 500);
        });

        if ($(window).width()>991) {

            if (document.querySelector('.faqs') !== null) {
                $('.each-block-info h2:not(.no-toggle)').on('click', function () {
                    $(this).toggleClass('sub-toggle');
                    $(this).next().slideToggle();
                });
            }
        }

        $('.mobnav-toggle').on('click', function(){
            // console.log('clicked');
            if ($(document).scrollTop() < 60){
                $("html, body").animate({ scrollTop: 60 }, 100);
            }
            $('.main-navigation, .mobnav-toggle').toggleClass('toggled');
            $('#primary-menu > li').removeClass('toggled');
        });


        if ($(window).width() > 991 ) {
            if(document.querySelector('.alt-conditions:not(.each-procedure):not(.treatment)') !== null){
                var distance1 = $('#symptoms-scroll-to').offset().top,
                    distance2 = $('#causes-scroll-to').offset().top,
                    distance3 = $('#request-scroll-to').offset().top,
                    distance4 = $('#testimony-scroll-to').offset().top;
            }
            if(document.querySelector('#conditions.generic .each-block-lock') !== null){

            }

            $('.fa-search:not(form)').on('click', function(e){
                if (e.target !== this) return;
                $(this).toggleClass('toggled');

            });
            $('.fa-search input[type="search"]').on('blur', function(){
                $('.fa-search').toggleClass('toggled');
            });

            if(window.location.href.indexOf("#all-back-pain") > -1) {
                $('li[data-target="back-pain-1"').delay(500).trigger("click");
            }
            if(window.location.href.indexOf("#lower-back-pain") > -1) {
                $('li[data-target="back-pain-2"').delay(500).trigger("click");
            }
            if(window.location.href.indexOf("#upper-back-pain") > -1) {
                $('li[data-target="back-pain-3"').delay(500).trigger("click");
            }
            if(window.location.href.indexOf("#neck-pain") > -1) {
                $('li[data-target="back-pain-4"').delay(500).trigger("click");
            }
        } else {

            if(window.location.href.indexOf("#all-back-pain") > -1) {
                $('#back-pain-1').prev().delay(100).trigger("click");
                // console.log($("#back-pain-1").offset().top);
                $('html, body').delay(101).animate({
                    scrollTop: $("#back-pain-1").offset().top - 150
                }, 500);
            }
            if(window.location.href.indexOf("#lower-back-pain") > -1) {
                $('#back-pain-2').prev().delay(100).trigger("click");
                // console.log($("#back-pain-1").offset().top);
                $('html, body').delay(101).animate({
                    scrollTop: $("#back-pain-2").offset().top - 150
                }, 500);
            }
            if(window.location.href.indexOf("#upper-back-pain") > -1) {
                $('#back-pain-3').prev().delay(100).trigger("click");
                // console.log($("#back-pain-1").offset().top);
                $('html, body').delay(101).animate({
                    scrollTop: $("#back-pain-3").offset().top - 150
                }, 500);
            }
            if(window.location.href.indexOf("#neck-pain") > -1) {
                $('#back-pain-4').prev().delay(100).trigger("click");
                // console.log($("#back-pain-1").offset().top);
                $('html, body').delay(101).animate({
                    scrollTop: $("#back-pain-4").offset().top - 150
                }, 500);
            }
        }


        $('#primary-menu > li').on('click', function(){
            if ($(window).width() < 991) {
                $(this).toggleClass('toggled');
                $('#primary-menu > li').not($(this)).removeClass('toggled');
            }
        });

        $('.search-mobile #search').on('click', function(){
            if ($(window).width() < 991) {
                $('.search-mobile').submit();
            }
        });


    });

    if (window.location.href.indexOf("thank-you") > -1) {
        $('.contact-area, #search, .ask-expert').remove();
        if ($(window).width() > 991){
            $('.main-navigation .menu-main-container, .main-navigation .menu-alt-main-menu-container').css('top', 0);
        }


        var me = getUrlVars()["thanks"];
        var newme = me.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-')
        var withhash = ('#'+newme);
        $(withhash).css('display', 'block');
        // remove form in footer
    }

    $(document).keyup(function(e) {
        if (e.keyCode == 27) { // escape key maps to keycode `27`
            $('.fa-search').removeClass('toggled');
        }
    });

    $('.bday').bind('keyup','keydown', function(event) {
        var inputLength = event.target.value.length;
        if (event.keyCode != 8){
            if(inputLength === 2 || inputLength === 5){
                var thisVal = event.target.value;
                thisVal += '/';
                $(event.target).val(thisVal);
            }
        }
    })

})(jQuery);




