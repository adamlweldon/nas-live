<?php
/**
 * Default search form
 *
 */
?>

<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
	<button type="submit" class="search-submit"></button>
	<label>
		<input type="search" class="search-field" placeholder="Site Search"
		name="s" title="<?php echo esc_attr_e( 'Search for:' ); ?>" />
		<div class="close-stuff"></div>
	</label>
</form>