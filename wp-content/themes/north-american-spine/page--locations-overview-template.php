<?php
/**
 * Template Name: Locations Overview Template
 *
 */
?>

<?php get_header(); ?>

<?php 
	// Start the loop.
	while ( have_posts() ) : the_post();
?>
	<div class="bluearea"></div>
	<div class="container generic locations" id="conditions">
		<section class="each-block-all" id="pinned-lock-1">
			<div class="left-50">
				<div class="each-block-lock first-lock alt-lock">
					<div class="generic-content-left">
						<h1><?php the_title(); ?></h1>
						<p><?php the_content(); ?></p>
						<ul>
						<?php
							$args = array('parent' => 21);
							$categories = get_categories( $args );
							foreach($categories as $category) { 

								$r = (strstr($category->name, ',') ? substr($category->name, 0, strpos($category->name, ',')) : $category->name);
						?>
								<li>
									<a href="#scroll-<?php echo $category->slug; ?>" class="smoothscroll"><?php echo $r; ?></a>
								</li>
						<?php 
							    
							}
						?>
							
						</ul>
						<!-- <div class="map">
							<img src="../wp-content/themes/north-american-spine/assets/img/map_loc.png" alt="NAS Locations" class="hidden-xs">
							<img src="../wp-content/themes/north-american-spine/assets/img/map_mob.png" alt="NAS Locations" class="visible-xs">
							<ul class="hidden-xs">
								<?php
									$args = array('parent' => 21);
									$categories = get_categories( $args );
									foreach($categories as $category) { 

										$r = (strstr($category->name, ',') ? substr($category->name, 0, strpos($category->name, ',')) : $category->name);
								?>
										<li>
											<a href="#scroll-<?php echo $category->slug; ?>" class="smoothscroll style-<?php echo $category->slug; ?>"><div class="hover-content"><?php echo $category->name; ?></div></a>
										</li>
								<?php 
									    
									}
								?>
							</ul>
						</div> -->
					</div>
				</div>
			</div>	
			
			<div class="right-50">
				<div class="each-block-info">
					<h2 class="spaces" id="each-loc-learn"><?php echo get_field('locations_block_title'); ?></h2>
					
					<div class="toggle-area">

						<!-- if subheader exists -->
						<?php if (get_field('locations_sub_header') != ''): ?>
							<h4 class="location-icon"><?php echo get_field('locations_sub_header'); ?></h4>
						<?php endif ?>
					
						<!-- if content exists -->
						<?php if (get_field('locations_block_info') != ''): ?>
							<p><?php echo get_field('locations_block_info'); ?></p>
						<?php endif ?>
						
					</div>
				</div>
			</div>
		</section>
		<section id="back-pain-1" class="each-block-all">
			<div class="left-50">
				<div class="each-block-lock lock-it">

					<div class="map">
						<img src="../wp-content/themes/north-american-spine/assets/img/map_loc.png" alt="NAS Locations" class="hidden-xs hidden-sm">
						<img src="../wp-content/themes/north-american-spine/assets/img/map_mob.png" alt="NAS Locations" class="visible-xs visible-sm">
						<ul class="hidden-xs hidden-sm">
							<?php
								$args = array('parent' => 21);
								$categories = get_categories( $args );
								foreach($categories as $category) { 

									$r = (strstr($category->name, ',') ? substr($category->name, 0, strpos($category->name, ',')) : $category->name);
							?>
									<li>
										<a href="#scroll-<?php echo $category->slug; ?>" class="smoothscroll style-<?php echo $category->slug; ?>"><div class="hover-content"><?php echo $category->name; ?></div></a>
									</li>
							<?php 
								    
								}
							?>
						</ul>
					</div>
				</div>
			</div>

			<div class="right-50">
				<div class="each-block-info">
					<?php

						$args = array('post_type' => 'nas-locations', 'child_of' => 21);
						$categories = get_categories( $args );
						foreach($categories as $category) { 

							global $post;

							$runme = get_posts(array( 
							    'post_type' => 'nas-locations',
							    'showposts' => -1,
							    'tax_query' => array(
							        array(
							            'taxonomy' => 'category',
							            'terms' => $category->cat_ID,
							            'field' => 'term_id',
							        )
							    )

							));
					?>
						
							<h2><span class="scroll-to-span" id="scroll-<?php echo $category->slug; ?>"><?php //echo $category->name; ?></span><?php echo $category->name; ?></h2>

							<div class="toggle-area locs">
								<!-- <h4>More information about this location, it's doctors, and it's accreditation.</h4> -->
								<?php 
									foreach ($runme as $post) {
										
										setup_postdata($post);

										echo '<h3 class="location-title">'.get_the_title().'</h3><p>'.get_field('overview_block_address').'</p><a href="' . esc_url( get_permalink()) . '" class="btn-orange invert"><div></div>View Facility</a>';

										wp_reset_postdata();
									}
								?>
							</div>
					<?php 
							wp_reset_postdata();
						}
					?>
							
				</div>
				
			</div>

		</section>

			
		<div class="nav-dots">
			<ul>
				<li data-target="each-loc-learn"><div class="fade-slide">Learn More About Each Location</div></li>
				<?php 
					foreach($categories as $category) { 							
					$r = (strstr($category->name, ',') ? substr($category->name, 0, strpos($category->name, ',')) : $category->name);
				?>
					<li data-target="scroll-<?php echo $category->slug; ?>"><div class="fade-slide"><?php echo $r; ?> Locations</div></li>
				<?php 
						    
					}
				?>
			</ul>
		</div>

	</div>
<?php 		
	// End of the loop.
	endwhile;
?>

<?php get_footer(); ?>

