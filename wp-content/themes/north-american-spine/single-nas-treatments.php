<?php
/**
 * Template Name: Each Condition Template Page
 *
 */
?>

<?php get_header(); ?>

<?php 
	// Start the loop.
	while ( have_posts() ) : the_post();
?>
	<div class="bluearea"></div>
	<div class="container treatment alt-conditions" id="conditions">
		<section class="each-block-all" id="pinned-lock-1">
			<div class="left-50">
				<div class="each-block-lock first-lock alt-lock">
					<?php 
						$thumb_id = get_post_thumbnail_id();
						$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
						$thumb_url = $thumb_url_array[0];
						$image_alt = get_post_meta( $thumb_id, '_wp_attachment_image_alt', true);	
					?>

					<?php if ($thumb_id != ''): ?>
						<div class="condition-content-left">
							<h1>Treatment Options for <?php the_title(); ?></h1>
							<?php the_content(); ?>
						</div>
						<div class="right-img">
						
							<img src="<?php echo $thumb_url; ?>" alt="<?php echo $image_alt; ?>">
						</div>
					<?php else: ?>
						<h1>Treatment Options for <?php the_title(); ?></h1>
						<?php the_content(); ?>
					<?php endif ?>
				</div>
			</div>	
			
			<div class="right-50">
				<div class="each-block-info targetedscrollpoint" id="symptoms-scroll-to">
					<h2>Procedure Options</h2>
					
					<div class="toggle-area">
						<h4><?php echo get_field('each_condition_symptoms_subheader'); ?></h4>

						<ul class="causesof"><?php echo get_field('recommended_procedures_list_title'); ?>


								
							<?php
								if( have_rows('associated_procedures') ):

									while( have_rows('associated_procedures') ): the_row(); 

										$pro_object = get_sub_field('linked_procedure');
										$pro = $pro_object;
										setup_postdata($pro); 

										$trimmedcontent = get_post_field('post_content', $pro->ID);
										$trimmedcontent = wp_trim_words($trimmedcontent, 25, '...');
							?>

										<li>
											<strong><?php echo get_post_field('post_title', $pro->ID); ?>: </strong><?php echo $trimmedcontent; ?> <a href="<?php the_permalink( $pro->ID ); ?>"> Read more.</a>
										</li>

										<?php wp_reset_postdata(); ?>
								<?php endwhile; ?>
							<?php endif; ?>
							
						</ul>

						<p><?php echo get_field('recommended_procedures_content'); ?></p>
						<p>If you do have an MRI or CT-scan that is 3 years old or less, we will review it free of charge. </p>

						<a href="/patient-care/free-mri-review/" class="btn-orange invert"><div></div>Free MRI/CT Review</a>
					</div>

				</div>
				
			</div>

		</section>

		<section class="each-block-all info testimony-info-block">

			<div class="left-50">
				<div class="each-block-lock targetedscrollpoint" id="testimony-scroll-to">
					<h2><?php the_title(); ?> Testimonial</h2>

					<?php 

						$post_object = get_field('featured_testimonial');
						$post = $post_object;
						setup_postdata($post); 
						
					?>

					<div class="patient-data">

						<?php 

							$thumb_id = get_post_thumbnail_id();
							$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
							$thumb_url = $thumb_url_array[0];
							$image_alt = get_post_meta( $thumb_id, '_wp_attachment_image_alt', true);
						?>
						
						<?php if ($thumb_url_array[1] != 48): ?>
							<img src="<?php echo $thumb_url; ?>" alt="<?php echo $image_alt; ?>">
						<?php endif ?>

						<div class="patient-info">
							<h4><?php echo get_field('testimonial_block_patient_name', $post->ID); ?></h4>
							
							<?php 
								$cond = get_field('testimonial_associated_condition', $post->ID);
								$proc = get_field('testimonial_page_link', $post->ID);
							?>
							<p>Condition: <a href="<?php the_permalink( $cond->ID ); ?>"><?php echo $cond->post_title; ?></a></p>
							<p>Procedure: <a href="<?php the_permalink( $proc->ID ); ?>"><?php echo $proc->post_title; ?></a></p>
							<?php if (get_field('testimonial_block_hometown', $post->ID) != ''): ?>
								<p>Hometown: <span><?php echo get_field('testimonial_block_hometown', $post->ID) ?></span></p>
							<?php endif ?>
							
						</div>
						
					</div>
					<h6 class="quote">“<?php echo get_field('testimonial_block_quote', $post->ID); ?>”</h6>
					<h4 class="lifestyle"><?php echo get_field('testimonial_block_subheader', $post->ID); ?></h4>
					<p><?php echo get_field('testimonial_block_content', $post->ID); ?></p>
					<div class="center-btns">

						<a href="<?php the_permalink( $post->ID ); ?>" class="btn-orange first-space"><div></div>Read Full Story</a>
						<a href="/testimonials" class="btn-orange hidden-md"><div></div>See All Testimonials</a>
					</div>
					<?php wp_reset_postdata(); ?>
				</div>
			</div>	
			
			<div class="right-50">
				<div class="each-block-info">
					<h2>Connect With A Patient</h2>
					
					<div class="toggle-area">
						<p>Some of our former patients have been kind enough to offer to speak with back and neck pain sufferers who are looking into treatment options. We encourage you to take them up on it! After we determine a treatment plan, we’ll connect you with a past patient who had a similar condition and procedure.</p>
						<a href="/connect-with-a-patient" class="btn-orange invert"><div></div>Connect with a Patient</a>
					</div>
				</div>
				
			</div>
		</section>

		<div class="nav-dots">
			<ul>
				<li data-target="symptoms-scroll-to"><div class="fade-slide">Procedure Options</div></li>
				<li data-target="testimony-scroll-to"><div class="fade-slide">Patient Testimonial</div></li>
			</ul>
		</div>

	</div>
<?php 		
	// End of the loop.
	endwhile;
?>

<?php get_footer(); ?>

