<?php
/**
 * Template Name: Each Location Template Page
 *
 */
?>

<?php get_header(); ?>

<?php 
	// Start the loop.
	while ( have_posts() ) : the_post();
?>
	<div class="bluearea"></div>
	<div class="container each-loc" id="conditions">
		<section class="each-block-all" id="pinned-lock-1">
			<div class="left-50">
				<div class="each-block-lock first-lock alt-lock">
					<h1><?php the_title(); ?></h1>

					<h4><?php echo get_field('overview_block_city_state'); ?></h4>
					<ul class="address">
						<li>
							<address><?php the_title(); ?><br><?php echo get_field('overview_block_address'); ?></address>
						</li>
						<li class="phone"><a href="tel:<?php echo get_option('support_phone'); ?>"><?php echo get_option('support_phone'); ?></a></li>
					</ul>
					<?php if (get_field('overview_block_content') != ''): ?>
						<p class="italics"><?php echo get_field('overview_block_content'); ?></p>
					<?php endif ?>
					
					<div class="orange-grad trigger-consult" data-toggle="modal" data-target="#consult">Schedule Consult</div>
					
				</div>
			</div>	
			
			<div class="right-50">
				<div class="each-block-info targetedscrollpoint" id="symptoms-scroll-to">
					<h2>Plan Your Trip Now</h2>
					
					<div class="toggle-area">
						<h4><?php echo get_field('plan_your_trip_subheader'); ?></h4>
						<h4 class="location-icon"><?php the_title(); ?> Overview</h4>
						<p class="with-icon"><?php the_content(); ?></p>
					</div>

					<?php 
					$actualimage =  wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'thumbnail');
					$image_alt = get_post_meta( get_post_thumbnail_id($post->ID), '_wp_attachment_image_alt', true);
					?>
					<img class="fill-it" src="<?php echo $actualimage; ?>" alt="<?php echo $image_alt; ?>">
					<h2 class="spaces targetedscrollpoint" id="causes-scroll-to">Patients & Visitors</h2>
					
					<div class="toggle-area">
						<h4><?php echo get_field('patients_&_visitors_subheader'); ?></h4>
						<p><?php echo get_field('patients_&_visitors_summary_content'); ?></p>

						<!-- <h5>Doctors</h5> -->
						<?php 
									// $currentid = $post->ID;
									// $allofit = get_the_category( $post->ID );
									// $pagecatid = ($allofit[0]->term_id);
									// // echo $pagecatid;

									// $term_id = 21;
									// $taxonomy_name = 'category';
									// $termchildren = get_term_children($term_id, $taxonomy_name);
									 
									// // get each location
									// foreach ( $termchildren as $child ) {

									// 	$term = get_term_by( 'id', $child, $taxonomy_name );
									// 	// printer($term);

									// 	if ($term->term_id == $pagecatid) {
										
									// 		// echo '<h4>'.$term->name.'</h4>';

									// 		global $post;

									// 		$runme = get_posts(array( 
									// 		    'post_type' => 'nas-locations',
									// 		    'showposts' => -1,
									// 		    'tax_query' => array(
									// 		        array(
									// 		            'taxonomy' => 'category',
									// 		            'terms' => $child,
									// 		            'field' => 'term_id',
									// 		        )
									// 		    )

									// 		));

									// 		foreach ($runme as $post) {
										
									// 			setup_postdata($post);

									// 			$nextlevelsearchid = get_the_ID();

									// 			if ($nextlevelsearchid == $currentid) {
												
									// 				// get each doctor by location
									// 				// get each doctor by location
									// 				$runnext = get_posts(array( 
									// 				    'post_type' => 'nas-doctors',
									// 				    'showposts' => -1,
									// 				));

									// 				echo "<ul>";
									// 				foreach ($runnext as $post) {
											
									// 					setup_postdata($post);

									// 					if( have_rows('practice_locations') ): 
									// 						while( have_rows('practice_locations') ): the_row(); 
									// 							$placesworked =  get_sub_field('practice_location');

									// 							if ($placesworked == $nextlevelsearchid) {
									// 								echo '<li><a href="'.esc_url( get_permalink()).'">' . get_the_title() . '</a></li>';
									// 							}						

									// 						endwhile;
									// 					endif;
														
									// 					wp_reset_postdata();
									// 				}
									// 				echo '</ul>';

									// 				// end get each doctor by location

									// 				wp_reset_postdata();
									// 			}
									// 		}

									// 		wp_reset_postdata();
									// 	}
									// }

								?>

						<?php if( have_rows('patients_&_visitors_blocks') ): ?>
							<?php while( have_rows('patients_&_visitors_blocks') ): the_row(); ?>
								<h5><?php echo get_sub_field('title'); ?></h5>
								<p><?php echo get_sub_field('content'); ?></p>
								<?php if (get_sub_field('text_url')): ?>
									<a href="<?php echo get_sub_field('text_url'); ?>" class="btn-orange invert"><div></div><?php echo get_sub_field('link_text'); ?></a>
								<?php endif ?>
							<?php endwhile ?>
						<?php endif	?>
					</div>
				</div>

			</div>

		</section>

		<section class="each-block-all info testimony-info-block" id="back-pain-1">

			<div class="left-50">
				<div class="each-block-lock targetedscrollpoint" id="testimony-scroll-to">
					<h2><?php the_title(); ?> Testimonial</h2>

					<?php 

						$post_object = get_field('featured_testimonial');
						$post = $post_object;
						setup_postdata($post); 
						
					?>

					<div class="patient-data">

						<?php 

							$thumb_id = get_post_thumbnail_id();
							$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
							$thumb_url = $thumb_url_array[0];
							$image_alt = get_post_meta( $thumb_id, '_wp_attachment_image_alt', true);
						?>
						
						<?php if ($thumb_url_array[1] != 48): ?>
							<img src="<?php echo $thumb_url; ?>" alt="<?php echo $image_alt; ?>">
						<?php endif ?>


						<div class="patient-info">
							<h4><?php echo get_field('testimonial_block_patient_name', $post->ID); ?></h4>
							
							<?php 
								$cond = get_field('testimonial_associated_condition', $post->ID);
								$proc = get_field('testimonial_page_link', $post->ID);
							?>
							<p>Condition: <a href="<?php the_permalink( $cond->ID ); ?>"><?php echo $cond->post_title; ?></a></p>
							<p>Procedure: <a href="<?php the_permalink( $proc->ID ); ?>"><?php echo $proc->post_title; ?></a></p>
							<?php if (get_field('testimonial_block_hometown', $post->ID) != ''): ?>
								<p>Hometown: <span><?php echo get_field('testimonial_block_hometown', $post->ID) ?></span></p>
							<?php endif ?>
						</div>
						
					</div>
					<h6 class="quote">“<?php echo get_field('testimonial_block_quote', $post->ID); ?>”</h6>
					<h4 class="lifestyle"><?php echo get_field('testimonial_block_subheader', $post->ID); ?></h4>
					<p><?php echo get_field('testimonial_block_content', $post->ID); ?></p>
					<div class="center-btns">

						<a href="<?php the_permalink( $post->ID ); ?>" class="btn-orange first-space"><div></div>Read Full Story</a>
						<a href="/testimonials" class="btn-orange hidden-md"><div></div>See All Testimonials</a>
					</div>
					<?php wp_reset_postdata(); ?>
				</div>
			</div>	
			
			<!--  -->
			<!-- update links -->
			<!--  -->
			<!--  -->
			<!-- update links -->
			<!--  -->
			<!--  -->
			<!-- update links -->
			<!--  -->
			<!--  -->
			<!-- update links -->
			<!--  -->
			<div class="right-50">
				<div class="each-block-info">
					<h2 class="no-toggle">Meet With a Back Surgery Specialist</h2>
					
					<div class="toggle-area canceled">
						<div id="location-map"></div>
					</div>
				</div>
				

				
			</div>
		</section>

		<div class="nav-dots">
			<ul>
				<li data-target="symptoms-scroll-to"><div class="fade-slide">Plan Your Trip Now</div></li>
				<li data-target="causes-scroll-to"><div class="fade-slide">Patients & Visitors</div></li>
				<li data-target="testimony-scroll-to"><div class="fade-slide">Map</div></li>
			</ul>
		</div>

	</div>


	<!-- modal -->
	<div id="consult" tabindex="-1" role="dialog" aria-labelledby="consult modal">
		<div class="modal-form">
			<div class="close-it"></div>
			<h3>Schedule A Consult</h3>
			<?php pgform_main() ?>
			<!-- <form action="#" _lpchecked="1">
				<input type="text" placeholder="Placeholder text">
				<input type="text" placeholder="Placeholder text">
				<input type="text" placeholder="Placeholder text" class="fullwidth">
				<input type="checkbox" name="check-placeholder-test" id="check-placeholder-test"><label for="check-placeholder-test">Placeholder label</label>
				<input type="submit" value="Schedule Consult" class="orange-grad">
			</form> -->
		</div>
	</div>
	

<?php 		
	// End of the loop.
	endwhile;
?>



	
    <script>


    </script>
   
<?php get_footer(); ?>

