<?php
/**
 * Template Name: Conditions Template Page
 *
 */
?>

<?php get_header(); ?>

<?php 
	// Start the loop.
	while ( have_posts() ) : the_post();
?>

	<div class="bluearea"></div>
	<div class="container" id="conditions">
		<!--<section class="each-block-all" id="pinned-lock-1">
			<div class="left-50">
				<div class="each-block-lock first-lock">
					<h1><?php echo get_field('conditions_title'); ?></h1>
					<h4><?php echo get_field('conditions_overview_subheader'); ?></h4>
					<p><?php echo get_field('conditions_overview_content'); ?></p>
				</div>
			</div>	
			<h2 class="proc-cond-toggle visible-xs visible-sm"><?php echo get_field('supporting_conditions_title'); ?></h2>


		</section>-->

		<?php if( have_rows('each_area') ): ?>
			<?php while( have_rows('each_area') ): the_row(); ?>
				<?php $rowIndex = get_row_index(); ?>
				
				<?php if ($rowIndex > 1 ) : ?>
				<h2 class="proc-cond-toggle visible-xs visible-sm"><?php echo get_sub_field('area_focus_title'); ?></h2>
				<section id="back-pain-<?php echo get_row_index(); ?>" class="each-block-all toggle-area-alt">
				<?php else : ?>
					<section id="pinned-lock-1" class="each-block-all">
				<?php endif; ?>
					<div class="left-50">
						<div class="each-block-lock <?php if ($rowIndex > 1) : ?>lock-it<?php else: ?>first-lock<?php endif; ?>">
							<h1 class="<?php if ($rowIndex > 1) : ?>hidden-xs hidden-sm<?php endif; ?>"><?php echo get_sub_field('area_focus_title'); ?></h1>
							<h4><?php echo get_sub_field('area_focus_sub_header'); ?></h4>
							<p><?php echo get_sub_field('area_focus_content'); ?></p>
							<!-- <ul>Common Symptoms Include:
								<?php // if( have_rows('symptom_list') ): ?>
									<?php // while( have_rows('symptom_list') ): the_row(); ?>
										<li><?php // echo get_sub_field('each_symptom_'); ?></li>
									<?php // endwhile; ?>
								<?php // endif; ?>
							</ul> -->
						</div>
					</div>	
					
					<div class="right-50">
						<div class="each-block-info">
							<?php if (get_sub_field('list_conditions_section_title') != ''): ?>
								<h2 class="no-line no-toggle"><?php echo get_sub_field('list_conditions_section_title'); ?></h2>
							<?php endif ?>
							<!-- <div class="toggle-area"> -->
								<?php if (get_sub_field('list_conditions_section_sub_header') != ''): ?>
									<h4 class="no-line"><?php echo get_sub_field('list_conditions_section_sub_header'); ?></h4>
								<?php endif ?>
								<?php if (get_sub_field('list_conditions_section_content') != ''): ?>
									<p><?php echo get_sub_field('list_conditions_section_content'); ?></p>
								<?php endif ?>
								
								<ul class="no-line">

									<?php 
										$getcat_id = get_sub_field('list_conditions_category')[0];
										// printer($getcat_id);

										
										if ($getcat_id == 17) {

											global $post;

											$runme = get_posts(array( 
											    'post_type' => 'nas-conditions',
											    'showposts' => -1,
											    'tax_query' => array(
											        array(
											            'taxonomy' => 'category',
											            'terms' => array(17, 18, 19, 20),
											            'field' => 'term_id',
											        )
											    ),
										    	'orderby' => 'title',
    											'order'   => 'ASC'

											));


											foreach ($runme as $post) {
										
												setup_postdata($post);

												echo '<li><a href="'.esc_url( get_permalink()).'">' . get_the_title() . '</a></li>';

												wp_reset_postdata();
											}

											wp_reset_postdata();

											// old
											// $args = array('post_type'=>'page', 'cat' => array(17, 18, 19, 20) );
											// $queryit = new WP_Query( $args );

											// if ( $queryit->have_posts() ) {
											// 	while ( $queryit->have_posts() ) {
											// 		$queryit->the_post();
											// 		echo '<li><a href="'.get_page_link().'">' . get_the_title() . '</a></li>';
											// 	}
											// 	wp_reset_postdata();
											// }
										}
										else{

											global $post;

											$runme = get_posts(array( 
											    'post_type' => 'nas-conditions',
											    'showposts' => -1,
											    'tax_query' => array(
											        array(
											            'taxonomy' => 'category',
											            'terms' => $getcat_id,
											            'field' => 'term_id',
											        )
											    ),
										    	'orderby' => 'title',
    											'order'   => 'ASC'

											));

											foreach ($runme as $post) {
										
												setup_postdata($post);

												echo '<li><a href="'.esc_url( get_permalink()).'">' . get_the_title() . '</a></li>';

												wp_reset_postdata();
											}

											wp_reset_postdata();

											// old
											// $args = array('post_type'=>'page', 'cat' => $getcat_id,);
											// $queryit = new WP_Query( $args );

											// if ( $queryit->have_posts() ) {
											// 	while ( $queryit->have_posts() ) {
											// 		$queryit->the_post();
											// 		echo '<li><a href="'.get_page_link().'">' . get_the_title() . '</a></li>';
											// 	}
											// 	wp_reset_postdata();
											// }
										}

									?>
									
								</ul>
							 </div> 
						<!--</div>
			
				<div class="each-block-info">-->
				
				

					</div>
				</section>

			<?php endwhile ?>
		<?php endif ?>
		
		<section class="each-block-all" id="back-pain-1">
			<div class="left-50">
				<div class="each-block-lock lock-it">
					<h1><?php echo get_field('conditions_title'); ?></h1>
					
					<p><?php echo get_field('conditions_overview_content'); ?></p>
				</div>
			</div>	
			<div class="right-50">
			<h2 class="proc-cond-toggle visible-xs visible-sm"><?php echo get_field('supporting_conditions_title'); ?></h2>
					 
					<div class="each-block-info">
					<h2 class="no-toggle no-toggle-alt "><?php echo get_field('supporting_conditions_title'); ?></h2> 
					<div class="no-toggle-area">
						<?php if (get_field('supporting_conditions_subheader') != ''): ?>
							<h4><?php echo get_field('supporting_conditions_subheader'); ?></h4>
						<?php endif ?>

						<?php if (get_field('supporting_conditions_content') != ''): ?>
							<p><?php echo get_field('supporting_conditions_content'); ?></p>
						<?php endif ?>

						<?php if( have_rows('supporting_conditions_images') ): ?>

							<?php while( have_rows('supporting_conditions_images') ): the_row(); 

								$supportingimg = get_sub_field('supporting_conditions_image');
								$actualimage =  wp_get_attachment_url($supportingimg);
								$image_alt = get_post_meta( $supportingimg, '_wp_attachment_image_alt', true);	
							?>
								<img src="<?php echo $actualimage; ?>" alt="<?php echo $image_alt; ?>">

							<?php endwhile; ?>

						<?php endif; ?>
					</div>
					</div>
			</div>
		</section>
		<h2 class="proc-cond-toggle visible-xs visible-sm">Request a Free Info Kit</h2>
		<section class="each-block-all info global-info toggle-area-alt testimony-info-block" id="request-info-block">

			
			<div class="left-50">
				<div class="each-block-lock targetedscrollpoint" id="testimony-scroll-to">
					<h2>Testimonial</h2>

					<?php 

						$post_object = get_field('featured_testimonial');
						$post = $post_object;
						setup_postdata($post); 

					?>

					<div class="patient-data">

						<?php 

							$thumb_id = get_post_thumbnail_id();
							$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
							$thumb_url = $thumb_url_array[0];
							$image_alt = get_post_meta( $thumb_id, '_wp_attachment_image_alt', true);
						?>
						<?php if ($thumb_url_array[1] != 48): ?>
							<img src="<?php echo $thumb_url; ?>" alt="<?php echo $image_alt; ?>">
						<?php endif ?>


						<div class="patient-info">
							<h4><?php echo get_field('testimonial_block_patient_name', $post->ID); ?></h4>
							
							<?php 
								$cond = get_field('testimonial_associated_condition', $post->ID);
								$proc = get_field('testimonial_page_link', $post->ID);
							?>
							<p>Condition: <a href="<?php the_permalink( $cond->ID ); ?>"><?php echo $cond->post_title; ?></a></p>
							<p>Procedure: <a href="<?php the_permalink( $proc->ID ); ?>"><?php echo $proc->post_title; ?></a></p>
							<?php if (get_field('testimonial_block_hometown', $post->ID) != ''): ?>
								<p>Hometown: <span><?php echo get_field('testimonial_block_hometown', $post->ID) ?></span></p>
							<?php endif ?>
						</div>
						
					</div>
					<h6 class="quote">“<?php echo get_field('testimonial_block_quote', $post->ID); ?>”</h6>
					<h4 class="lifestyle"><?php echo get_field('testimonial_block_subheader', $post->ID); ?></h4>
					<p><?php echo get_field('testimonial_block_content', $post->ID); ?></p>
					<div class="center-btns">

						<a href="<?php the_permalink( $post->ID ); ?>" class="btn-orange first-space"><div></div>Read Full Story</a>
						<a href="/testimonials" class="btn-orange hidden-md"><div></div>See All Testimonials</a>
					</div>
					<?php wp_reset_postdata(); ?>
				</div>
				
			</div>
				
			<div class="right-50">
				<div class="request-alt-block targetedscrollpoint updatefix" id="request-scroll-to">
					<h2 class="hidden-xs hidden-sm">Request a Free Info Kit</h2>
					
					<div class="toggle-area">
						<a href="/resource-center/request-free-info-kit/"><img src="../../wp-content/themes/north-american-spine/assets/img/brochureback.png" alt="#"><img src="../../wp-content/themes/north-american-spine/assets/img/brochurefront.png" alt="Request Free Info Kit"></a>
						<a href="/resource-center/request-free-info-kit/" class="btn-orange invert"><div></div>Get Information Kit</a>
					</div>
				</div>
			</div>
		</section>

		<div class="nav-dots">
			<ul>
				<li data-target="pinned-lock-1"><div class="fade-slide">Spine Conditions</div></li>
				<?php if( have_rows('each_area') ): ?>
					<?php while( have_rows('each_area') ): the_row(); ?>
						<li data-target="back-pain-<?php echo get_row_index(); ?>"><div class="fade-slide"><?php echo get_sub_field('area_focus_title'); ?></div></li>
					<?php endwhile ?>
				<?php endif ?>
				<li data-target="request-info-block"><div class="fade-slide">Request Info Kit</div></li>

			</ul>
		</div>

	</div>
<?php 		
	// End of the loop.
	endwhile;
?>


<?php get_footer(); ?>