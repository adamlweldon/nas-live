<?php
/**
 * Template Name: Webinar Template
 *
 */
?>

<?php get_header(); ?>

<?php 
	// Start the loop.
	while ( have_posts() ) : the_post();
?>
	<div class="bluearea"></div>
	<div class="container generic webinar" id="conditions">
		<section class="each-block-all" id="pinned-lock-1">
			<div class="left-50">
				<div class="each-block-lock first-lock alt-lock">
					<div class="generic-content-left">
						<h1><?php the_title(); ?></h1>
						<h4><?php the_content(); ?></h4>

						<!-- call the namespace \ class -->
						<?php pgform_webinar(); ?>
			
					</div>
				</div>
			</div>	
			
			<div class="right-50">
				<div class="each-block-info">
					
					<h2><?php echo the_field('webinar_block_title'); ?></h2>

					<div class="toggle-area">
						<div class="video">
							<div class="video-container">
								<div id="yt-player"></div>
								<div class="hover-video">
									<ul>
										<li>
											<i class="fa fa-pause pause-vid" aria-hidden="true"></i>
										</li>
										<li>
											<i class="fa fa-play play-vid" aria-hidden="true"></i>
										</li>
										<li>
											<i class="fa fa-repeat repeat-vid" aria-hidden="true"></i>
										</li>
									</ul>
								</div>
								
							</div>
						</div>

						<ul class="id-badge">
							<?php echo the_field('webinar_block_sub_header'); ?>
							<li><?php echo the_field('webinar_block_list_title'); ?></li>

							<?php if( have_rows('webinar_block_list') ): ?>

								<?php while( have_rows('webinar_block_list') ): the_row(); ?>
									<li><?php echo get_sub_field('webinar_list_item'); ?> </li>
							
								<?php endwhile ?>
							<?php endif; ?>
						</ul>
					</div>

				</div>
				
			</div>

		</section>

		<section class="each-block-all info testimony-info-block" id="back-pain-1">

			<div class="left-50">
				<div class="each-block-lock targetedscrollpoint" id="testimony-scroll-to">
					<h2><?php the_title(); ?> Testimonial</h2>

					<?php 

						$post_object = get_field('featured_testimonial');
						$post = $post_object;
						setup_postdata($post); 

					?>

					<div class="patient-data">

						<?php 

							$thumb_id = get_post_thumbnail_id();
							$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
							$thumb_url = $thumb_url_array[0];
							$image_alt = get_post_meta( $thumb_id, '_wp_attachment_image_alt', true);
						?>
						<?php if ($thumb_url_array[1] != 48): ?>
							<img src="<?php echo $thumb_url; ?>" alt="<?php echo $image_alt; ?>">
						<?php endif ?>


						<div class="patient-info">
							<h4><?php echo get_field('testimonial_block_patient_name', $post->ID); ?></h4>
							
							<?php 
								$cond = get_field('testimonial_associated_condition', $post->ID);
								$proc = get_field('testimonial_page_link', $post->ID);
							?>
							<p>Condition: <a href="<?php the_permalink( $cond->ID ); ?>"><?php echo $cond->post_title; ?></a></p>
							<p>Procedure: <a href="<?php the_permalink( $proc->ID ); ?>"><?php echo $proc->post_title; ?></a></p>
							<?php if (get_field('testimonial_block_hometown', $post->ID) != ''): ?>
								<p>Hometown: <span><?php echo get_field('testimonial_block_hometown', $post->ID) ?></span></p>
							<?php endif ?>
						</div>
						
					</div>
					<h6 class="quote">“<?php echo get_field('testimonial_block_quote', $post->ID); ?>”</h6>
					<h4 class="lifestyle"><?php echo get_field('testimonial_block_subheader', $post->ID); ?></h4>
					<p><?php echo get_field('testimonial_block_content', $post->ID); ?></p>
					<div class="center-btns">

						<a href="<?php the_permalink( $post->ID ); ?>" class="btn-orange first-space"><div></div>Read Full Story</a>
						<a href="/testimonials" class="btn-orange hidden-md"><div></div>See All Testimonials</a>
					</div>
					<?php wp_reset_postdata(); ?>
				</div>

			</div>	
						
			<!--  -->
			<!-- update links -->
			<!--  -->
			<!--  -->
			<!-- update links -->
			<!--  -->
			<!--  -->
			<!-- update links -->
			<!--  -->
			<!--  -->
			<!-- update links -->
			<!--  -->

			<div class="right-50">
				<div class="each-block-info">
					<h2><?php echo get_field('webinar_candidate_block_title'); ?></h2>
					
					<div class="toggle-area">
						<h4><?php echo get_field('webinar_candidate_block_subheader'); ?></h4>
						<ul class="causesof">Conditions Treated
							<?php if( have_rows('conditions_treated_list') ): ?>

								<?php while( have_rows('conditions_treated_list') ): the_row(); ?>
									<li><?php echo get_sub_field('each_condition_treated_list_item'); ?> </li>
							
								<?php endwhile ?>
							<?php endif; ?>
						</ul>
						<ul class="heart">Common symptoms
							<?php if( have_rows('common_symptoms_list') ): ?>

								<?php while( have_rows('common_symptoms_list') ): the_row(); ?>
									<li><?php echo get_sub_field('each_common_symptom_list_item'); ?> </li>
							
								<?php endwhile ?>
							<?php endif; ?>
						</ul>
						<ul class="cross">Free Second Opinion
							<?php if( have_rows('second_opinion_list') ): ?>

								<?php while( have_rows('second_opinion_list') ): the_row(); ?>
									<li><?php echo get_sub_field('each_opinion_list_item'); ?> </li>
							
								<?php endwhile ?>
							<?php endif; ?>
						</ul>
						<a href="#" class="btn-orange invert" data-target="#webinar-modal"><div></div><?php echo get_field('webinar_link_text'); ?></a>
					</div>
				</div>
				
			</div>
		</section>
			
		<div class="nav-dots">
			<ul>
				<li data-target="pinned-lock-1"><div class="fade-slide">Webinar Preview</div></li>
				<li data-target="back-pain-1"><div class="fade-slide">Am I A Candidate?</div></li>

			</ul>
		</div>

	</div>
<?php 		
	// End of the loop.
	endwhile;
?>

<?php get_footer(); ?>

