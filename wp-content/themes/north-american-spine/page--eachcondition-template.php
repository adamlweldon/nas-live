<?php
/**
 * Template Name: Each Condition Template Page
 *
 */
?>

<?php get_header(); ?>

<?php 
	// Start the loop.
	while ( have_posts() ) : the_post();
?>

	<div class="container alt-conditions" id="conditions">
		<section class="each-block-all" id="pinned-lock-1">
			<div class="left-50">
				<div class="each-block-lock first-lock alt-lock">
					<div class="condition-content-left">
					<h1>What is <?php the_title(); ?>?</h1>
					<h4><?php echo get_field('each_condition_subheader'); ?></h4>
					<p><?php echo get_field('each_condition_what_is_content'); ?></p>
					</div>
					<div class="right-img">
					
					<?php 
						$supportingimg = get_field('each_condition_what_is_image');
						$actualimage =  wp_get_attachment_url($supportingimg);
						$image_alt = get_post_meta( $supportingimg, '_wp_attachment_image_alt', true);	
					?>

						<img src="<?php echo $actualimage; ?>" alt="<?php echo $image_alt; ?>">
					</div>
				</div>
			</div>	
			
			<div class="right-50">
				<div class="each-block-info targetedscrollpoint" id="symptoms-scroll-to">
					<h2>Symptoms</h2>
					
					<div class="toggle-area">
						<h4><?php echo get_field('each_condition_symptoms_subheader'); ?></h4>
						<ul>Common symptoms of <?php the_title(); ?> include:
							
							<?php if( have_rows('each_condition_symptoms_list') ): ?>
								<?php while( have_rows('each_condition_symptoms_list') ): the_row(); ?>
									<li><?php echo get_sub_field('each_condition_symptom'); ?></li>
								<?php endwhile ?>
							<?php endif ?>
						</ul>
						<a href="#" class="btn-orange invert"><div></div>Explore Treatment Options</a>
					</div>

					<h2 class="spaces targetedscrollpoint" id="causes-scroll-to">Causes of <?php the_title(); ?></h2>
					
					<div class="toggle-area">
						<h4><?php echo get_field('each_condition_causes_subheader'); ?></h4>
						<ul class="causesof">Additional common <?php the_title(); ?> causes include:
							<?php if( have_rows('each_condition_causes_list') ): ?>
								<?php while( have_rows('each_condition_causes_list') ): the_row(); ?>
									<li><?php echo get_sub_field('each_condition_cause'); ?></li>
								<?php endwhile ?>
							<?php endif ?>
						</ul>
						<p><?php echo get_field('each_condition_causes_content'); ?></p>
						<a href="#" class="btn-orange invert"><div></div>Learn About Our Procedures</a>
					</div>

					<div class="request-alt-block targetedscrollpoint" id="request-scroll-to">
						<h2>Request a Free Info Kit</h2>
						
						<div class="toggle-area">
							<a href="#"><img src="../../wp-content/themes/north-american-spine/assets/img/brochureback.png" alt="#"><img src="../../wp-content/themes/north-american-spine/assets/img/brochurefront.png" alt="#"></a>
							<a href="#" class="btn-orange invert"><div></div>Get Information Kit</a>
						</div>
					</div>

				</div>
				
			</div>

		</section>

		<section class="each-block-all info testimony-info-block">

			<?php if (get_field('testimonial_block_title') != ''): ?>
			<div class="left-50">
				<div class="each-block-lock targetedscrollpoint" id="testimony-scroll-to">
					<h2><?php echo get_field('testimonial_block_title'); ?></h2>
					<div class="patient-data">

						<?php 
							$testimg = get_field('testimonial_block_image');
							$actualtestimage =  wp_get_attachment_url($testimg);
							$image_test_alt = get_post_meta($testimg, '_wp_attachment_image_alt', true);	
						?>
						
						<?php if ($actualtestimage != ''): ?>
							<img src="<?php echo $actualtestimage; ?>" alt="<?php echo $image_test_alt; ?>">
						<?php endif ?>

						<div class="patient-info">
							<h4><?php echo get_field('testimonial_block_patient_name'); ?></h4>
							
							<?php 
								$getesttitle = url_to_postid(get_field('testimonial_page_link'));
								$getesttitle =	get_the_title($getesttitle);
							?>

							<p>Procedure: <a href="<?php echo get_field('testimonial_page_link'); ?>"><?php echo $getesttitle; ?></a></p>
							<p>Hometown: <span><?php echo get_field('testimonial_block_hometown'); ?></span></p>
						</div>
						
					</div>
					<h6 class="quote">“<?php echo get_field('testimonial_block_quote'); ?>”</h6>
					<h4 class="lifestyle"><?php echo get_field('testimonial_block_subheader'); ?></h4>
					<p><?php echo get_field('testimonial_block_content'); ?></p>
					<div class="center-btns">
						<a href="<?php echo get_field('testimonial_block_link_url'); ?>" class="btn-orange first-space"><div></div><?php echo get_field('testimonial_block_link_text'); ?></a>
						<a href="/testimonials" class="btn-orange hidden-md"><div></div>See All Testimonials</a>
					</div>
					
				</div>
			</div>	
			<?php else: ?>
			<div class="left-50">
				
			</div>
			<?php endif ?>
			
			<!--  -->
			<!-- update links -->
			<!--  -->
			<!--  -->
			<!-- update links -->
			<!--  -->
			<!--  -->
			<!-- update links -->
			<!--  -->
			<!--  -->
			<!-- update links -->
			<!--  -->

			<div class="right-50">
				<div class="each-block-info">
					<h2>Connect With A Patient</h2>
					
					<div class="toggle-area">
						<p>To learn more about the treatment process, connect with one of our satisfied patients. It may give you a better idea of treatment plans and reduce stress. Each of our available patients have agreed to answer any questions about the process, equipment used and overall experience. </p>
						<a href="#" class="btn-orange invert"><div></div>Get In Touch</a>
					</div>
				</div>
				
			</div>
		</section>

		<div class="nav-dots">
			<ul>
				<li data-target="symptoms-scroll-to"><div class="fade-slide">Symptoms <?php echo the_title(); ?></div></li>
				<li data-target="causes-scroll-to"><div class="fade-slide">Causes of <?php echo the_title(); ?></div></li>
				<li data-target="request-scroll-to"><div class="fade-slide">Request Info</div></li>
				<li data-target="testimony-scroll-to"><div class="fade-slide">Patient Testimonial</div></li>
			</ul>
		</div>

	</div>
<?php 		
	// End of the loop.
	endwhile;
?>

<?php get_footer(); ?>

