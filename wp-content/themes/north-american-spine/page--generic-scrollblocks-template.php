<?php
/**
 * Template Name: Generic Scrollblock Template
 *
 */
?>

<?php get_header(); ?>

<?php 
	// Start the loop.
	while ( have_posts() ) : the_post();
?>
	<div class="bluearea"></div>
	<div class="container generic" id="conditions">
		<section class="each-block-all" id="pinned-lock-1">
			<div class="left-50">
				<div class="each-block-lock first-lock alt-lock">
					<div class="generic-content-left">
					<h1><?php the_title(); ?></h1>
					<p><?php the_content(); ?></p>
					</div>
				</div>
			</div>	
			
			<div class="right-50">
				<div class="each-block-info">

					<!-- TURN ON OR OFF FOR DOCTORS -->
					<?php 
						$host = $_SERVER['REQUEST_URI'];
						if($host == '/resource-center/partered-physicians/' || $host=='/resource-center/partered-physicians'){
					?>

							<h2>Our Doctors</h2>

							<div class="toggle-area doctors-list">
								<?php 

									$term_id = 21;
									$taxonomy_name = 'category';
									$termchildren = get_term_children($term_id, $taxonomy_name);
									 
									// get each location
									foreach ( $termchildren as $child ) {

										$term = get_term_by( 'id', $child, $taxonomy_name );
										echo '<h4>'.$term->name.'</h4>';

										global $post;

										$runme = get_posts(array( 
										    'post_type' => 'nas-locations',
										    'showposts' => -1,
										    'tax_query' => array(
										        array(
										            'taxonomy' => 'category',
										            'terms' => $child,
										            'field' => 'term_id',
										        )
										    )

										));

										echo '<ul class="first-level">';

										foreach ($runme as $post) {
									
											setup_postdata($post);

											$nextlevelsearchid = get_the_ID();

											// echo '<li><a href="'.esc_url( get_permalink()).'">' . get_the_title() . '</a></li>';

											
												// get each doctor by location
												// get each doctor by location
									
												// printer($nextlevelsearchid);
												$runnext = get_posts(array( 
												    'post_type' => 'nas-doctors',
												    'showposts' => -1,

												));

												echo "<ul>";
												foreach ($runnext as $post) {
										
													setup_postdata($post);

													if( have_rows('practice_locations') ): 
														while( have_rows('practice_locations') ): the_row(); 
															$placesworked =  get_sub_field('practice_location');

															if ($placesworked == $nextlevelsearchid) {
																echo '<li><a href="'.esc_url( get_permalink()).'">' . get_the_title() . '</a></li>';
															}						

														endwhile;
													endif;
													
													wp_reset_postdata();
												}
												echo '</ul>';

											// end get each doctor by location

											wp_reset_postdata();
										}

										echo '</ul>';

										wp_reset_postdata();
									}

								?>

							</div>

					<?php	  	
						}
					?>

					<!-- END TURN ON OR OFF -->
					<!-- END TURN ON OR OFF -->
					<!-- END TURN ON OR OFF -->


					<?php if( have_rows('each_content_block') ): ?>
						<?php while( have_rows('each_content_block') ): the_row(); ?>
							<?php 
							$ogBlockTitle = get_sub_field('each_content_block_title');
							$regEx = '/([0-9]\.|Q\:)/';
							preg_match($regEx, $ogBlockTitle, $m );

							if (isset($m[1])) : 
								 $blockTitle = preg_replace($regEx,'<span class="title-entry">' . $m[1] . '</span>', $ogBlockTitle); 
							else: 
								$blockTitle = $ogBlockTitle; 
							endif; 
							?>
							<h2 class="spaces targetedscrollpoint" id="generic-sectionscroll-<?php echo get_row_index(); ?>"><?php echo $blockTitle; ?></h2>
							
							<div class="toggle-area">
								<!-- if subheader exists -->
								<?php if (get_sub_field('each_content_block_subheader') != ''): ?>
									<h4><?php echo get_sub_field('each_content_block_subheader'); ?></h4>
								<?php endif ?>
							
								<!-- if content exists -->
								<?php if (get_sub_field('each_content_block_content') != ''): ?>
									<p><?php echo get_sub_field('each_content_block_content'); ?></p>
								<?php endif ?>

								<!-- if button exists -->
								<?php if (get_sub_field('each_content_block_link_text') != ''): ?>	
									<a href="<?php echo get_sub_field('each_content_block_link_url'); ?>" class="btn-orange invert"><div></div><?php echo get_sub_field('each_content_block_link_text'); ?></a>
								<?php endif ?>
							</div>
							
						<?php endwhile ?>
					<?php endif ?>	

				</div>
				
			</div>

		</section>

		<?php if (!is_page('spine-pain-doctors')): ?>
			
			<div class="nav-dots">
				<ul>
					<?php if( have_rows('each_content_block') ): ?>
						<?php while( have_rows('each_content_block') ): the_row(); ?>
							<li data-target="generic-sectionscroll-<?php echo get_row_index(); ?>"><div class="fade-slide"><?php echo get_sub_field('each_content_block_title'); ?></div></li>
						<?php endwhile ?>
					<?php endif ?>	

				</ul>
			</div>
		<?php endif ?>

	</div>
<?php 		
	// End of the loop.
	endwhile;
?>

<?php get_footer(); ?>

