<?php
/*
Plugin Name: NAS Blog Search Widget
Description: Adds a Widget for searching blog posts.
*/

add_action( 'admin_init', 'nas_blog_search_widget_require' );
function nas_blog_search_widget_require() {
    if ( is_admin() && current_user_can( 'activate_plugins' ) &&  !is_plugin_active( 'nas-support-functions/nas-support-functions.php' ) ) {
        add_action( 'admin_notices', 'nas_blog_search_widget_error' );

        deactivate_plugins( plugin_basename( __FILE__ ) ); 

        if ( isset( $_GET['activate'] ) ) {
            unset( $_GET['activate'] );
        }
    }
}

function nas_blog_search_widget_error(){
    ?><div class="error"><p>Sorry, but NAS Related Post Widget requires the NAS Support Functions plugin to be installed and active.</p></div><?php
}

require_once( ABSPATH . 'wp-content/plugins/nas-support-functions/nas-support-functions.php' );

define( 'NAS_BLOG_SEARCH_WIDGET_DIR', plugin_dir_path( __FILE__ ) );

require_once( NAS_BLOG_SEARCH_WIDGET_DIR . 'class.nas-blog-search-widget.php' );