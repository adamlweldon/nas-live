<?php

// Creating the widget 
class nbs_widget extends WP_Widget {

    function __construct() {
        parent::__construct(
            // Base ID of the widget
            'nbs_widget', 

            // Widget name will appear in UI
            __('Blog Search Widget', 'nas_bs_widget'), 

            // Widget description
            array( 'description' => __( 'Displays a search field for searching blog posts.', 'nas_bs_widget' ), ) 
        );
    }

    // Creating widget front-end
    // This is where the action happens
    public function widget( $args, $instance ) {
        $title = apply_filters( 'widget_title', $instance['title'] );
        // before and after widget arguments are defined by themes
        echo $args['before_widget'];
        // if ( ! empty( $title ) )
            // echo $args['before_title'] . $title . $args['after_title'];



        ?>
            <form role="search" method="post" id="searchform" class="searchform" action="/">
                <label class="screen-reader-text h4sr" for="s">Search for:</label>
                <input type="hidden" value="post" name="post_type" />
                <input type="text" value="" name="s" id="s" placeholder="Search Blog" /><input type="submit" id="searchsubmit" value="&#xf002;" />
            </form>

        <?php
        echo $args['after_widget'];
    }
		
    // Widget Backend 
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        } else {
            $title = __( 'Blog Search', 'nas_bs_widget' );
        }

        // Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <?php 
    }
    	
    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }
} // Class nbs_widget ends here

// Register and load the widget
function nbs_load_widget() {
	register_widget( 'nbs_widget' );
}
add_action( 'widgets_init', 'nbs_load_widget' );