<?php
/*
Plugin Name: ROI Revolution Web Analytics
Plugin URI: http://www.roirevolution.com/wordpress
Description: Automatically integrates ROI Revolution tracking with WordPress and Gravity Forms sitewide.
Version: 1.1.0
Author: ROI Revolution, Inc.
Author URI: http://www.roirevolution.com/
*/

/*
Copyright 2014 ROI Revolution, Inc.

   For use only by active clients of ROI Revolution, Inc. on approved web
   properties. Modification, redistribution, and/or attempted reverse engineering
   of this script is strictly forbidden and may be subject to legal action.
*/

add_action('init',  array('ROI_Revolution', 'init'));
add_action('admin_init',  array('ROI_Revolution', 'admin_init'));

class ROI_Revolution {
    private static $min_gravityforms_version = "1.6.0";

    public static function init() {
        add_action('wp_head', array("ROI_Revolution", "print_gate_script"), 1);
		add_action('mobile_head', array("ROI_Revolution", "print_gate_script"), 1); // naspine.com mobile site

		if ( self::is_gravityforms_supported() ) {
			add_filter("gform_confirmation", array("ROI_Revolution", "track_gate_goal"), 10, 4);
			add_action("gform_pre_submission", array("ROI_Revolution", "modify_crm_lead"));
        }
    }

	public static function admin_init() {
		if ( self::is_gravityforms_supported() ) {
			add_action("gform_after_save_form", array("ROI_Revolution", "create_crm_fields"), 10, 2);
			add_action("gform_before_delete_field", array("ROI_Revolution", "log_crm_deleted"), 10, 2);
		}
	}

	public static function print_gate_script() {
		print self::get_gate_script();
	}

	public static function track_gate_goal($confirmation, $form, $lead, $is_ajax) {
		$goal = http_build_query(array("thanks" => $form["title"]));
		$goal_script = "<script type='text/javascript'>ga('send', 'pageview', '/virtual/?{$goal}'); ga(function(){try{ROITracker.trackWebLead();}catch(e){}});</script>\n";

		if ( is_array($confirmation) ) {
			$confirmation["redirect"] = http_build_url($confirmation["redirect"], array("query" => $goal), HTTP_URL_JOIN_QUERY);
		} elseif( preg_match("/gformRedirect\(\)/", $confirmation) ) {
			$confirmation = $goal_script . preg_replace("/function gformRedirect\(\)\{document\.location\.href='([^']*)';\}/", "function gformRedirect(){setTimeout(function(){document.location.href='$1';},300);}", $confirmation);
		} else {
			$confirmation .= self::get_gate_script() . $goal_script;
		}

		return $confirmation;
	}

	public static function create_crm_fields($form, $is_new) {
		$form["fields"] = is_array($form["fields"]) ? $form["fields"] : array();
		$visitor_id = self::get_crm_field_id($form["fields"], "ROI Visitor");
		$attribution_id = self::get_crm_field_id($form["fields"], "ROI Attribution");

		if ( $is_new && ( is_null($visitor_id) && is_null($attribution_id) ) ) {
			$form["fields"][] = self::new_crm_field($form["fields"], "ROI Visitor");
			$form["fields"][] = self::new_crm_field($form["fields"], "ROI Attribution");

			GFFormDetail::save_form_info($form["id"], GFCommon::json_encode($form));
		}
	}

	public static function log_crm_deleted($form_id, $field_id) {
		$user = wp_get_current_user();
		$domain = self::get_current_domain();
		$form = RGFormsModel::get_form_meta($form_id);
		$field = RGFormsModel::get_field($form, $field_id);

		if ( stristr($field["label"], "ROI Visitor") || stristr($field["label"], "ROI Attribution") ) {
			$subject = "NAS Warning - {$field["label"]} Field Deleted on {$domain}";
			$body = current_time("mysql", 1) . " - The {$field["label"]} field was deleted by {$user->user_login} on " . get_site_url() . ". Form {$form_id}: {$form["title"]}.";
			wp_mail('charlie@roirevolution.com', $subject, $body);
		}
	}

	public static function modify_crm_lead($form) {
		$visitor_id = self::get_crm_field_id($form["fields"], "ROI Visitor");
		$attribution_id = self::get_crm_field_id($form["fields"], "ROI Attribution");

		if ( $visitor_id ) {
			$_POST["input_{$visitor_id}"] = isset($_COOKIE["__roia"]) ? $_COOKIE["__roia"] : "";
		}

		if ( $attribution_id ) {
			$_POST["input_{$attribution_id}"] = isset($_COOKIE["__roiz"]) ? $_COOKIE["__roiz"] : "";
		}
	}

	private static function get_crm_field_id($fields, $label) {
		if ( is_array($fields) ) {
			foreach ( $fields as $field ) {
				if ( stristr($field["label"], $label) ) {
					return $field["id"];
				}
			}
		}
		return NULL;
	}

	private static function get_current_domain() {
		$site_info = parse_url( get_site_url() );
		$host = $site_info['host'];
		$host_names = explode(".", $host);
		return ( $host_names[count($host_names)-2] . "." . $host_names[count($host_names)-1] );
	}

	private static function get_gate_script() {
    return <<< END_CODE
    <script type="text/javascript">
    if(typeof ROITracker==='undefined'){window.ROIStorage={};ROIStorage.q=[];window.ga=function(){ROIStorage.q.push(arguments)};var ga=window.ga;window.ga.q=window.ga.q||[];ROIStorage.roiq=[];ROIStorage.analyticsJsNotLoaded=true;window.ga.q.push([function(){var e;ROIStorage.realGa=window.ga;ROIStorage.analyticsJsNotLoaded=false;window.ga=function(){if(typeof arguments[0]==='function'){ROIStorage.realGa(arguments)}else{ROIStorage.q.push(arguments)}};ROIStorage.roiq.push=function(){ROIStorage.realGa.apply(window,arguments)};for(e=0;e<ROIStorage.roiq.length;e+=1){ROIStorage.realGa.call(window,ROIStorage.roiq[e])}}])}ROIStorage.gaq=ROIStorage.gaq||[];window._gaq={push:function(){var e;for(e=0;e<arguments.length;e++){ROIStorage.gaq.push(arguments[e])}}};var _gaq=window._gaq;
    (function () {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://c772931.ssl.cf2.rackcdn.com/gate.js' : 'http://c772931.r62.cf2.rackcdn.com/gate.js');
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();
    </script>
END_CODE;
}

    private static function is_gravityforms_supported() {
        if ( class_exists("GFCommon") ) {
			$is_correct_version = version_compare(GFCommon::$version, self::$min_gravityforms_version, ">=");
			return $is_correct_version;
        }
        return false;
    }

	private static function new_crm_field($fields, $label) {
		$field = array();

		$max_id = 1;
		foreach( $fields as $field ) {
			$max_id = ( $field["id"] > $max_id ) ? $field["id"] : $max_id;
		}

		$field["id"] = ++$max_id;
		$field["label"] = $label;
		$field["adminLabel"] = "";
		$field["type"] = "hidden";
		$field["isRequired"] = false;
		$field["size"] = "medium";
		$field["errorMessage"] = "";
		$field["inputs"] = null;
		$field["calculationFormula"] = "";
		$field["calculationRounding"] = "";
		$field["enableCalculation"] = "";
		$field["disableQuantity"] = false;
		$field["displayAllCategories"] = false;
		$field["inputMask"] = false;
		$field["inputMaskValue"] = "";
		$field["allowsPrepopulate"] = false;

		return $field;
	}
}

if (!function_exists('http_build_url'))
{
    define('HTTP_URL_REPLACE', 1);              // Replace every part of the first URL when there's one of the second URL
    define('HTTP_URL_JOIN_PATH', 2);            // Join relative paths
    define('HTTP_URL_JOIN_QUERY', 4);           // Join query strings
    define('HTTP_URL_STRIP_USER', 8);           // Strip any user authentication information
    define('HTTP_URL_STRIP_PASS', 16);          // Strip any password authentication information
    define('HTTP_URL_STRIP_AUTH', 32);          // Strip any authentication information
    define('HTTP_URL_STRIP_PORT', 64);          // Strip explicit port numbers
    define('HTTP_URL_STRIP_PATH', 128);         // Strip complete path
    define('HTTP_URL_STRIP_QUERY', 256);        // Strip query string
    define('HTTP_URL_STRIP_FRAGMENT', 512);     // Strip any fragments (#identifier)
    define('HTTP_URL_STRIP_ALL', 1024);         // Strip anything but scheme and host

    // Build an URL
    // The parts of the second URL will be merged into the first according to the flags argument.
    //
    // @param   mixed           (Part(s) of) an URL in form of a string or associative array like parse_url() returns
    // @param   mixed           Same as the first argument
    // @param   int             A bitmask of binary or'ed HTTP_URL constants (Optional)HTTP_URL_REPLACE is the default
    // @param   array           If set, it will be filled with the parts of the composed url like parse_url() would return
    function http_build_url($url, $parts=array(), $flags=HTTP_URL_REPLACE, &$new_url=false)
    {
        $keys = array('user','pass','port','path','query','fragment');

        // HTTP_URL_STRIP_ALL becomes all the HTTP_URL_STRIP_Xs
        if ($flags & HTTP_URL_STRIP_ALL)
        {
            $flags |= HTTP_URL_STRIP_USER;
            $flags |= HTTP_URL_STRIP_PASS;
            $flags |= HTTP_URL_STRIP_PORT;
            $flags |= HTTP_URL_STRIP_PATH;
            $flags |= HTTP_URL_STRIP_QUERY;
            $flags |= HTTP_URL_STRIP_FRAGMENT;
        }
        // HTTP_URL_STRIP_AUTH becomes HTTP_URL_STRIP_USER and HTTP_URL_STRIP_PASS
        else if ($flags & HTTP_URL_STRIP_AUTH)
        {
            $flags |= HTTP_URL_STRIP_USER;
            $flags |= HTTP_URL_STRIP_PASS;
        }

        // Parse the original URL
        $parse_url = parse_url($url);

        // Scheme and Host are always replaced
        if (isset($parts['scheme']))
            $parse_url['scheme'] = $parts['scheme'];
        if (isset($parts['host']))
            $parse_url['host'] = $parts['host'];

        // (If applicable) Replace the original URL with it's new parts
        if ($flags & HTTP_URL_REPLACE)
        {
            foreach ($keys as $key)
            {
                if (isset($parts[$key]))
                    $parse_url[$key] = $parts[$key];
            }
        }
        else
        {
            // Join the original URL path with the new path
            if (isset($parts['path']) && ($flags & HTTP_URL_JOIN_PATH))
            {
                if (isset($parse_url['path']))
                    $parse_url['path'] = rtrim(str_replace(basename($parse_url['path']), '', $parse_url['path']), '/') . '/' . ltrim($parts['path'], '/');
                else
                    $parse_url['path'] = $parts['path'];
            }

            // Join the original query string with the new query string
            if (isset($parts['query']) && ($flags & HTTP_URL_JOIN_QUERY))
            {
                if (isset($parse_url['query']))
                    $parse_url['query'] .= '&' . $parts['query'];
                else
                    $parse_url['query'] = $parts['query'];
            }
        }

        // Strips all the applicable sections of the URL
        // Note: Scheme and Host are never stripped
        foreach ($keys as $key)
        {
            if ($flags & (int)constant('HTTP_URL_STRIP_' . strtoupper($key)))
                unset($parse_url[$key]);
        }


        $new_url = $parse_url;

        return
             ((isset($parse_url['scheme'])) ? $parse_url['scheme'] . '://' : '')
            .((isset($parse_url['user'])) ? $parse_url['user'] . ((isset($parse_url['pass'])) ? ':' . $parse_url['pass'] : '') .'@' : '')
            .((isset($parse_url['host'])) ? $parse_url['host'] : '')
            .((isset($parse_url['port'])) ? ':' . $parse_url['port'] : '')
            .((isset($parse_url['path'])) ? $parse_url['path'] : '')
            .((isset($parse_url['query'])) ? '?' . $parse_url['query'] : '')
            .((isset($parse_url['fragment'])) ? '#' . $parse_url['fragment'] : '')
        ;
    }
}