<?php
/*
Plugin Name: Ajax Post Loader
Description: Loads next page of posts via ajax
*/
define( 'ZOG_AJAX_LOADER_URL', plugin_dir_url( __FILE__ ) );


function zog_ajax_loader_scripts() {
    // $categoryBase = preg_replace("/[^A-Za-z0-9 -]/", '', get_option('category_base'));
    wp_enqueue_script( 'zog_ajax_loader_script', ZOG_AJAX_LOADER_URL . 'js/scripts.min.js', array('jquery'), '1.0' );
    wp_localize_script( 'zog_ajax_loader_script', 'ajax_object',
            array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
}
add_action( 'wp_enqueue_scripts', 'zog_ajax_loader_scripts' );


add_action( 'wp_ajax_zog_ajax_loader', 'zog_ajax_loader' );
add_action( 'wp_ajax_nopriv_zog_ajax_loader', 'zog_ajax_loader' );

function zog_ajax_loader() {

    $postToLoad = (isset($_GET['postLoad'])) ? $_GET['postLoad'] : 6;
    $categoriesToLoad = (isset($_GET['categories']) && $_GET['categories'] != "") ? $_GET['categories'] : false;
    $postType = (isset($_GET['postType'])) ? $_GET['postType'] : '';
    $offset = (isset($_GET['offset'])) ? $_GET['offset'] : $postToLoad - 1;
    $author = (isset($_GET['author'])) ? $_GET['author'] : false;  

    // echo $categoriesToLoad;
    // echo $postType;
    // wp_die();



    $postArgs = array(
        'numberposts'      => $postToLoad,
        'offset'           => $offset,
        // 'category'         => $categoriesToLoad,
        'orderby'          => 'date',
        // 'exclude'          => get_the_ID(),

        'tax_query' => array(
            array(
                'taxonomy' => 'category',
                'terms' => $categoriesToLoad,
                'field' => 'term_id',
            )
        ),
        'order'            => 'DESC',
        'post_type'        => $postType,
        'post_status'      => array('publish', 'private'),
        'suppress_filters' => true 
    );

    // if ($categoriesToLoad != false) {
    //     $postArgs['category'] = $categoriesToLoad;
    // };

    if ($author !== false) {
        $postArgs['author_name'] = $author;
    }




    $posts_array = get_posts( $postArgs );

    global $post;

    $return = "";
    // This should mimic the individual exceprt format

    $counter = 1;
    foreach ($posts_array as $post) : setup_postdata( $post ); 
        // $return .= '<article id="post-' . get_the_ID() . '">';
        // $return .=  '<a href="' . get_permalink() . '">' . get_the_title() . '</a>';
        // $return .=  '<p class="post-entry-info">' . nas_posted_entry(false) . '</p>';
        // $return .= '</article>';
        
        // $return = explode(" ", string)
        if ($counter % 2 != 0 ) :
            $return .= "<div class='container'><div class='archive-row'>";
        endif;

        $return .= '<article id="post-' . get_the_ID() . '" class="' . implode(" ", get_post_class('col-md-6')) . '">';
        $return .= '<header class="entry-header">';
        $return .= '<div class="image-container">';
        if(get_the_post_thumbnail($post, 'medium_large') == ''){
            $return .='<img src="/wp-content/themes/north-american-spine/inc/images/logo.jpg" style="max-width: 100%; min-height: auto;">';
        }
        else{
            $return .= get_the_post_thumbnail($post, 'medium_large');
        }
        $return .= '</div>';
        $return .= '<h1 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . get_the_title() . '</a></h1>';
        $return .= '<div class="entry-meta lb-border-bottom">';
        $return .= nas_posted_entry(false, false);
        $return .= '</div>';
        $return .= '</header>';
        $return .= '<div class="entry-content"><p>';
        $return .= get_the_excerpt();
        $return .= '<p></div>';
        $return .= '<footer class="entry-footer">';
        $return .= '<a class="read-more" href="' . esc_url( get_permalink() ) . '" rel="bookmark">Read More</a>';
        $return .= '</footer>';
        $return .= '</article>';

        

        if ($counter % 2 == 0 || count($posts_array) == 1) :
            $return .= "</div></div>";
        endif;
        $counter++;
    endforeach;

    wp_reset_postdata();

    echo $return;
    wp_die();

}


/**
 * The meat of the loader. Returns a link with next page (ideal is javascript is not enabled for whatever reason)
 * @param  string $categories A string of comma seperated category ID's. Useful for calling on a category page template.
 * @return string             The actual load more button 
 */
function zog_ajax_load_more($categories = false) {
    global $paged, $wp_query;

    // Get post type, and post type object. Use this to determine correct slug for paged button.
    $postType = get_post_type();
    $postTypeObject = get_post_type_object($postType);

    $slug = $postTypeObject->rewrite['slug'];
    
    $author = false;
    if ( is_author() )
        $author = get_the_author();

    // printer($postTypeObject);

    // Grab all categories (will need updating for category page)
    if (!$categories) {
        $categories = array();

        // printer(get_post_type());

        // $postType = 
        // die;
    //     // printer(get_post_type_object());
    //     // $postType = get_post_type();
    //     // $postObject = get_post_type_object($postType);
    //     // printer($postObject);
    //     $args = array();
    //     $categories = "";
        if ($postType === 'nas-testimonials') {
            $idObj = get_category_by_slug('patient-reviews'); 
            $id = $idObj->term_id;
            $categories[] = $id;
        } elseif ($postType === 'post') {
           $idObj = get_category_by_slug('blog'); 
            $id = $idObj->term_id;
            $categories[] = $id;
        } 

        else {
    //         /**
    //          * TODO: Replace child_of with dynamic category pull,
    //          * and add blog category term_id
    //          */
    //         $args = array( 
    //             // 'child_of' => 22,
    //             'hide_empty' => false,
    //         );
    //         foreach (get_categories($args) as $category) {
    //             $categories[] = $category->term_id;
    //         } 
        }

        $categories = implode(',', $categories);
    }

    // var_dump($categories);
    // die;

    // Get post type


    // Get the amount of posts to load
    $postsToLoad = get_option('posts_per_page');

    // For no javascript 
    $max_page = $wp_query->max_num_pages;

    // printer($max_page);

    

    if (!$paged) 
        $paged = 1;

    

    $nextpage = intval($paged) + 1;
    $link = false;

    if ( !is_single() && ( $nextpage <= $max_page ) ) {
        $attr = apply_filters( 'next_posts_link_attributes', '' );

        $link = next_posts($max_page, false); 
    }


    // 6 items per page
    // offset = 1
    // offset = 6
    // offset = 12
    // 
    // 
    // we're on page 2 of 3
    // each page loads 2
    // 
    // offset = 1
    // offset = 2
    // 
    // toLoad = 2
    // 
    // we're on page 1 of 3
    // each page loads 2
    // offset = 1
    // offset = 2
    // offset = 4
    // 
    // toLoad = 2
    // 


    // If the link exists, display the loader
    // printer($categories);
    if ($link) :
    ?>
        
        <a href="<?php echo $link; ?>" class="load-more button btn-orange invert" data-offset="<?php echo $paged * $postsToLoad; ?>" data-categories="<?php echo $categories; ?>" data-load-count="<?php echo $postsToLoad; ?>" data-paged="<?php echo $paged; ?>" data-max-count="<?php echo $max_page; ?>" data-post-type="<?php echo $postType; ?>" data-slug="<?php echo $slug;?>" <?php if ($author !== false) : ?>data-author="<?php echo $author; ?>"<?php endif; ?>><div></div><?php _e('Load More'); ?></a>

    <?php
    endif; 
}