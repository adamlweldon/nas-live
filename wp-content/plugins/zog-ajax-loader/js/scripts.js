/**
 * jQuery Wrapper
 */
(function($) {

    $(document).on('ready', function() {

        $('.load-more').on('click', function(e) {
            e.preventDefault();


            var clicked = $(this),
            offset = $(this).data('offset'),
            toLoad = $(this).data('load-count'), 
            categories = $(this).data('categories'),
            slug = $(this).data('slug'),
            postType = $(this).data('post-type'),
            currentOffset = offset + toLoad,
            author = $(this).data('author');
            $(this).data('offset', currentOffset);

            console.log(author);


            paged = currentOffset / toLoad; 


            var state = '/' + slug + '/page/' + (paged );


            $.ajax({
                type: 'GET',
                dataType: 'html',   
                url: ajax_object.ajax_url,
                data: {
                    'postLoad': toLoad,
                    'categories': categories,
                    'offset': offset,
                    'postType':postType,
                    'action': 'zog_ajax_loader',
                    'author': author
                },
                beforeSend : function () {
                    $('#blog').addClass('loading');
                },
                success: function (response) {
                    console.log(response);
                    window.setTimeout(function() {
                        $('.load-section').append(response);
                        $('#blog').removeClass('loading');
                         window.history.pushState(null, null, state);
                         if (paged == clicked.data('max-count') ) {
                            clicked.remove();
                        } else {
                         clicked.attr('href', '/' + slug + '/page/' + (paged + 1));
                        }
                    }, 500);

                     
                },
                error : function (response, textStatus, errorThrown) {
                    console.log(response);
                },
            });



        })
    })

})(jQuery);