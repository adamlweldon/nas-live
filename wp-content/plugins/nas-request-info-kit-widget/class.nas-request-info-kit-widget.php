<?php

// Creating the widget 
class nrik_widget extends WP_Widget {

    function __construct() {
        parent::__construct(
            // Base ID of the widget
            'nrik_widget', 

            // Widget name will appear in UI
            __('Info Kit Widget', 'nas_nrik_widget'), 

            // Widget description
            array( 'description' => __( 'Displays related posts on the blog page.', 'nas_nrik_widget' ), ) 
        );
    }

    // Creating widget front-end
    // This is where the action happens
    public function widget( $args, $instance ) {


        $title = apply_filters( 'widget_title', $instance['title'] );
        $button_text = $instance['button_text'];

        $front_image = get_field('image_one', 'widget_' . $args['widget_id']);
        $back_image = get_field('image_two', 'widget_' . $args['widget_id']);


        ?>

            <div class="request-info-kit">
                <?php   
                echo $args['before_widget'];
                if ( ! empty( $title ) )
                    echo '<h3>' . $title . '</h3>'; 

                if ( ! empty( $front_image ) && ! empty( $back_image )) :
                ?>
                <a href="/resource-center/request-free-info-kit/" id="trigger-info" data-toggle="modal" data-target="#freekit">
                    <?php if ( ! empty( $back_image ) ) : ?>
                        <img src="<?php echo $back_image; ?>" alt="Info Brochure Back">
                    <?php endif; ?>
                    <?php if ( ! empty( $front_image ) ) : ?>
                        <img src="<?php echo $front_image; ?>" alt="Info Brochure Front">
                    <?php endif; ?>
                </a>
                <?php endif; ?>
                <a href="/resource-center/request-free-info-kit/" class="btn-orange invert" id="trigger-info" data-toggle="modal" data-target="#freekit">
                    <div></div>
                    <?php echo $button_text; ?>
                </a>
            </div>

        <?php
    }
        
    // Widget Backend 
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        } else {
            $title = __( 'Request a Free Info Kit', 'nas_nrik_widget' );
        }

        if (isset( $instance['button_text'] ) ) {
            $button_text = $instance['button_text'];
        } else {
            $button_text = __( 'Get Information Kit', 'nas_nrik_widget' );
        }

        if ( isset ($instance[ 'quantity' ] ) ) {
            $quantity = $instance['quantity'];
        } else {
            $quantity = 2;
        }


        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />

            <label for="<?php echo $this->get_field_id( 'button_text' ); ?>"><?php _e( ' Button Text:' ); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id( 'button_text' ); ?>" name="<?php echo $this->get_field_name( 'button_text' ); ?>" type="text" value="<?php echo esc_attr( $button_text ); ?>" />

        </p>
        <?php 
    }
        
    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['button_text'] = ( ! empty ($new_instance['button_text'] ) ) ? strip_tags( $new_instance['button_text'] ) : "" ;
        return $instance;
    }
} // Class nrik_widget ends here

// Register and load the widget
function nrik_widget_load() {
    register_widget( 'nrik_widget' );
}
add_action( 'widgets_init', 'nrik_widget_load' );