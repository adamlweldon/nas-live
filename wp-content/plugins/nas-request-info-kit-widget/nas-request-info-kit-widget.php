<?php
/*
Plugin Name: NAS Request Info Kit Widget
Description: Adds a Widget for displaying related posts. Used on homepage, procedures, and blog posts.
*/

add_action( 'admin_init', 'nas_request_info_kit_widget_require' );
function nas_request_info_kit_widget_require() {
    if ( is_admin() && current_user_can( 'activate_plugins' ) &&  !is_plugin_active( 'nas-support-functions/nas-support-functions.php' ) ) {
        add_action( 'admin_notices', 'nas_request_info_kit_widget_error' );

        deactivate_plugins( plugin_basename( __FILE__ ) ); 

        if ( isset( $_GET['activate'] ) ) {
            unset( $_GET['activate'] );
        }
    }
}

function nas_request_info_kit_widget_error(){
    ?><div class="error"><p>Sorry, but NAS Request Info kit Widget requires the NAS Support Functions plugin to be installed and active.</p></div><?php
}


require_once( ABSPATH . 'wp-content/plugins/nas-support-functions/nas-support-functions.php' );
define( 'NAS_REQUEST_INFOT_KIT_WIDGET__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
require_once( NAS_REQUEST_INFOT_KIT_WIDGET__PLUGIN_DIR . 'class.nas-request-info-kit-widget.php' );