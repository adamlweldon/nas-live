/**
 * Facebook API Setup
 */
window.fbAsyncInit = function() {
    FB.init({
        appId      : '744892592340398',
        xfbml      : true,
        version    : 'v2.8'
    });
    //FB.AppEvents.logPageView();
};

(function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


/**
 * jQuery Wrapper
 */
(function($) {

    $(document).on('ready', function() {


        $('.social-share .facebook').on('click', function(e) {
            e.preventDefault();
            FB.ui({
                method: 'share',
                href: $(this).data('link'),
            }, function(response){});
        })


    })


})(jQuery);
