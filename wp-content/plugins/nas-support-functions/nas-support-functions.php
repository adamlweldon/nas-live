<?php
/*
Plugin Name: NAS Support Functions
Description: Provides some additional functions for use in NAS related plugins and theme files.
*/



define( 'ZOG_SUPPORT_PLUGINS', plugin_dir_url( __FILE__ ) );


function zog_support_plugins_scripts() {
    wp_enqueue_script( 'zog_support_script', ZOG_SUPPORT_PLUGINS . 'js/scripts.min.js', array('jquery'), '1.0' );
    wp_enqueue_script( 'twitter_widget_api', 'https://platform.twitter.com/widgets.js', array(), '1.0');
}
add_action( 'wp_enqueue_scripts', 'zog_support_plugins_scripts' );


// Returns the date posted, if in the loop.
if ( ! function_exists('nas_posted_entry')) :
    function nas_posted_entry($echo = true, $author = true, $date = false) {
        $string = "";
        if ($author && !$date) {
            $string = get_the_author_posts_link();
        } elseif ($date && !$author) {
            $string = get_the_time('F d, Y');
        } elseif ($date && $author) {
            $string = get_the_time('F d, Y') . ' | ' . get_the_author_posts_link();
        }
        
        if ($echo == true) {
            echo '<p>' . $string . '</p>';
        } else {
            return $string;
        }
    }
endif; 


// Returning the share icons for posts.
if ( ! function_exists('nas_entry_footer') ) :
    function nas_entry_footer($echo = true) {


        if (get_post_type() === 'nas-testimonials') {
            return false;
        }

        $string = '';


        /**
         * Get the tag cloud for the post. 
         * Used for generating hashtags.
         */
        $tags = get_the_tags(); 
        $tagCloud = [];
        foreach ($tags as $tag) {
            if ($tag->name == 'North American Spine') {
                continue;
            }
            $tagCloud[] = str_replace(' ', '', $tag->name);
        }
        $tagCloud = implode(',', $tagCloud);

        /**
         * Returns the page URL.
         */
        $pageUrl = get_permalink();
        

        $twitter = [
            'url'      => $pageUrl,
            'hashtags' => $tagCloud,
            'via'      => 'NASpine',
            'text'     => '',
        ];

        $twitterIntent = http_build_query($twitter);


        $email = [
            'subject' => get_the_title(),
            'body' => get_the_excerpt() . '  See more - ' . esc_url( get_permalink() ),
        ];


        $email = urldecode(http_build_query($email));


        $string = '<div class="social-share"><p>Share on:</p><ul><li class="facebook" data-link="' . $pageUrl . '"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li><li><a href="https://twitter.com/intent/tweet?'.  $twitterIntent . '"><i class="fa fa-twitter" aria-hidden="true"></i></a></li><li><a href="https://plus.google.com/share?url=' . $pageUrl . '" onclick="javascript:window.open(this.href,\'\', \'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600\');return false;"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li><li><a href=\'mailto:?' . $email. '\'><i class="fa fa-envelope" aria-hidden="true"></i></a></ul></div>';

        if ($echo = true) {
            echo $string;
        } else {
            return $string;
        }
    }
endif; 



if ( ! function_exists( 'printer' ) ) :
    function printer($data = 'No variable to print!') {
        echo '<pre>'; var_dump($data); echo '</pre>';
    }
endif;