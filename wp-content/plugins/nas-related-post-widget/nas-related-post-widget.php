<?php
/*
Plugin Name: NAS Related Post Widget
Description: Adds a Widget for displaying related posts.
*/

add_action( 'admin_init', 'nas_related_post_widget_require' );
function nas_related_post_widget_require() {
    if ( is_admin() && current_user_can( 'activate_plugins' ) &&  !is_plugin_active( 'nas-support-functions/nas-support-functions.php' ) ) {
        add_action( 'admin_notices', 'nas_related_post_widget_error' );

        deactivate_plugins( plugin_basename( __FILE__ ) ); 

        if ( isset( $_GET['activate'] ) ) {
            unset( $_GET['activate'] );
        }
    }
}

function nas_related_post_widget_error(){
    ?><div class="error"><p>Sorry, but NAS Related Post Widget requires the NAS Support Functions plugin to be installed and active.</p></div><?php
}


require_once( ABSPATH . 'wp-content/plugins/nas-support-functions/nas-support-functions.php' );
define( 'NAS_RELATED_POSTS__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
require_once( NAS_RELATED_POSTS__PLUGIN_DIR . 'class.nas-related-post-widget.php' );