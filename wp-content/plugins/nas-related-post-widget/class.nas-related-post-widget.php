<?php

// Creating the widget 
class nrp_widget extends WP_Widget {

    function __construct() {
        parent::__construct(
            // Base ID of the widget
            'nrp_widget', 

            // Widget name will appear in UI
            __('Related Posts Widget', 'nas_rp_widget'), 

            // Widget description
            array( 'description' => __( 'Displays related posts on the blog page.', 'nas_rp_widget' ), ) 
        );
    }

    // Creating widget front-end
    // This is where the action happens
    public function widget( $args, $instance ) {


        $title = apply_filters( 'widget_title', $instance['title'] );



        // Get the current post category
        $postCategories = array();
        // printer(get_the_category());
        foreach (get_the_category() as $category) {
            $postCategories[] = $category->term_id;
        }
        $postCategories = implode(',', $postCategories);
        
        // Determine the current post type.
        $postType = get_post_type();

        $postArgs = array(
            'numberposts'      => $instance['quantity'],
            'tag'              => 'featured',
            'exclude'          => get_the_ID(),
            'order'            => 'DESC',
            'orderby'          => 'rand',
            'post_type'        => $postType,
            'post_status'      => 'publish',
            'suppress_filters' => false
        );


        // if (! empty($postCategories))
            // $postArgs['post_category'] = $postCategories;

        // Get the related posts
        $posts_array = get_posts( $postArgs );

        // printer($postArgs);
        // printer($posts_array);

        if (count($posts_array) > 0) :
            // before and after widget arguments are defined by themes
            echo $args['before_widget'];
            if ( ! empty( $title ) )
                echo $args['before_title'] . $title . $args['after_title'];
        

            global $post;
            foreach ($posts_array as $post) : setup_postdata( $post ); ?>
                <article id="post-<?php the_ID(); ?>" class="lb-border-bottom">
                    <a href="<?php the_permalink(); ?>">
                        <!-- <span class="image-container"> -->
                        <?php the_post_thumbnail('original'); ?>
                        <!-- </span> -->
                        <?php the_title(); ?>
                    </a>
                    <p class="post-entry-info">
                        <?php echo nas_posted_entry(false, $instance['author'], $instance['date']); ?>
                        <?php if (get_field('contains_video') == 1) : ?>
                            <img src="<?php echo NAS_TEMPLATE_DIR . '/assets/img/video-icon.svg'; ?>" alt="Video Icon" class="has-video">
                        <?php endif; ?>
                    </p>
                </article>
            <?php 
            endforeach;
            wp_reset_postdata();    

            echo $args['after_widget'];

        endif;
    }
		
    // Widget Backend 
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        } else {
            $title = __( 'Related Posts', 'nas_rp_widget' );
        }

        if ( isset ($instance[ 'quantity' ] ) ) {
            $quantity = $instance['quantity'];
        } else {
            $quantity = 2;
        }


        $author = $instance[ 'author' ] ? 'true' : 'false';
        $date = $instance[ 'date' ] ? 'true' : 'false';

        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
            <label for="<?php echo $this->get_field_id( 'quantity' ); ?>"><?php _e('Quantity to display:'); ?></label>
            <input type="number" min="1" max="5" class="widefat" id="<?php echo $this->get_field_id( 'quantity' ); ?>" name="<?php echo $this->get_field_name( 'quantity' ); ?>" value="<?php echo esc_attr( $quantity ); ?>">
            <label for="<?php echo $this->get_field_id( 'author' ); ?>"><?php _e('Display the author:'); ?></label>
            <input type="checkbox" <?php checked( $instance[ 'author' ], 'on' ); ?>  class="widefat" id="<?php echo $this->get_field_id( 'author' ); ?>" name="<?php echo $this->get_field_name( 'author' ); ?>">
            <label for="<?php echo $this->get_field_id( 'date' ); ?>"><?php _e('Display the date:'); ?></label>
            <input type="checkbox" <?php checked( $instance[ 'date' ], 'on' ); ?>  class="widefat" id="<?php echo $this->get_field_id( 'date' ); ?>" name="<?php echo $this->get_field_name( 'date' ); ?>">
        </p>
        <?php 
    }
    	
    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['quantity'] = ( ! empty( $new_instance['quantity'] ) ) ? strip_tags( $new_instance['quantity'] ) : '';
        $instance['author'] = $new_instance[ 'author' ];
        $instance['date'] = $new_instance[ 'date' ];
        return $instance;
    }
} // Class nrp_widget ends here



// Creating the widget 
class nrp_next_widget extends WP_Widget {
    
        function __construct() {
            parent::__construct(
                // Base ID of the widget
                'nrp_next_widget', 
    
                // Widget name will appear in UI
                __('Next Post Widget', 'nrp_next_widget'), 
    
                // Widget description
                array( 'description' => __( 'Displays the next postbased on the current post.', 'nrp_next_widget' ), ) 
            );
        }
    
        // Creating widget front-end
        // This is where the action happens
        public function widget( $args, $instance ) {
    
    
            $title = apply_filters( 'widget_title', $instance['title'] );
    
    
            // global $post;
            
            
            $next_post = get_adjacent_post(false, '', true);
            
            if (!empty( $next_post )): ?>
              <a href="<?php echo esc_url( get_permalink( $next_post->ID ) ); ?>" class="button btn-orange invert"><div></div><?php echo esc_attr( $title ); ?></a>

            <?php
            endif;
        }
            
    // Widget Backend 
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        } else {
            $title = __( 'Next Post', 'nas_rp_widget' );
        }

        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <?php 
    }
        
    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }
} // Class nrp_widget ends here

// Register and load the widget
function wpb_load_widget() {
    register_widget( 'nrp_widget' );
    register_widget( 'nrp_next_widget' );
}
add_action( 'widgets_init', 'wpb_load_widget' );